//
//  AccountManagerController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 10/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
class AccountManagerController: BaseController {
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           
           navigationController?.isNavigationBarHidden = false
           tabBarController?.tabBar.isHidden = true
       }
       
       override func viewDidLoad() {
           super.viewDidLoad()
           definesPresentationContext = true

           view.backgroundColor = .white
           navigationItem.title = "Account Manager"
        
        view.addConstraints(format: "V:|-120-[v0]-15-[v1]-15-[v2(200)]-15-[v3]", views: topicTf, messageLb, messageTf, submitBtn)
        
        view.addConstraints(formats: ["H:|-15-[v0]-15-|"], views: topicTf, messageTf, submitBtn)
        view.addConstraints(format: "H:|-15-[v0]", views: messageLb)

    }
    
    lazy var topicTf = getLabelledTextField(placeholder: "", labelText: "Topic", keyboardType: .emailAddress)
    
    lazy var messageLb = getLabel(text: "Message", fontSize: 14, boldFont: false, textColor: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1))
    
    let messageTf : UITextView = {
        let textField = UITextView()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = .systemFont(ofSize: 17)
        textField.layer.borderWidth = 1.5
        textField.layer.borderColor = UIColor(hex: "#dddddd")?.cgColor
        textField.layer.cornerRadius = 4
        textField.backgroundColor = UIColor.white
        textField.setShadow()
        return textField
    }()
    
    lazy var submitBtn = getButton(title: "Submit", action: #selector(submit))
    
    @objc func submit(){
        if topicTf.isValid(){
            let dict = ["userId":DefaultsManager.getUserId(), "topic":topicTf.trimmedText!, "message":messageTf.text ?? ""]
            makeHttpCall(.submitMessage, .post, dict){data in
                
                let dataDict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                let message:String = (dataDict?["msg"] as? String) ?? "Your message has been successfully sent"
                                
                self.showDialog(message: message, disableBackgroundTap:true){_ in
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
            
    }
}
