//
//  BaseController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 10/05/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class BaseController: UIViewController, UITextFieldDelegate {

    let useDummyData = true
    
    var scrollView:UIScrollView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        if let scrollView = scrollView{
            let userInfo = notification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = self.view.convert(keyboardFrame, from: nil)

            var contentInset:UIEdgeInsets = self.scrollView!.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
        }
    }

    @objc func keyboardWillHide(notification:NSNotification){
        if let scrollView = scrollView{
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            scrollView.contentInset = contentInset
        }
    }
    
    
    var tag = 1
    func getLabelledTextField(placeholder:String, labelText:String = "", keyboardType:UIKeyboardType = .default, isPassword:Bool = false, isUserInteractionEnabled:Bool = true, action: Selector?, forEvent: UIControl.Event?)-> CustomLabelledTextField{
        
        let textField = CustomLabelledTextField()
        
        textField.placeholder = placeholder
        
        textField.labelText = labelText != "" ? labelText : placeholder
        textField.returnKeyType = .next
        textField.keyboardType = keyboardType
        textField.isUserInteractionEnabled = isUserInteractionEnabled
        
        if isPassword{
            textField.textField.isSecureTextEntry = true
        }
        
        if action != nil && forEvent == nil{
            let tapGesture = UITapGestureRecognizer(target: self, action: action)
            textField.textField.addGesture(tapGesture)
        }else if forEvent != nil{
            textField.addTarget(self, action: action!, for: forEvent!)
        }
        
        if isUserInteractionEnabled && ((action == nil && forEvent == nil) || (action != nil && forEvent != nil)) {
            textField.tag = tag
            tag+=1
            textField.delegate = self
        }
        
        if keyboardType == .numberPad || keyboardType == .phonePad{
            let keyboardToolbar = UIToolbar()
            keyboardToolbar.sizeToFit()
            let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil, action: nil)
            let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done,
                                                target: view, action: #selector(UIView.endEditing(_:)))
            keyboardToolbar.items = [flexBarButton, doneBarButton]
            textField.inputAccessoryView2 = keyboardToolbar
        }
        
        return textField
    }
    func getLabelledTextField(placeholder:String, labelText:String = "", keyboardType:UIKeyboardType = .default, isPassword:Bool = false, isUserInteractionEnabled:Bool = true)-> CustomLabelledTextField{

        return getLabelledTextField(placeholder:placeholder, labelText:labelText, keyboardType:keyboardType, isPassword:isPassword, isUserInteractionEnabled:isUserInteractionEnabled, action: nil, forEvent:nil)
    }
    func getLabelledTextField(placeholder:String, labelText:String = "", keyboardType:UIKeyboardType = .default, isPassword:Bool = false, action: Selector)-> CustomLabelledTextField{
        
        return getLabelledTextField(placeholder:placeholder, labelText:labelText, keyboardType:keyboardType, isPassword:isPassword, isUserInteractionEnabled:true, action: action, forEvent:nil)
    }
    
    func getAllTextfield(view: UIView) -> [UITextField] {
        var results = [UITextField]()
        for subview in view.subviews as [UIView] {
            if let textField = subview as? UITextField {
                if textField.tag != 0 {
                    results += [textField]
                }
            } else {
                results += getAllTextfield(view: subview)
            }
        }
        return results
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        let allTextField = getAllTextfield(view: self.view)
                
        if textField.tag == allTextField.count{
            textField.resignFirstResponder()
            textField.returnKeyType = .done
        }else{
            for mTextField in allTextField{
                if mTextField.tag == textField.tag+1{
                    if mTextField.tag == allTextField.count{
                        mTextField.returnKeyType = .done
                    }
                    mTextField.becomeFirstResponder()
                }
            }
        }
        return true
    }

    
}
