//
//  FarmHistoryController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 10/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
import DTPagerController

class FarmHistoryController: DTPagerController {
    
    let dummyImage = ""
    
    init() {
        super.init(viewControllers: [])
        title = "Farm History"
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
        navigationItem.title = "Farm History"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

//        preferredSegmentedControlHeight = 100

        view.backgroundColor = .white
        perferredScrollIndicatorHeight = 4
        
        textColor = UIColor.gray
        selectedTextColor = .black

        let viewController1 = RunningFarmsController()
        viewController1.title = "Running Farms"
        
        let viewController2 = ActiveFarmController()
        viewController2.title = "Active Farms"
        
        let viewController3 = AllFarmHistoryController()
        viewController3.title = "Farms History"

        viewControllers = [viewController1, viewController2, viewController3]
        scrollIndicator.backgroundColor = .primaryColour
        scrollIndicator.layer.cornerRadius = scrollIndicator.frame.height / 2

//        setSelectedPageIndex(1, animated: false)

//        pageSegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.customYellow], for: .selected)
        pageSegmentedControl.backgroundColor = .white
        
        pageSegmentedControl.layer.masksToBounds = false
        pageSegmentedControl.layer.shadowColor = UIColor.white.cgColor
        pageSegmentedControl.layer.shadowOffset = CGSize(width: 0, height: 1)
        pageSegmentedControl.layer.shadowRadius = 1
        pageSegmentedControl.layer.shadowOpacity = 0.5
        
    }

}
