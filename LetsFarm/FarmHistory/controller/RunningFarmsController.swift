//
//  RunningFarmsController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 10/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class RunningFarmsController: BaseController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
   var activeFarmsResData:[ActiveFarmsResData]?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true

        view.backgroundColor = .white
        navigationItem.title = "Running Farms"
        
        view.addConstraints(format: "H:|[v0]|", views: [farmsCv])
        view.addConstraints(format: "V:|-10-[v0]-25-|", views: farmsCv)
        fetchFarmHistory()

    }
    
    lazy var farmsCv = getCollectionView(spacing: 20, cell: FarmsCell.self, identifier: "farmHistoryCell", delegate: self, dataSource: self)

    
    func fetchFarmHistory(){
//        self.makeHttpCall(.getActiveSponsorship, .get, path:DefaultsManager.getUserId()){data in
//            self.activeFarmsResData = try? JSONDecoder().decode(ActiveFarmsRes.self, from: data).data
//            self.farmsCv.reloadData()
//        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 40, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activeFarmsResData?.count ?? 0
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let position = indexPath.item
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "farmHistoryCell", for: indexPath) as! FarmsCell
//            cell.tag = position
//            cell.addGesture(UITapGestureRecognizer(target: self, action: #selector(onFarmCellTap(sender:))))
//            cell.farm = self.selectedFarms![indexPath.item]
            return cell
    }
}
