//
//  ActiveFarmCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 10/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class ActiveFarmCell: UICollectionViewCell {
    
    var activeFarm : ActiveFarmsResData?{
        didSet{
            if let order = activeFarm?.orderId{
                self.descrLb.text = "\(order.farmName!) at \(order.roi!)% in \(order.duration!) \(order.durationType!)"

                self.investmenLb.text = "Amount invested: ₦\(order.totalPrice!)"
            }
            self.endCycleLb.text = "End of cycle: \(activeFarm?.expectedEndDate!.getFormattedDate() ?? "")"
            self.startCycleLb.text = "Start of cycle: \(activeFarm?.startDate!.getFormattedDate() ?? "")"
         
            self.returnsLb.text = "Expected returns: ₦\(activeFarm?.expectedReturn?.withCommas() ?? "0")"
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addConstraints(format: "V:|-10-[v0]-10-[v1]-10-[v2]-10-[v3(1)]", views: descrLb, startCycleLb, investmenLb, lineVw)
        addConstraints(format: "H:|-10-[v0]", views: descrLb)
        addConstraints(formats: ["H:|-15-[v0]"], views: startCycleLb, investmenLb)

        addConstraints(format: "V:|-10-[v0]-10-[v1]-10-[v2]-10-|", views: descrLb, startCycleLb, investmenLb)

        addConstraints(formats: ["H:[v0]-15-|"], views: endCycleLb, returnsLb)

        addConstraints(format: "H:|[v0]|", views: lineVw)
        lineVw.backgroundColor = UIColor.init(hex: "#eeeeee")
        
        endCycleLb.alignBottomToBottomOf(of: startCycleLb)
        returnsLb.alignBottomToBottomOf(of: investmenLb)

    }
    

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var descrLb = getLabel(text: "Fish farm at 30% in 8 Months", fontSize: 14, boldFont: true, textColor: .black)
    
    lazy var startCycleLb = getLabel(text: "Start of cycle: Jun 30, 2020", fontSize: 12)
    lazy var endCycleLb = getLabel(text: "End of cycle: Jun 30, 2020", fontSize: 12)
    
    lazy var investmenLb = getLabel(text: "Amount invested: N65,000", fontSize: 12)
    lazy var returnsLb = getLabel(text: "Expected returns: N65,000", fontSize: 12)

    lazy var lineVw = UIView()
}
