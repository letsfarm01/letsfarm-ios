//
//  ActiveFarmsRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 10/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct ActiveFarmsRes : Codable {

        let code : Int?
        let data : [ActiveFarmsResData]?
        let msg : String?
        let status : String?

        enum CodingKeys: String, CodingKey {
            case code = "code"
            case data = "data"
            case msg = "msg"
            case status = "status"
        }
    
}

struct ActiveFarmsResData : Codable {

    let v : Int?
    let id : String?
    let advanceType : String?
    let createdAt : String?
    let deleted : Bool?
    let expectedEndDate : String?
    let expectedReturn : Int?
    let farmId : String?
    let itemRemaining : Int?
    let orderId : OrderId?
//    let roiData : [AnyObject]?
    let startDate : String?
    let status : String?
    let updatedAt : String?
    let userId : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case advanceType = "advanceType"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case expectedEndDate = "expected_end_date"
        case expectedReturn = "expected_return"
        case farmId = "farmId"
        case itemRemaining = "itemRemaining"
        case orderId = "orderId"
//            case roiData = "roi_data"
        case startDate = "start_date"
        case status = "status"
        case updatedAt = "updatedAt"
        case userId = "userId"
    }
    
}

struct OrderId : Codable {

    let v : Int?
    let id : String?
    let cartId : String?
    let createdAt : String?
    let currency : String?
    let deleted : Bool?
    let duration : Int?
    let durationType : String?
    let farmName : String?
    let farmId : String?
    let orderReference : String?
    let paymentChannel : String?
    let pricePerUnit : Int?
    let qtyRemainingBeforeOrder : Int?
    let quantity : Int?
    let roi : Int?
    let totalPrice : Int?
    let updatedAt : String?
    let userId : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case cartId = "cartId"
        case createdAt = "createdAt"
        case currency = "currency"
        case deleted = "deleted"
        case duration = "duration"
        case durationType = "duration_type"
        case farmName = "farm_name"
        case farmId = "farmId"
        case orderReference = "orderReference"
        case paymentChannel = "payment_channel"
        case pricePerUnit = "price_per_unit"
        case qtyRemainingBeforeOrder = "qty_remaining_before_order"
        case quantity = "quantity"
        case roi = "roi"
        case totalPrice = "total_price"
        case updatedAt = "updatedAt"
        case userId = "userId"
    }
    
}
