//
//  BankInfoController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 25/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class BankInfoController: BaseController  {
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       
       navigationController?.isNavigationBarHidden = false
       tabBarController?.tabBar.isHidden = true
   }
       
    override func viewDidLoad() {
       super.viewDidLoad()

        self.scrollView = getMainScrollView(formSv)

        view.backgroundColor = .white
        navigationItem.title = "Bank Info"
        
        view.addConstraints(format: "V:|-130-[v0]-20-[v1]-30-|", views: self.scrollView!, updateBtn)
        
        view.addConstraints(formats: ["H:|-20-[v0]-20-|"], views: self.scrollView!, updateBtn)
        
        fecthUserInfo()
        fetchBanks()
    }
    
    lazy var accountNameTf = getLabelledTextField(placeholder: "Account name")
    lazy var accountNumberTf = getLabelledTextField(placeholder: "Account Number")
    lazy var bankTf = getLabelledTextField(placeholder: "Bank", action: #selector(bankPicker))
    lazy var bvnTf = getLabelledTextField(placeholder: "BVN")
    
    lazy var formSv = getStackView(accountNameTf, accountNumberTf, bankTf, bvnTf)

    lazy var updateBtn = getButton(title: "Update Profile", action: #selector(updateProfile))
    
    @objc func updateProfile(){
        
        self.userData?.userId = DefaultsManager.getUserId()

        if areValid(accountNameTf, accountNumberTf, bankTf, bvnTf) {
            
            userData?.accountName = accountNameTf.text
            userData?.accountNumber = accountNumberTf.text
            userData?.bankName = bankTf.text
            userData?.bvn = bvnTf.text
            
            makeHttpCall(.updateBankInfo, .put, userData?.toDictionary(), path:DefaultsManager.getUserId()){data in
                
                self.showDialog(message: "Update successful"){_ in
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }

    var userData:ProfileUpdateData? = ProfileUpdateData()

    func fecthUserInfo(){
        makeHttpCall(.updateBankInfo, .get, path:DefaultsManager.getUserId()){data in
            self.userData = try? JSONDecoder().decode(ProfileUpdate.self, from: data).data
            
            self.accountNameTf.text = self.userData?.accountName
            self.accountNumberTf.text = self.userData?.accountNumber
            self.bankTf.text = self.userData?.bankName
            self.bvnTf.text = self.userData?.bvn
            
            self.userData?.fullName = nil
            self.userData?.referralCode = nil
        }
    }
    
    func fetchBanks(){
        makeHttpCall(.fetchBanks, .get){data in
            if let banksData = try? JSONDecoder().decode(BanksRes.self, from: data).data{
                self.setBankNames(banksData)
            }
        }
    }
    
    func setBankNames(_ banksData:[BankData]){
        
        for bank in banksData{
            banks.append(bank.name ?? "")
        }
    }
    
    var banks = [""]

    @objc func bankPicker(){
        let alert = UIAlertController(style: .actionSheet, title: "Gender", message: "Select your gender")

        let pickerViewValues: [[String]] = [banks.map {$0}]

        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: banks.count)

        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            self.bankTf.text = self.banks[index.row]
        }
        alert.addAction(title: "Done", style: .cancel)
        alert.show()
    }
}
