//
//  BusinessInfoController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 25/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class BusinessInfoController: BaseController {
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       
       navigationController?.isNavigationBarHidden = false
       tabBarController?.tabBar.isHidden = true
   }
       
    override func viewDidLoad() {
       super.viewDidLoad()

        
        self.scrollView = getMainScrollView(formSv)

        view.backgroundColor = .white
        navigationItem.title = "Business Info"

        
        view.addConstraints(format: "V:|-130-[v0]-20-[v1]-30-|", views: self.scrollView!, updateBtn)
        
        view.addConstraints(formats: ["H:|-20-[v0]-20-|"], views: self.scrollView!, updateBtn)
        fecthUserInfo()

    }
    
    lazy var nameTf = getLabelledTextField(placeholder: "Company Name")
    lazy var emailTf = getLabelledTextField(placeholder: "Company Email Address")
    lazy var phoneNumberTf = getLabelledTextField(placeholder: "Company Phone number")
    lazy var addressTf = getLabelledTextField(placeholder: "Company Address")
    
    lazy var formSv = getStackView(nameTf, emailTf, phoneNumberTf, addressTf)

    lazy var updateBtn = getButton(title: "Update Profile", action: #selector(updateProfile))
    
    @objc func updateProfile(){
        self.userData?.userId = DefaultsManager.getUserId()

        if areValid(nameTf, emailTf, phoneNumberTf, addressTf) {
            
            userData?.companyName = nameTf.text
            userData?.companyEmail = emailTf.text
            userData?.companyPhone = phoneNumberTf.text
            userData?.companyAddress = addressTf.text
            
            makeHttpCall(.updateBusinessInfo, .put, userData?.toDictionary(), path:DefaultsManager.getUserId()){data in
             
                self.showDialog(message: "Update successful"){_ in
                    self.navigationController?.popViewController(animated: true)
                }

            }
        }
    }

    var userData:ProfileUpdateData? = ProfileUpdateData()

    func fecthUserInfo(){
        makeHttpCall(.updateBusinessInfo, .get, path:DefaultsManager.getUserId()){data in
            self.userData = try? JSONDecoder().decode(ProfileUpdate.self, from: data).data
            
            self.nameTf.text = self.userData?.companyName
            self.emailTf.text = self.userData?.companyEmail
            self.phoneNumberTf.text = self.userData?.companyPhone
            self.addressTf.text = self.userData?.companyAddress
            
            self.userData?.fullName = nil
            self.userData?.referralCode = nil
        }
    }
    
    
}
