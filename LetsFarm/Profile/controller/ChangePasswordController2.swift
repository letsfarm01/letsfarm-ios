//
//  ChangePasswordController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 25/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class ChangePasswordController2: BaseController {
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       
       navigationController?.isNavigationBarHidden = false
       tabBarController?.tabBar.isHidden = true
   }
       
    override func viewDidLoad() {
       super.viewDidLoad()

        
        self.scrollView = getMainScrollView(formSv)

        view.backgroundColor = .white
        navigationItem.title = "Change password"
  
        view.addConstraints(format: "V:|-130-[v0]-20-[v1]-30-|", views: self.scrollView!, updateBtn)
        
        view.addConstraints(formats: ["H:|-20-[v0]-20-|"], views: self.scrollView!, updateBtn)
        
    }
    
    lazy var currentPasswordTf = getLabelledTextField(placeholder: "Current Password", keyboardType: .default, isPassword:true)
    lazy var newPasswordTf = getLabelledTextField(placeholder: "New Password", keyboardType: .default, isPassword:true)
    lazy var confirmPasswordTf = getLabelledTextField(placeholder: "Confirm Password", keyboardType: .default, isPassword:true)
    
    
    lazy var formSv = getStackView(currentPasswordTf, newPasswordTf, confirmPasswordTf)

    lazy var updateBtn = getButton(title: "Update Profile", action: #selector(updateProfile))
    
    @objc func updateProfile(){
        if currentPasswordTf.isValid(6) && newPasswordTf.textField.isPasswordMatch(confirmPasswordTf.textField){
            
            let dict = ["old_password":currentPasswordTf.trimmedText!,"new_password":newPasswordTf.trimmedText!, "confirm_password":confirmPasswordTf.trimmedText!, "userId":DefaultsManager.getUserId()]
            
            makeHttpCall(.changePassword, .post, dict, { data, response, error in
                    
                    if (200 ... 299).contains(response.statusCode) {
                        self.showDialog(message: "Update successful"){_ in
                            self.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        let genRes = try? JSONDecoder().decode(GeneralResponse.self, from: data)
                        self.showDialog(message:"\(genRes?.msg ?? "An error occurred! Please try again")")
                    }
                    
            }, {_ in})
        }
    }

    
}
