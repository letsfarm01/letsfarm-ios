//
//  NextOfKinInfoController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 25/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class NextOfKinInfoController: BaseController {
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       
       navigationController?.isNavigationBarHidden = false
       tabBarController?.tabBar.isHidden = true
   }
       
    override func viewDidLoad() {
       super.viewDidLoad()

        
        self.scrollView = getMainScrollView(formSv)

        view.backgroundColor = .white
        navigationItem.title = "Next Of Kin Info"
                
        view.addConstraints(format: "V:|-130-[v0]-20-[v1]-30-|", views: self.scrollView!, updateBtn)
        
        view.addConstraints(formats: ["H:|-20-[v0]-20-|"], views: self.scrollView!, updateBtn)
        fecthUserInfo()

    }
    
    lazy var fullNameTf = getLabelledTextField(placeholder: "Full name")
    lazy var emailTf = getLabelledTextField(placeholder: "Email address")
    lazy var phoneNumberTf = getLabelledTextField(placeholder: "Phone number")
    lazy var relationshipTf = getLabelledTextField(placeholder: "Relationship")
    lazy var addressTf = getLabelledTextField(placeholder: "Address")
    
    
    lazy var formSv = getStackView(fullNameTf, emailTf, phoneNumberTf, relationshipTf, addressTf)

    lazy var updateBtn = getButton(title: "Update Profile", action: #selector(updateProfile))
    
    @objc func updateProfile(){
        self.userData?.userId = DefaultsManager.getUserId()

        if areValid(fullNameTf, emailTf, phoneNumberTf, relationshipTf, addressTf) {
            
            userData?.name = fullNameTf.text
            userData?.email = emailTf.text
            userData?.phoneNumber = phoneNumberTf.text
            userData?.relationship = relationshipTf.text
            userData?.address = addressTf.text
           
            makeHttpCall(.updateNextKinInfo, .put, userData?.toDictionary(), path:DefaultsManager.getUserId()){data in
                
                self.showDialog(message: "Update successful"){_ in
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
        }
    }

    var userData:ProfileUpdateData? = ProfileUpdateData()

    func fecthUserInfo(){
        makeHttpCall(.updateNextKinInfo, .get, path:DefaultsManager.getUserId()){data in
            self.userData = try? JSONDecoder().decode(ProfileUpdate.self, from: data).data
            
            self.fullNameTf.text = self.userData?.name
            self.emailTf.text = self.userData?.email
            self.phoneNumberTf.text = self.userData?.phoneNumber
            self.relationshipTf.text = self.userData?.relationship
            self.addressTf.text = self.userData?.address
            
            self.userData?.fullName = nil
            self.userData?.referralCode = nil
        }
    }
    
    
}

