//
//  ProfileController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 25/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
import FirebaseStorage
import Kingfisher

class PersonalInfoController: BaseController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var userData:ProfileUpdateData? = ProfileUpdateData()

    var userImage:UIImage?{
        didSet{
            self.userImg.image = userImage
        }
    }
        
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       
       navigationController?.isNavigationBarHidden = false
       tabBarController?.tabBar.isHidden = true
   }
       
    override func viewDidLoad() {
       super.viewDidLoad()

        self.scrollView = getMainScrollView(formSv)
        
        view.backgroundColor = .white
        navigationItem.title = "Personal Info"
        
        view.addConstraints(format: "V:|-120-[v0]-20-[v1]-20-[v2]-30-|", views: userImg, self.scrollView!, updateBtn)

        view.addConstraints(formats: ["H:|-20-[v0]-20-|"], views: self.scrollView!, updateBtn)
        view.addConstraints(format: "H:|-20-[v0]-30-[v1]", views: userImg, imageLb)

        self.userImg.layer.cornerRadius = 40
        self.userImg.clipsToBounds = true

        imageLb.alignTopToTopOf(of: userImg, by: 30)
        fecthUserInfo()
    }
    
    lazy var userImg = getImage(#imageLiteral(resourceName: "user"), height: 80, width: 80, action: #selector(selectImage))
    
    lazy var imageLb = getLabel(text: "Tap to change", fontSize: 18, textColor: .black, action: #selector(selectImage))
    
    lazy var firstNameTf = getLabelledTextField(placeholder: "First name")
    lazy var lastNameTf = getLabelledTextField(placeholder: "Last name")
    lazy var emailTf = getLabelledTextField(placeholder: "Email")
    lazy var phoneNumberTf = getLabelledTextField(placeholder: "Phone number")
    lazy var dateOfBirthTf = getLabelledTextField(placeholder: "Date of birth")
    lazy var genderTf = getLabelledTextField(placeholder: "Gender", action: #selector(genderPicker))
    lazy var addressTf = getLabelledTextField(placeholder: "Address")
    lazy var cityTf = getLabelledTextField(placeholder: "City")
    lazy var stateTf = getLabelledTextField(placeholder: "State", action: #selector(statePicker))
    lazy var countryTf = getLabelledTextField(placeholder: "Country", action: #selector(countryPicker))
    
    lazy var formSv = getStackView(firstNameTf, lastNameTf, emailTf, phoneNumberTf, dateOfBirthTf, genderTf, addressTf, cityTf, stateTf, countryTf)

    lazy var updateBtn = getButton(title: "Update Profile", action: #selector(updateProfile))
    
    @objc func updateProfile(){
        self.userData?.userId = DefaultsManager.getUserId()

        if areValid(firstNameTf, lastNameTf, emailTf, phoneNumberTf, dateOfBirthTf, genderTf, addressTf, cityTf, stateTf, countryTf) {
            
            userData?.firstName = firstNameTf.text
            userData?.lastName = lastNameTf.text
            userData?.email = emailTf.text
            userData?.phoneNumber = phoneNumberTf.text
            userData?.dob = dateOfBirthTf.text
            userData?.gender = genderTf.text
            userData?.address = addressTf.text
            userData?.city = cityTf.text
            userData?.stateProvince = stateTf.text
            userData?.country = countryTf.text
            
            makeHttpCall(.updatePersonalInfo, .put, userData?.toDictionary(), path:DefaultsManager.getUserId()){data in
                
                self.showDialog(message: "Update successful"){_ in
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }

    func fecthUserInfo(){

        makeHttpCall(.updatePersonalInfo, .get, path:DefaultsManager.getUserId()){data in
            self.userData = try? JSONDecoder().decode(ProfileUpdate.self, from: data).data
            
            self.firstNameTf.text = self.userData?.firstName
            self.lastNameTf.text = self.userData?.lastName
            self.emailTf.text = self.userData?.email
            self.phoneNumberTf.text = self.userData?.phoneNumber
            self.dateOfBirthTf.text = self.userData?.dob
            self.genderTf.text = self.userData?.gender
            self.addressTf.text = self.userData?.address
            self.cityTf.text = self.userData?.city
            self.stateTf.text = self.userData?.stateProvince
            self.countryTf.text = self.userData?.country
            
            if let displayImg =  self.userData?.userImageUrl, let url = URL(string: displayImg){
                self.userImg.kf.setImage(with: url)
            }
            
            self.userData?.fullName = nil
            self.userData?.referralCode = nil
        }
    }

    let countries = Locale.isoRegionCodes.compactMap { Locale.current.localizedString(forRegionCode: $0) }

    let states = [
      "", "Abia", "Adamawa", "Akwa Ibom", "Anambra", "Bauchi", "Bayelsa", "Benue", "Borno", "Cross River", "Delta", "Ebonyi", "Edo", "Ekiti", "Enugu", "FCT - Abuja", "Gombe", "Imo", "Jigawa", "Kaduna", "Kano", "Katsina", "Kebbi", "Kogi", "Kwara", "Lagos", "Nasarawa", "Niger", "Ogun", "Ondo", "Osun", "Oyo", "Plateau", "Rivers", "Sokoto", "Taraba", "Yobe", "Zamfara"
    ]
    
    @objc func statePicker(){
        let alert = UIAlertController(style: .actionSheet, title: "State", message: "Select your state")

        let pickerViewValues: [[String]] = [states.map {$0}]

        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: states.count)

        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            self.stateTf.text = self.states[index.row]
        }
        alert.addAction(title: "Done", style: .cancel)
        alert.show()
    }
    
    @objc func countryPicker(){
        let alert = UIAlertController(style: .actionSheet, title: "Country", message: "Select your country")

        let pickerViewValues: [[String]] = [countries.map {$0}]

        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: countries.count)

        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            self.countryTf.text = self.countries[index.row]
        }
        alert.addAction(title: "Done", style: .cancel)
        alert.show()
    }
    
    @objc func genderPicker(){
        let alert = UIAlertController(style: .actionSheet, title: "Gender", message: "Select your gender")

        let genders: [String] = ["", "Male", "Female"]
        let pickerViewValues: [[String]] = [genders.map {$0}]

        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: genders.count)

        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            self.genderTf.text = genders[index.row]
        }
        alert.addAction(title: "Done", style: .cancel)
        alert.show()
    }
    
    @objc func selectImage(){
        let vc = UIImagePickerController()
//        vc.sourceType = .camera
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        userImage = image
        uploadImg()
    }
 
    func uploadImg(){
        let indicator = activityIndicator
        indicator.startAnimating()
        
        let storageRef = Storage.storage().reference()
        
        let fileRef = storageRef.child("profile_image/\(DefaultsManager.getEmail()).jpg")

        fileRef.putData((userImage?.pngData())!, metadata: nil) { (metadata, error) in
            
            indicator.removeFromSuperview()
            
            fileRef.downloadURL { (url, error) in
                if let downloadURL = url {
//                    self.submitImg(url: "\(downloadURL)")
                    self.userData?.userImageUrl = "\(downloadURL)"
                    print("downloadURL \(downloadURL)")
                }
            }
        }
    }
    
    func submitImg(url:String){
        
    }
}
