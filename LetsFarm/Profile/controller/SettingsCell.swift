//
//  SettingsCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 25/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class SettingsCell: UICollectionViewCell {
    
    var menuItem:MenuItems?{
        didSet{
            itemLb.text = menuItem?.title
            itemImg.image = menuItem?.image
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        backgroundColor = #colorLiteral(red: 0.9359462857, green: 0.9402776361, blue: 0.9508653283, alpha: 1)
        
        layer.cornerRadius = 10
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)

        addConstraints(format: "H:|-20-[v0(25)]-30-[v1]", views: itemImg, itemLb)
        addConstraints(format: "V:[v0(25)]", views: [itemImg])
//        addConstraints(format: "H:|-5-[v0]-5-|", views: [itemLb])
        alignVertical(views: itemLb, itemImg)
    }
    
    
    
    lazy var itemImg:UIImageView={
        let imageView = UIImageView()
        imageView.setCornerRadius(radius: 6)
        return imageView
    }()
    
    lazy var itemLb:UILabel={
        let label = UILabel()
        label.text = "Items"
//        label.backgroundColor = UIColor.red
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
