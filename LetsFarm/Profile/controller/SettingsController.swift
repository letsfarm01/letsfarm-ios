//
//  SettingsController.swift
//  CleanAgent
//
//  Created by Dekola Ak on 15/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

class SettingsController: BaseController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.isNavigationBarHidden = false
//        tabBarController?.tabBar.isHidden = true
        
        navigationController?.isNavigationBarHidden = true
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        navigationItem.title = ""
        view.backgroundColor = .white
        
        view.addConstraints(format: "V:|-60-[v0]-100-[v1]-60-|", views: headerLb, menuCv)
//        view.addConstraints(formats: ["V:|-60-[v0]"], views: headerLb, logOutImg)
        logOutImg.alignTopToTopOf(of: headerLb)
        view.addConstraints(format: "H:|-15-[v0]-15-|", views: menuCv)
        
        view.addConstraints(format: "H:|-15-[v0]", views: headerLb)
        view.addConstraints(format: "H:[v0]-15-|", views: logOutImg)
    }
    
    lazy var headerLb = getLabel(text: "Account Settings", fontSize: 20, boldFont: true, textColor: .black)
    
    lazy var logOutImg = getImage(#imageLiteral(resourceName: "logout"), height: 30, width: 30, action: #selector(logOutClick))
    
    @objc func logOutClick(){
        self.logOut()
    }

    
    lazy var menuCv:UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 20
        flowLayout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SettingsCell.self, forCellWithReuseIdentifier: "settingsCell")
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "settingsCell", for: indexPath) as! SettingsCell
        cell.menuItem = menuItems[indexPath.item]
        cell.tag = indexPath.item
        cell.addGesture(UITapGestureRecognizer(target: self, action: #selector(onMenu(sender:))))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-30, height: 55)
    }
    
    
    @objc func onMenu(sender:UITapGestureRecognizer){
        let position = sender.view?.tag ?? 0
        var controller:UIViewController?
        switch position{
            case 0: controller = PersonalInfoController()
            case 1: controller = BankInfoController()
            case 2: controller = NextOfKinInfoController()
            case 3: controller = BusinessInfoController()
            case 4: controller = ChangePasswordController2()
            case 5: controller = ChangePasswordController2()
        default:
            print("Default")
        }
        if let controller = controller {
            navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    var menuItems:[MenuItems] = [MenuItems(image: #imageLiteral(resourceName: "settings"), title: "Personal details"), MenuItems(image: #imageLiteral(resourceName: "settings"), title: "Bank details"), MenuItems(image: #imageLiteral(resourceName: "settings"), title: "Next of kin info"), MenuItems(image: #imageLiteral(resourceName: "settings"), title: "Business info"), MenuItems(image: #imageLiteral(resourceName: "settings"), title: "Change password"), MenuItems(image: #imageLiteral(resourceName: "settings"), title: "Terms of use")]
    
    
}
