//
//  BanksRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 26/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct BanksRes : Codable {

    let code : Int?
    let data : [BankData]?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }

}

struct BankData : Codable {

    let active : Bool?
    let code : String?
    let country : String?
    let createdAt : String?
    let currency : String?
    let gateway : String?
    let id : Int?
    let longcode : String?
    let name : String?
    let payWithBank : Bool?
    let slug : String?
    let type : String?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case active = "active"
        case code = "code"
        case country = "country"
        case createdAt = "createdAt"
        case currency = "currency"
        case gateway = "gateway"
        case id = "id"
        case longcode = "longcode"
        case name = "name"
        case payWithBank = "pay_with_bank"
        case slug = "slug"
        case type = "type"
        case updatedAt = "updatedAt"
    }

}
