//
//  ProfileUpdateData.swift
//  LetsFarm
//
//  Created by Akano Adekola on 26/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct ProfileUpdate : Codable {

    let code : Int?
    let data : ProfileUpdateData?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }
    
}

struct ProfileUpdateData : Codable {

    var id : String?
    var accountName : String?
    var accountNumber : String?
    var address : String?
    var bankName : String?
    var bvn : String?
    var city : String?
    var companyAddress : String?
    var companyEmail : String?
    var companyName : String?
    var companyPhone : String?
    var country : String?
    var dob : String?
    var email : String?
    var firstName : String?
    var fullName : String?
    var gender : String?
    var lastName : String?
    var name : String?
    var phoneNumber : String?
    var referralCode : String?
    var relationship : String?
    var stateProvince : String?
    var userImageUrl : String?
    var userId : String?
    var username : String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case accountName = "account_name"
        case accountNumber = "account_number"
        case address = "address"
        case bankName = "bank_name"
        case bvn = "bvn"
        case city = "city"
        case companyAddress = "company_address"
        case companyEmail = "company_email"
        case companyName = "company_name"
        case companyPhone = "company_phone"
        case country = "country"
        case dob = "dob"
        case email = "email"
        case firstName = "first_name"
        case fullName = "full_name"
        case gender = "gender"
        case lastName = "last_name"
        case name = "name"
        case phoneNumber = "phone_number"
        case referralCode = "referral_code"
        case relationship = "relationship"
        case stateProvince = "state_province"
        case userImageUrl = "user_image_url"
        case userId = "userId"
        case username = "username"
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if id != nil{
            dictionary["_id"] = id
        }
        if accountName != nil{
            dictionary["account_name"] = accountName
        }
        if accountNumber != nil{
            dictionary["account_number"] = accountNumber
        }
        if address != nil{
            dictionary["address"] = address
        }
        if bankName != nil{
            dictionary["bank_name"] = bankName
        }
        if bvn != nil{
            dictionary["bvn"] = bvn
        }
        if city != nil{
            dictionary["city"] = city
        }
        if companyAddress != nil{
            dictionary["company_address"] = companyAddress
        }
        if companyEmail != nil{
            dictionary["company_email"] = companyEmail
        }
        if companyName != nil{
            dictionary["company_name"] = companyName
        }
        if companyPhone != nil{
            dictionary["company_phone"] = companyPhone
        }
        if country != nil{
            dictionary["country"] = country
        }
        if dob != nil{
            dictionary["dob"] = dob
        }
        if email != nil{
            dictionary["email"] = email
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if name != nil{
            dictionary["name"] = name
        }
        if phoneNumber != nil{
            dictionary["phone_number"] = phoneNumber
        }
        if referralCode != nil{
            dictionary["referral_code"] = referralCode
        }
        if relationship != nil{
            dictionary["relationship"] = relationship
        }
        if stateProvince != nil{
            dictionary["state_province"] = stateProvince
        }
        if userImageUrl != nil{
            dictionary["user_image_url"] = userImageUrl
        }
        if userId != nil{
            dictionary["userId"] = userId
        }
        if username != nil{
            dictionary["username"] = username
        }
        return dictionary
    }
}
