//
//  AllFarmRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct AllFarmRes : Codable {

    let code : Int?
    let data : AllFarmResData?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }


}


struct AllFarmResData : Codable {

    let data : [AllFarmResData2]?
    let limit : Int?
    let page : Int?
    let total : Int?

    enum CodingKeys: String, CodingKey {
        case data = "data"
        case limit = "limit"
        case page = "page"
        case total = "total"
    }


}

struct AllFarmResData2 : Codable {

    let v : Int?
    let id : String?
    let amountToInvest : Int?
    let closingDate : String?
    let createdAt : String?
    let currency : String?
    let deleted : Bool?
    let descriptionField : String?
    let displayImg : String?
    let duration : Int?
    let durationType : String?
    let farmLocation : String?
    let farmName : String?
    let farmStatus : FarmStatus?
    let farmType : FarmType?
    let openingDate : String?
    let percentageToGain : Int?
    let remainingInStock : Int?
    let roiInterval : Int?
    let totalInStock : Int?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case amountToInvest = "amount_to_invest"
        case closingDate = "closing_date"
        case createdAt = "createdAt"
        case currency = "currency"
        case deleted = "deleted"
        case descriptionField = "description"
        case displayImg = "display_img"
        case duration = "duration"
        case durationType = "duration_type"
        case farmLocation = "farm_location"
        case farmName = "farm_name"
        case farmStatus = "farm_status"
        case farmType = "farm_type"
        case openingDate = "opening_date"
        case percentageToGain = "percentage_to_gain"
        case remainingInStock = "remaining_in_stock"
        case roiInterval = "roi_interval"
        case totalInStock = "total_in_stock"
        case updatedAt = "updatedAt"
    }


}

struct FarmType : Codable {

    let v : Int?
    let id : String?
    let createdAt : String?
    let deleted : Bool?
    let name : String?
    let status : Bool?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case name = "name"
        case status = "status"
        case updatedAt = "updatedAt"
    }


}

struct FarmStatus : Codable {

    let v : Int?
    let id : String?
    let canSponsor : Bool?
    let createdAt : String?
    let deleted : Bool?
    let name : String?
    let status : Bool?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case canSponsor = "can_sponsor"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case name = "name"
        case status = "status"
        case updatedAt = "updatedAt"
    }


}
