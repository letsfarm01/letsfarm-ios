//
//  CheckOutRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct CheckOutRes : Codable {

    let code : Int?
    let data : CheckOutResData?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }
}

struct CheckOutResData : Codable {

    let amount : Int?
    let transactionId : String?
    let transactionRef : String?
    let authorizationUrl : String?
    let details : CheckOutResDetail?
    
    enum CodingKeys: String, CodingKey {
        case amount = "amount"
        case transactionId = "transactionId"
        case transactionRef = "transactionRef"
        case authorizationUrl = "authorization_url"
        case details = "details"
    }

}

struct CheckOutResDetail : Codable {

    let accessCode : String?
    let charge : Float?
    let email : String?
    let fullName : String?
    let ref : String?
    let subTotal : Int?
    let total : Float?
    let transactionId : String?

    enum CodingKeys: String, CodingKey {
        case accessCode = "access_code"
        case charge = "charge"
        case email = "email"
        case fullName = "full_name"
        case ref = "ref"
        case subTotal = "subTotal"
        case total = "total"
        case transactionId = "transactionId"
    }


}
