//
//  FarmStatusRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation


struct FarmStatusRes : Codable {

    let code : Int?
    let data : [FarmStatusResData]?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }

}
struct FarmStatusResData : Codable {

    let v : Int?
    let id : String?
    let canSponsor : Bool?
    let createdAt : String?
    let deleted : Bool?
    let name : String?
    let status : Bool?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case canSponsor = "can_sponsor"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case name = "name"
        case status = "status"
        case updatedAt = "updatedAt"
    }


}
