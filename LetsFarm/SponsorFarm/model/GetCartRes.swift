//
//  GetCartRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct GetCartRes : Codable {

    let code : Int?
    let data : [GetCartResData]?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }


}

struct GetCartResData : Codable {

    let v : Int?
    let id : String?
    let checkoutStatus : Bool?
    let createdAt : String?
    let deleted : Bool?
    let farmId : GetCartResFarmId?
    let quantity : Int?
    let totalPrice : Int?
    let updatedAt : String?
    let userId : GetCartResUserId?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case checkoutStatus = "checkout_status"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case farmId = "farmId"
        case quantity = "quantity"
        case totalPrice = "total_price"
        case updatedAt = "updatedAt"
        case userId = "userId"
    }


}

struct GetCartResUserId : Codable {

    let _id : String?
    let email : String?
    let firstName : String?
    let fullName : String?
    let id : String?
    let lastName : String?
    let phoneNumber : String?

    enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case email = "email"
        case firstName = "first_name"
        case fullName = "full_name"
        case id = "id"
        case lastName = "last_name"
        case phoneNumber = "phone_number"
    }


}

struct GetCartResFarmId : Codable {

    let v : Int?
    let id : String?
    let amountToInvest : Int?
    let closingDate : String?
    let createdAt : String?
    let currency : String?
    let deleted : Bool?
    let descriptionField : String?
    let displayImg : String?
    let duration : Int?
    let durationType : String?
    let farmLocation : String?
    let farmName : String?
//    let farmStatus : GetCartResFarmStatus?
//    let farmType : GetCartResFarmType?
    let openingDate : String?
    let percentageToGain : Int?
    let remainingInStock : Int?
    let totalInStock : Int?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case amountToInvest = "amount_to_invest"
        case closingDate = "closing_date"
        case createdAt = "createdAt"
        case currency = "currency"
        case deleted = "deleted"
        case descriptionField = "description"
        case displayImg = "display_img"
        case duration = "duration"
        case durationType = "duration_type"
        case farmLocation = "farm_location"
        case farmName = "farm_name"
//        case farmStatus = "farm_status"
//        case farmType = "farm_type"
        case openingDate = "opening_date"
        case percentageToGain = "percentage_to_gain"
        case remainingInStock = "remaining_in_stock"
        case totalInStock = "total_in_stock"
        case updatedAt = "updatedAt"
    }


}

//struct GetCartResFarmType : Codable {
//
//    let v : Int?
//    let id : String?
//    let createdAt : String?
//    let deleted : Bool?
//    let name : String?
//    let status : Bool?
//    let updatedAt : String?
//
//    enum CodingKeys: String, CodingKey {
//        case v = "__v"
//        case id = "_id"
//        case createdAt = "createdAt"
//        case deleted = "deleted"
//        case name = "name"
//        case status = "status"
//        case updatedAt = "updatedAt"
//    }
//
//
//}
//
//struct GetCartResFarmStatus : Codable {
//
//    let v : Int?
//    let id : String?
//    let canSponsor : Bool?
//    let createdAt : String?
//    let deleted : Bool?
//    let name : String?
//    let status : Bool?
//    let updatedAt : String?
//
//    enum CodingKeys: String, CodingKey {
//        case v = "__v"
//        case id = "_id"
//        case canSponsor = "can_sponsor"
//        case createdAt = "createdAt"
//        case deleted = "deleted"
//        case name = "name"
//        case status = "status"
//        case updatedAt = "updatedAt"
//    }
//
//}
