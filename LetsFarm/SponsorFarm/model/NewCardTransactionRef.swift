//
//  NewCardTransactionRef.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation


struct NewCardTransactionRef : Codable {

    let code : Int?
    let data : NewCardTransactionRefData?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }

}

struct NewCardTransactionRefData : Codable {

    let authorizationUrl : String?
    let details : NewCardTransactionRefDetail?

    enum CodingKeys: String, CodingKey {
        case authorizationUrl = "authorization_url"
        case details = "details"
    }

}

struct NewCardTransactionRefDetail : Codable {

    let accessCode : String?
    let charge : Int?
    let email : String?
    let fullName : String?
    let ref : String?
    let subTotal : Int?
    let total : Int?
    let transactionId : String?

    enum CodingKeys: String, CodingKey {
        case accessCode = "access_code"
        case charge = "charge"
        case email = "email"
        case fullName = "full_name"
        case ref = "ref"
        case subTotal = "subTotal"
        case total = "total"
        case transactionId = "transactionId"
    }


}
