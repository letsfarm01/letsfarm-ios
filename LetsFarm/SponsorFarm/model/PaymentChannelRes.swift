//
//  PaymentChannelRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct PaymentChannelRes : Codable {

    let code : Int?
    let data : [PaymentChannelResData]?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }

}

struct PaymentChannelResData : Codable {

    let v : Int?
    let id : String?
    let canFund : Bool?
    let createdAt : String?
    let deleted : Bool?
    let descriptionField : String?
    let isActive : Bool?
    let name : String?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case canFund = "can_fund"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case descriptionField = "description"
        case isActive = "is_active"
        case name = "name"
        case updatedAt = "updatedAt"
    }

}
