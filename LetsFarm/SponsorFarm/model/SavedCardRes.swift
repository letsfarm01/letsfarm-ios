//
//  SavedCardRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct SavedCardRes : Codable {

    let code : Int?
    let data : [SavedCardResData]?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }

}

struct SavedCardResData : Codable {

    let v : Int?
    let id : String?
    let authorizationCode : String?
    let bank : String?
    let bin : String?
    let cardType : String?
    let channel : String?
    let countryCode : String?
    let createdAt : String?
    let deleted : Bool?
    let expMonth : Int?
    let expYear : Int?
    let last4 : String?
    let reusable : Bool?
    let signature : String?
    let updatedAt : String?
    let userId : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case authorizationCode = "authorization_code"
        case bank = "bank"
        case bin = "bin"
        case cardType = "card_type"
        case channel = "channel"
        case countryCode = "country_code"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case expMonth = "exp_month"
        case expYear = "exp_year"
        case last4 = "last4"
        case reusable = "reusable"
        case signature = "signature"
        case updatedAt = "updatedAt"
        case userId = "userId"
    }


}
