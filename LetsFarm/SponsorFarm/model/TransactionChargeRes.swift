//
//  TransactionChargeRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct TransactionChargeRes : Codable {

    let code : Int?
    let data : TransactionChargeResData?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }


}

struct TransactionChargeResData : Codable {

    let charge : Float?
    let subTotal : Float?
    let total : Float?

    enum CodingKeys: String, CodingKey {
        case charge = "charge"
        case subTotal = "subTotal"
        case total = "total"
    }


}
