//
//  CartCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class CartCell: UICollectionViewCell {
    
    var deleteCart:((String)->Void)?
    
    var farm : GetCartResData?{
        didSet{
            farmNameLb.text = farm?.farmId?.farmName
            roiLb.text = "\(farm?.farmId?.percentageToGain ?? 0)% ROI in \(farm?.farmId?.duration ?? 0) \(farm?.farmId?.durationType ?? "-")"
            amountLb.text = "₦\(farm?.farmId?.amountToInvest?.withCommas() ?? "0") per unit"
            locationLb.text = farm?.farmId?.farmLocation
            totalAmountLb.text = "₦\(farm?.totalPrice?.withCommas() ?? "0") in total"
            quantityLb.text = "\(farm?.quantity ?? 0) units"

            if let displayImg = farm?.farmId?.displayImg, let url = URL(string: displayImg){
                farmImg.kf.setImage(with: url)
            }
            
            
        }
    }


    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        setShadow()

        layer.cornerRadius = 7
        
        addConstraints(formats: ["H:[v0]-10-|", "V:|-10-[v0]"], views: deleteImg)
        
        addConstraints(format: "V:|-30-[v0(120)]-30-|", views: farmImg)
        addConstraints(format: "H:|-10-[v0]-10-[v1]-10-[v2]", views: farmImg, roiImg, roiLb)
        addConstraints(format: "H:|-10-[v0]-10-[v1]-10-[v2]", views: farmImg, amountImg, amountLb)
        addConstraints(format: "H:|-10-[v0]-10-[v1]-10-[v2]", views: farmImg, locationImg, locationLb)
        addConstraints(format: "H:|-10-[v0]-10-[v1]-10-[v2]", views: farmImg, totalAmountImg, totalAmountLb)
        addConstraints(format: "H:|-10-[v0]-10-[v1]-10-[v2]", views: farmImg, quantityImg, quantityLb)
        addConstraints(format: "H:|-10-[v0]-15-[v1]", views: farmImg, farmNameLb)

        addConstraints(format: "V:|-15-[v0]-10-[v1]-13-[v2]-13-[v3]-13-[v4]-13-[v5]", views: farmNameLb, roiImg, amountImg, locationImg, totalAmountImg, quantityImg)
        addConstraints(format: "V:|-15-[v0]-10-[v1]-13-[v2]-13-[v3]-13-[v4]-13-[v5]", views: farmNameLb, roiLb, amountLb, locationLb, totalAmountLb, quantityLb)

    }
    
    lazy var farmImg = getImage(#imageLiteral(resourceName: "add"), height: 120, width: 120)
    lazy var deleteImg = getImage(#imageLiteral(resourceName: "delete"), height: 25, width: 25, action:#selector(deleteItem))

    lazy var roiImg = getImage(#imageLiteral(resourceName: "roi"), height: 15, width: 15)
    lazy var amountImg = getImage(#imageLiteral(resourceName: "wallet"), height: 15, width: 15)
    lazy var locationImg = getImage(#imageLiteral(resourceName: "location"), height: 15, width: 15)
    lazy var totalAmountImg = getImage(#imageLiteral(resourceName: "wallet"), height: 15, width: 15)
    lazy var quantityImg = getImage(#imageLiteral(resourceName: "location"), height: 15, width: 15)

    lazy var farmNameLb = getLabel(text: "Cucumber Farm", fontSize: 15, boldFont: true, textColor: .black)
    lazy var roiLb = getLabel(text: "20% ROI in 6 months", fontSize: 12)
    lazy var amountLb = getLabel(text: "N50,000", fontSize: 12)
    lazy var locationLb = getLabel(text: "Ogun state", fontSize: 12)
    lazy var totalAmountLb = getLabel(text: "N50,000", fontSize: 12)
    lazy var quantityLb = getLabel(text: "2", fontSize: 12)

    @objc func deleteItem(){
        deleteCart!(farm!.id!)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
