//
//  FarmStatus.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class FarmStatusCell: UICollectionViewCell {
    
    var farmStatus : FarmStatusResData?{
        didSet{
            statusLb.text = farmStatus?.name
        }
    }
    var currentSelected : Bool?{
        didSet{
            if currentSelected ?? false{
                statusLb.textColor = .white
                backgroundColor = #colorLiteral(red: 0.2275, green: 0.2471, blue: 0.361, alpha: 1)
            }else{
                statusLb.textColor = .black
                backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)

        layer.cornerRadius = 5
        
        addConstraints(formats: ["V:|-10-[v0]-10-|", "H:|-10-[v0]-10-|"], views: statusLb)
        
    }
    
    lazy var statusLb = getLabel(text: "Cucumber Farm", fontSize: 14, boldFont: true,textColor: .black)
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
