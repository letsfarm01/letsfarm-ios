//
//  FarmsCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class FarmsCell: UICollectionViewCell {
    
    var farm : AllFarmResData2?{
        didSet{
            farmNameLb.text = farm?.farmName
            roiLb.text = "\(farm?.percentageToGain ?? 0)% ROI in \(farm?.duration ?? 0) \(farm?.durationType ?? "-")"
            amountLb.text = "₦\(farm?.amountToInvest?.withCommas() ?? "0") per unit"
            locationLb.text = farm?.farmLocation
            let buttonText = farm?.farmStatus?.canSponsor ?? false ? "Available" : "Not Available"
            farmBtn.setTitle(buttonText, for: .normal)
            
            if let displayImg = farm?.displayImg, let url = URL(string: displayImg){
                farmImg.kf.setImage(with: url)
            }
            
            
        }
    }


    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        setShadow()

        layer.cornerRadius = 7
        
        addConstraints(format: "V:|-30-[v0(120)]-30-|", views: farmImg)
        addConstraints(format: "H:|-10-[v0]-10-[v1]-10-[v2]", views: farmImg, roiImg, roiLb)
        addConstraints(format: "H:|-10-[v0]-10-[v1]-10-[v2]", views: farmImg, amountImg, amountLb)
        addConstraints(format: "H:|-10-[v0]-10-[v1]-10-[v2]", views: farmImg, locationImg, locationLb)
        addConstraints(format: "H:|-10-[v0]-10-[v1(130)]", views: farmImg, farmBtn)
        addConstraints(format: "H:|-10-[v0]-15-[v1]", views: farmImg, farmNameLb)

        addConstraints(format: "V:|-15-[v0]-20-[v1]-13-[v2]-13-[v3]", views: farmNameLb, roiImg, amountImg, locationImg)
        addConstraints(format: "V:|-15-[v0]-20-[v1]-13-[v2]-13-[v3]-13-[v4(30)]", views: farmNameLb, roiLb, amountLb, locationLb, farmBtn)

    }
    
    lazy var farmImg = getImage(#imageLiteral(resourceName: "add"), height: 120, width: 120)
    
    lazy var roiImg = getImage(#imageLiteral(resourceName: "roi"), height: 15, width: 15)
    lazy var amountImg = getImage(#imageLiteral(resourceName: "wallet"), height: 15, width: 15)
    lazy var locationImg = getImage(#imageLiteral(resourceName: "location"), height: 15, width: 15)

    lazy var farmNameLb = getLabel(text: "Cucumber Farm", fontSize: 15, boldFont: true, textColor: .black)
    lazy var roiLb = getLabel(text: "20% ROI in 6 months", fontSize: 12)
    lazy var amountLb = getLabel(text: "N50,000", fontSize: 12)
    lazy var locationLb = getLabel(text: "Ogun state", fontSize: 12)
    lazy var farmBtn = getButton(title: "Avaliable", bgColor: .white, borderColor: .primaryColour, titleColor: .primaryColour, cornerRadius: 7)

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
