//
//  PaymentCardCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 17/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class PaymentCardCell: UICollectionViewCell {
    
    var paymentCardsRes:SavedCardResData?{
        didSet{
            cardNumberLb.text = "\(paymentCardsRes!.bin!) **** **** **** \(paymentCardsRes!.last4!)"
            cardTypeLb.text = paymentCardsRes?.cardType
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.9325684905, green: 0.9270246625, blue: 0.9368299246, alpha: 1)
                

        addConstraints(format: "H:|-10-[v0]-10-|", views: cardNumberLb)
        addConstraints(format: "H:|-10-[v0]", views: cardTypeLb)
        addConstraints(format: "V:|-10-[v0]-10-[v1]-10-|", views: cardNumberLb, cardTypeLb)

    }
    
    lazy var cardNumberLb:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    lazy var cardTypeLb:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
