//
//  PaymentOptionCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class PaymentOptionCell: UICollectionViewCell {
    
    var cardCharges:TransactionChargeResData?
    var totalPrice:Int?
    var paymentOptions:PaymentChannelResData?{
        didSet{
            self.descriptionLb.text = paymentOptions?.descriptionField
            
            if paymentOptions?.name?.lowercased().contains("wallet") ?? false{
                balanceTitleLb.text = "Wallet balance:"
                balanceLb.text = "₦\(walletBalance?.withCommas() ?? "0")"
                
                totalTitleLb.text = "Total amount:"
                totalLb.text = "₦\(totalPrice?.withCommas() ?? "0")"
            }else if paymentOptions?.name?.lowercased() == "paystack" {
                balanceTitleLb.text = "Card charges:"
                balanceLb.text = "₦\(cardCharges?.charge?.withCommas() ?? "0")"
                
                totalTitleLb.text = "Total amount:"
                totalLb.text = "₦\(cardCharges?.total?.withCommas() ?? "0")"
            }else{
                balanceTitleLb.text = "Charges:"
                balanceLb.text = "₦0"
                
                totalTitleLb.text = "Total amount:"
                totalLb.text = "₦\(totalPrice?.withCommas() ?? "0")"
            }
        }
    }
    var walletBalance:Int?
    var paymentOptionSelection:((PaymentChannelResData)->Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.8079474568, green: 0.803145647, blue: 0.8116392493, alpha: 1)
        
        isUserInteractionEnabled = true
        addGesture(UITapGestureRecognizer(target: self, action: #selector(onOptionTap)))
        
        addConstraints(format: "H:|-10-[v0]-10-|", views: descriptionLb)
        addConstraints(format: "H:|-10-[v0]-5-[v1]", views: balanceTitleLb, balanceLb)
        addConstraints(format: "H:|-10-[v0]-5-[v1]", views: totalTitleLb, totalLb)
        addConstraints(format: "V:|-10-[v0]-5-[v1]-5-[v2]-10-|", views: descriptionLb, balanceTitleLb, totalTitleLb)
        addConstraints(format: "V:|-10-[v0]-5-[v1]-5-[v2]-10-|", views: descriptionLb, balanceLb, totalLb)

        self.descriptionLb.textAlignment = .left
        
        
    }
    
    lazy var descriptionLb = getLabel(text: "scsdvsvsdvs vsdvsvsv vsdvsvsvds", fontSize: 13, boldFont: true, textColor: .black)
    lazy var balanceTitleLb = getLabel(text: "Wallet Balance:", fontSize: 13, textColor: .black)
    lazy var balanceLb = getLabel(text: "N233,232", fontSize: 13, textColor: #colorLiteral(red: 0.7354474664, green: 0.1774312258, blue: 0.1072754338, alpha: 1))
    lazy var totalTitleLb = getLabel(text: "Wallet Balance:", fontSize: 13, textColor: .black)
    lazy var totalLb = getLabel(text: "N233,232", fontSize: 13, textColor: #colorLiteral(red: 0.7354474664, green: 0.1774312258, blue: 0.1072754338, alpha: 1))
    
    @objc func onOptionTap(){
        paymentOptionSelection!(paymentOptions!)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
