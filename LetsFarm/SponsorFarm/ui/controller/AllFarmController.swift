//
//  SponsorFarm.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class AllFarmController: BaseController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    var allFarms : [AllFarmResData2]?
    var selectedFarms : [AllFarmResData2]?
    
    var allFarmStatus : [FarmStatusResData]?
    var selectedFarmStatus : FarmStatusResData?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
        
        fetchCartCount()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationItem.title = "Sponsor Farm"
        
        view.addConstraints(format: "H:|-10-[v0]-10-[v1]-10-|", views: farmStatusCv, cartImg)
        view.addConstraints(format: "H:[v0]-15-|", views: cartImg)
        view.addConstraints(format: "H:|[v0]|", views: [farmsCv])

        view.addConstraints(format: "V:|-110-[v0(50)]-30-[v1]-25-|", views: farmStatusCv, farmsCv)
        view.addConstraints(format: "V:|-115-[v0]", views: cartImg)

        view.addConstraints(format: "V:|-120-[v0(20)]", views: cartCountLb)
        view.addConstraints(format: "H:[v0(20)]-10-|", views: cartCountLb)

        cartCountLb.layer.cornerRadius = 11
        cartCountLb.clipsToBounds = true

        farmStatusCv.showsHorizontalScrollIndicator = false
        farmStatusCv.showsVerticalScrollIndicator = false
        
        fetchFarms()
        fetchStatus()
        
    }
    
    lazy var cartImg = getImage(#imageLiteral(resourceName: "cart"), height: 30, width: 30, action: #selector(showCart))
    lazy var cartCountLb = getLabel(text: "0", textColor: .white, backgroundColour: .primaryColour, action: #selector(showCart))

    lazy var farmStatusCv = getCollectionView(spacing: 10, cell: FarmStatusCell.self, identifier: "farmStatusCell", delegate: self, dataSource: self, scrollDirection: .horizontal)
    
    lazy var farmsCv = getCollectionView(spacing: 20, cell: FarmsCell.self, identifier: "farmCell", delegate: self, dataSource: self)

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == farmStatusCv {
            return CGSize(width: 130, height: 50)
        }
        else{
            return CGSize(width: view.frame.width - 40, height: 180)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == farmStatusCv {
            return allFarmStatus?.count ?? 0
        }
        else{
            return selectedFarms?.count ?? 0
        }
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let position = indexPath.item
        if collectionView == farmStatusCv {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "farmStatusCell", for: indexPath) as! FarmStatusCell
            cell.currentSelected = currentSelectedPosition == position
            cell.tag = position
            cell.farmStatus = allFarmStatus![position]
            cell.addGesture(UITapGestureRecognizer(target: self, action: #selector(onStatusCellTap(sender:))))
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "farmCell", for: indexPath) as! FarmsCell
            cell.tag = position
            cell.addGesture(UITapGestureRecognizer(target: self, action: #selector(onFarmCellTap(sender:))))
            cell.farm = self.selectedFarms![indexPath.item]
            return cell
        }
    }
    
    var currentSelectedPosition = -1
    
    @objc func onStatusCellTap(sender:UITapGestureRecognizer){
        currentSelectedPosition = sender.view?.tag ?? 0
        
        self.selectedFarmStatus = self.allFarmStatus![currentSelectedPosition]

        selectedFarms = getFilteredFarms(self.selectedFarmStatus!)
        
        self.farmStatusCv.reloadData()
        self.farmsCv.reloadData()
    }
    
    @objc func onFarmCellTap(sender:UITapGestureRecognizer){
        
        let selectedFarm = self.allFarms![sender.view?.tag ?? 0]

        if selectedFarm.farmStatus?.canSponsor ?? false{
            
            let controller = SponsorFarmController()
            controller.farm = selectedFarm
            
            navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    func getFilteredFarms(_ selectedFarmStatus:FarmStatusResData)->[AllFarmResData2]{
        
        var filteredFarms = [AllFarmResData2]()
        
        for farm in allFarms! {
            if farm.farmStatus?.name == selectedFarmStatus.name {
                filteredFarms.append(farm)
            }
        }
        
        return filteredFarms
        
    }
    
    func fetchFarms(){
        self.makeHttpCall(.fetchFarms, .get){data in
            self.allFarms = try? JSONDecoder().decode(AllFarmRes.self, from: data).data?.data
            self.selectedFarms = self.allFarms

            self.farmsCv.reloadData()

        }
    }

    var cartCount:Int?
    
    func fetchCartCount(){
        self.makeHttpCall(.getCartCount, .get, path:DefaultsManager.getUserId()){data in
            self.cartCount = try? JSONDecoder().decode(CartCount.self, from: data).data
            self.cartCountLb.text = "\(self.cartCount ?? 0)"
        }
    }
    func fetchStatus(){
        self.makeHttpCall(.fetchFarmsStatus, .get){data in
            self.allFarmStatus = try? JSONDecoder().decode(FarmStatusRes.self, from: data).data
            
            self.farmStatusCv.reloadData()
        }
    }
    
    @objc func showCart(){
        if cartCount > 0 {
            navigationController?.pushViewController(CartController(), animated: true)
        }else{
            showDialog(message: "No item in cart")
        }
        
    }
    
}
