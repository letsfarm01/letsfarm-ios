//
//  CardSelectionController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 17/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class CardSelectionController: BaseController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate   {
    
    var amount:Int?
    var newCardListener:(()->Void)?
    var savedCardListener:((String)->Void)?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true

        view.backgroundColor = .white
//        view.backgroundColor = UIColor.red
        navigationItem.title = "Select a card"
        
        view.addConstraints(formats: ["V:|-15-[v0(250)]-15-|", "H:|-15-[v0]-15-|"], view: addSv)
        
        self.noCardLb.isHidden = false
        self.cardCv.isHidden = true
        fetchCards()
    }
    
    var paymentCardsRes:[SavedCardResData]?
    
    func fetchCards(){
        makeHttpCall(.getSavedCard, .get, path:DefaultsManager.getUserId()){data in
            do{
                self.paymentCardsRes = try JSONDecoder().decode(SavedCardRes.self, from: data).data

                if self.paymentCardsRes != nil && !self.paymentCardsRes!.isEmpty{
                    self.cardCv.isHidden = false
                    self.noCardLb.isHidden = true
                }
                
                self.cardCv.reloadData()
            }catch{
                
            }
        }
    }
    
    lazy var selectCardLb = getLabel(text: "Select a card", fontSize: 14, textColor: .black)
    lazy var noCardLb = getLabel(text: "No card found", fontSize: 20, textColor: .black)

    lazy var cardCv = getCollectionView(spacing: 12, cell: PaymentCardCell.self, identifier: "paymentCardCell", delegate: self, dataSource: self)
    
    lazy var addSv = getStackView(selectCardLb, cardCv, noCardLb, newCardBtn)
    
    lazy var newCardBtn = getButton(title: "Pay with a new card", bgColor: .white, borderColor: .primaryColour, titleColor: .primaryColour, cornerRadius: 7, action: #selector(addNewCard))

    @objc func addNewCard(){
        newCardListener?()
        self.dismiss(animated: true)
    }
     
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paymentCardsRes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paymentCardCell", for: indexPath) as! PaymentCardCell
        cell.tag = indexPath.item
        cell.paymentCardsRes = paymentCardsRes![indexPath.item]
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(numberPadTap(sender:)))
        cell.addGesture(tapGesture)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-40, height: 60)
    }
    
    @objc func numberPadTap(sender:UITapGestureRecognizer){
        let tag = sender.view!.tag
        let selectedCardId = paymentCardsRes![tag].id!
        savedCardListener?(selectedCardId)
        self.dismiss(animated: true)
    }
}
