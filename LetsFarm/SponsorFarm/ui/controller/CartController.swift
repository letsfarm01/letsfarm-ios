//
//  CartController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog

class CartController: BaseController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var cardCharges:TransactionChargeResData?
    var paymentOptions:[PaymentChannelResData]?
    var walletBalance:Int?

    var cartItems : [GetCartResData]?
    var totalPrice:Int?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true

        view.backgroundColor = .white
        navigationItem.title = "Cart"
     
        view.addConstraints(format: "H:|[v0]|", views: [farmsCv])
        view.addConstraints(format: "H:|-15-[v0]-15-|", views: [checkoutBtn])
        view.addConstraints(format: "H:|-15-[v0]", views: [totalAmountTitleLb, totalAmountLb])
        
        view.addConstraints(format: "V:|-110-[v0]-10-[v1]-25-[v2]-15-[v3]-20-|", views: totalAmountTitleLb, totalAmountLb, farmsCv, checkoutBtn)

        fetchWalletBalance()
        fetchCart()
        fetchPaymentOptions()
    }
    
    lazy var totalAmountTitleLb = getLabel(text: "Total Amount:", fontSize: 17, textColor: .primaryColour)
    lazy var totalAmountLb = getLabel(text: "₦--,---", fontSize: 20, boldFont: true, textColor: .black)
    
    lazy var farmsCv = getCollectionView(spacing: 20, cell: CartCell.self, identifier: "cartCell", delegate: self, dataSource: self)

    lazy var checkoutBtn = getButton(title: "Check Out", action: #selector(showPaymentOption))

    @objc func fetchCart(){
        makeHttpCall(.getCart, .get, path:DefaultsManager.getUserId()){data in
            self.cartItems = try? JSONDecoder().decode(GetCartRes.self, from: data).data
            
            if self.cartItems == nil || self.cartItems?.count == 0{
                self.navigationController?.popViewController(animated: true)
            }else{
            
                self.farmsCv.reloadData()
                
                self.totalPrice = self.getTotalPrice(self.cartItems!)
                self.totalAmountLb.text = "₦\(self.totalPrice?.withCommas() ?? "0")"
                
                self.getCardPaymentCharges(self.totalPrice!)
            }
        }
    }

    func fetchWalletBalance(){
        makeHttpCall(.getWalletBalance, .get, path:DefaultsManager.getUserId()){data in
            self.walletBalance = try? JSONDecoder().decode(WalletBalanceRes.self, from: data).data?.walletBalance

        }
    }
    func getCardPaymentCharges(_ totalPrice:Int){
        
        let data = ["amount":totalPrice]
        
        makeHttpCall(.getCardPaymentCharges, .post, data){data in
            self.cardCharges = try? JSONDecoder().decode(TransactionChargeRes.self, from: data).data
        }
    }
    

    func fetchPaymentOptions(){
        
        makeHttpCall(.getPaymentOptionCart, .get){data in
            self.paymentOptions = try? JSONDecoder().decode(PaymentChannelRes.self, from: data).data
        }
    }
    
    
    func getTotalPrice(_ cartItems : [GetCartResData])->Int{
        var totalPrice = 0
        
        for cart in cartItems{
            totalPrice += cart.totalPrice ?? 0
        }
        
        return totalPrice
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 40, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cartItems?.count ?? 0
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let position = indexPath.item
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cartCell", for: indexPath) as! CartCell
        cell.tag = position
        cell.deleteCart = deleteCart
//        cell.addGesture(UITapGestureRecognizer(target: self, action: #selector(onFarmCellTap(sender:))))
        cell.farm = self.cartItems![indexPath.item]
        return cell        
    }
    
    func deleteCart(_ id:String){
        
        makeHttpCall(.cart, .delete, path:id){data in
            self.fetchCart()
        }
        
    }
    
    @objc func showPaymentOption(){
        if (paymentOptions == nil){
            fetchPaymentOptions()
            return
        }

        if let totalPrice = totalPrice, cardCharges == nil{
            getCardPaymentCharges(totalPrice)
            return
        }
        
//        let controller = CardSelectionController()
//        controller.savedCardListener = {cardId in
//            self.checkOutSavedCard(cardId, self.paymentOptions![1])
//        }
//        controller.newCardListener = {
//            self.checkOut(self.paymentOptions![1])
//        }
//
//        let popup = PopupDialog(viewController: controller, transitionStyle: .bounceDown, tapGestureDismissal: true, panGestureDismissal: false)
//
//        present(popup, animated: true, completion: nil)
        
        
        let controller = PaymentOptionController()

        controller.paymentOptionSelection = paymentOptionSelection

        controller.walletBalance = walletBalance
        controller.paymentOptions = paymentOptions
        controller.cardCharges = cardCharges
        controller.totalPrice = totalPrice

        let popup = PopupDialog(viewController: controller, transitionStyle: .bounceDown, tapGestureDismissal: true, panGestureDismissal: false)

        present(popup, animated: true, completion: nil)
    }
    
    func paymentOptionSelection(_ paymentChannelResData:PaymentChannelResData){
        if (paymentChannelResData.name == "Paystack"){

            let controller = CardSelectionController()
            controller.savedCardListener = {cardId in
                self.checkOutSavedCard(cardId, paymentChannelResData)
            }
            controller.newCardListener = {
                self.checkOut(paymentChannelResData)
            }

            let popup = PopupDialog(viewController: controller, transitionStyle: .bounceDown, tapGestureDismissal: true, panGestureDismissal: false)
            
            present(popup, animated: true, completion: nil)
            
        }else{
            checkOut(paymentChannelResData)
        }
    }
    
    func checkOutSavedCard(_ cardId:String, _ paymentChannelResData:PaymentChannelResData){
//        {"card_id":"5edc2a26bb242f2b287c6205","payment_gateway":"5edc0b7cc9567500173af059","userId":"5edc08a1c9567500173af050"}
        let dict = ["card_id":cardId, "payment_gateway":paymentChannelResData.id!, "userId":DefaultsManager.getUserId()]
        makeHttpCall(.checkoutSavedCard, .post, dict){data in
            self.showSuccess()
        }
    }
    
    func checkOut(_ paymentChannelResData:PaymentChannelResData){
        
        let dict = ["payment_gateway":paymentChannelResData.id!]
        
        makeHttpCall(.checkout, .post, dict){data in
            
            let checkOutRes = try? JSONDecoder().decode(CheckOutRes.self, from: data)
            
            if paymentChannelResData.name == "Paystack"{
                
                if let details = checkOutRes?.data?.details{
                    let controller = CheckOutNewCardController()
                    
                    controller.isWalletFunding = false
                    controller.ref = details.ref
                    controller.accessCode = details.accessCode
                    controller.transactionId = details.transactionId
                    controller.totalPrice = details.total
                    
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                
            }else if paymentChannelResData.name == "Bank Transfer" {
                self.showDialog(message: "Transaction Successfull", title: "Success", disableBackgroundTap: true){_ in
                    
                    let controller = UploadPaymentProofController()
                    controller.transactionId = checkOutRes?.data?.transactionId
                    controller.transactionRef = checkOutRes?.data?.transactionRef
                    controller.amount = checkOutRes?.data?.amount
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                
            }else if paymentChannelResData.name == "e-Wallet" {
                self.showSuccess()
            }else{
                self.showSuccess()
            }
        }
    }
    
    func showSuccess(){
        self.showDialog(message: "Transaction Successfull", title: "Success", disableBackgroundTap: true){_ in
            let scene = UIApplication.shared.connectedScenes.first
            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                sd.changeRootViewContoller(TabBarController())
            }
        }
    }
    

}
