//
//  CheckOutNewCardController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
import Paystack

class CheckOutNewCardController: BaseController {
    
    var isWalletFunding:Bool?
    var ref:String?
    var accessCode:String?
    var transactionId:String?
    var totalPrice:Float?
    

    var requestId:String?
//    var listener:CardListener?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = "Card Details"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monthTf.addTarget(self, action: #selector(editingChanged(sender:)), for: .editingChanged)
        yearTf.addTarget(self, action: #selector(editingChanged(sender:)), for: .editingChanged)
        cvvTf.addTarget(self, action: #selector(editingChangedCvv(sender:)), for: .editingChanged)

        view.backgroundColor = .white
        
        view.addConstraints(format: "V:|-110-[v0]-40-[v1]-20-[v2]-50-[v3]", views: titleLb, cardNumberTf, monthTf, makePaymentBtn)
        
        view.addConstraints(format: "H:|-20-[v0]", views: titleLb)
        view.addConstraints(format: "H:|-20-[v0(90)]-20-[v1(90)]-20-[v2]-20-|", views: monthTf, yearTf, cvvTf)

        yearTf.alignBottomToBottomOf(of: monthTf)
        cvvTf.alignBottomToBottomOf(of: monthTf)

        view.addConstraints(format: "H:|-20-[v0]-20-|", views: [cardNumberTf, makePaymentBtn])
        
//        4084084084084081
//        5060666666666666666
        
        cardNumberTf.text = "5060666666666666666"
        monthTf.text = "12"
        yearTf.text = "22"
        cvvTf.text = "123"
    }
    
    @objc private func editingChangedCvv(sender: UITextField) {
        if let text = sender.text, text.count >= 3 {
            sender.text = String(text.dropLast(text.count - 3))
            return
        }
    }
    @objc private func editingChanged(sender: UITextField) {
        if let text = sender.text, text.count >= 2 {
            sender.text = String(text.dropLast(text.count - 2))
            return
        }
    }
    
    lazy var titleLb = getLabel(text: "Input your card details", fontSize: 17, textColor: .black)
    
    lazy var cardNumberTf = getLabelledTextField(placeholder: "Card Number", labelText: "Card Number", keyboardType: .numberPad)
    lazy var monthTf = getLabelledTextField(placeholder: "mm", labelText: "Exp Month", keyboardType: .numberPad)
    lazy var yearTf = getLabelledTextField(placeholder: "yy", labelText: "Exp Year", keyboardType: .numberPad)
    lazy var cvvTf = getLabelledTextField(placeholder: "cvv", labelText: "Cvv", keyboardType: .numberPad)
    
    lazy var makePaymentBtn = getButton(title: "Make Payment", action: #selector(makePayment))
        
    lazy var saveCardSt : UISwitch = {
        let mSwitch = UISwitch(frame:CGRect(x: 10, y: 10, width: 10, height: 10))
        mSwitch.setOn(false, animated: false)
        return mSwitch
    }()

    lazy var saveCardLb = getLabel(text:  "Save this card for subsequest transactions", fontSize: 14, textColor: .black)
    
    @objc func makePayment(){
        if cardNumberTf.isValid(16) && isCardValid() && cvvTf.isValid(3) {
            let cardParams:PSTCKCardParams = PSTCKCardParams.init();
             // then set parameters thus from card
            cardParams.number = cardNumberTf.text!
             cardParams.cvc = cvvTf.text!
            cardParams.expYear = UInt(Int(yearTf.text!)!)
            cardParams.expMonth = UInt(Int(monthTf.text!)!)

            charge(cardParams)
        }
    }
    
    func charge(_ cardParams:PSTCKCardParams) {
        
        self.activityIndicator.startAnimating()
        
        let transactionParams = PSTCKTransactionParams.init()
        transactionParams.amount = UInt(totalPrice!)
        transactionParams.access_code = accessCode ?? ""
        transactionParams.reference = ref
        
        transactionParams.email = DefaultsManager.getEmail()

        PSTCKAPIClient.shared().chargeCard(cardParams, forTransaction: transactionParams, on: self,
                   didEndWithError: { (error, reference) -> Void in
//                    handleError(error)
                    print("didEndWithError")
                    self.activityIndicator.stopAnimating()
                    self.showDialog(message: "\(error.localizedDescription)")
                }, didRequestValidation: { (reference) -> Void in
                    // an OTP was requested, transaction has not yet succeeded
                    print("didRequestValidation")
                }, didTransactionSuccess: { (reference) -> Void in
                    // transaction may have succeeded, please verify on backend
                    print("didTransactionSuccess")
                    if self.isWalletFunding!{
                        self.verifyWalletTransaction()
                    }else{
                        self.verifyTransaction()
                    }
                    
            })
    }
    
    func verifyTransaction(){
        let dict = ["transactionId":transactionId!]
        makeHttpCall(.verifyTransaction, .post, dict){data in
            self.showDialog(message: "Transaction Successfull", title: "Success", disableBackgroundTap: true){_ in
                let scene = UIApplication.shared.connectedScenes.first
                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                    sd.changeRootViewContoller(TabBarController())
                }
            }
        }
    }
    
    func verifyWalletTransaction(){
        let dict = ["transactionId":transactionId!]
        makeHttpCall(.verifyWalletTransaction, .post, dict){data in
            self.showDialog(message: "Transaction Successfull", title: "Success", disableBackgroundTap: true){_ in                
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
    
    func isCardValid()->Bool{
        return isExpiryMonthValid() && isExpiryYearValid() && isCardExpired()
    }
    
    func isCardExpired()->Bool{
        let calendar = Calendar.current
        var dateComponents: DateComponents? = calendar.dateComponents([.hour, .minute, .second], from: Date())
        dateComponents?.month = Int(monthTf.text!)!
        dateComponents?.year = Int(yearTf.text!)! + 2000

        let date: Date? = calendar.date(from: dateComponents!)
        
        let currentDate = Date()
                
        let valid = date! > currentDate
        
        if !valid{
            showDialog(message: "Invalid card")
        }
        
        return valid
    }
    
    func isExpiryMonthValid()->Bool{
        let month = Int(monthTf.text!)!
        let valid = (1...12).contains(month)
        
        if !valid{
            showDialog(message: "Invalid month")
        }
        
        return valid
    }
    func isExpiryYearValid()->Bool{
        let lastTwoDigitsOfCurrentYear: Int =  Calendar.current.component(.year, from: Date()) % 100
        
        let cardYear = Int(yearTf.text!)!

        let valid = cardYear >= lastTwoDigitsOfCurrentYear
        
        if !valid{
            showDialog(message: "Card has expired")
        }
        
        return valid
    }
}
