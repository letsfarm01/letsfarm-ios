//
//  PaymentOptionController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class PaymentOptionController: BaseController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var paymentOptionSelection:((PaymentChannelResData)->Void)?
    
    var cardCharges:TransactionChargeResData?
    var paymentOptions:[PaymentChannelResData]?
    var walletBalance:Int?
    var totalPrice:Int?

    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
                    
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.addConstraints(formats: ["V:|-10-[v0(280)]-10-|", "H:|[v0]|"], views: paymentOptionCv)
        
        for aaa in paymentOptions!{
            print("\(aaa.descriptionField!)")
        }
        
        paymentOptionCv.reloadData()
    }
    
    lazy var paymentOptionCv = getCollectionView(spacing: 10, cell: PaymentOptionCell.self, identifier: "paymentOptionCell", delegate: self, dataSource: self)
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paymentOptions?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paymentOptionCell", for: indexPath) as! PaymentOptionCell
        
        cell.walletBalance = walletBalance
        cell.cardCharges = cardCharges
        cell.totalPrice = totalPrice
        cell.paymentOptionSelection = paymentOptionCellTap
        cell.paymentOptions = paymentOptions![indexPath.item]

        cell.tag = indexPath.item
        
        return cell
    }
    
    func paymentOptionCellTap(paymentChannelResData:PaymentChannelResData){
        dismiss(animated: true)
        paymentOptionSelection!(paymentChannelResData)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-22, height: 80)
    }
    
}
