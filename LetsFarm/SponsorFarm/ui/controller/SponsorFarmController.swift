//
//  SponsorFarmController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class SponsorFarmController: BaseController {
    
    var farm : AllFarmResData2?{
        didSet{
            navigationItem.title = farm?.farmName
            farmNameLb.text = farm?.farmName

            roiLb.text = "\(farm?.percentageToGain ?? 0)% ROI in \(farm?.duration ?? 0) \(farm?.durationType ?? "-")"
            amountLb.text = "₦\(farm?.amountToInvest?.withCommas() ?? "0")"
            locationLb.text = farm?.farmLocation            
            
            
            if let displayImg = farm?.displayImg, let url = URL(string: displayImg){
                showDialog(message: displayImg)
                farmImg.kf.setImage(with: url)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
                    
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.addConstraints(format: "H:|[v0]|", views:farmImg)
        
        view.addConstraints(format: "H:|-25-[v0]", views:selectQuantityLb)
        view.addConstraints(format: "H:|-25-[v0]-10-[v1]", views:roiImg, roiLb)
        view.addConstraints(format: "H:|-25-[v0]-10-[v1]", views: amountImg, amountLb)
        view.addConstraints(format: "H:|-25-[v0]-10-[v1]", views: locationImg, locationLb)

        view.addConstraints(format: "V:|-90-[v0(170)]-30-[v1]-20-[v2]-13-[v3]-35-[v4]", views: farmImg, roiImg, amountImg, locationImg, selectQuantityLb)
        view.addConstraints(format: "V:|-90-[v0(170)]-30-[v1]-20-[v2]-13-[v3]", views: farmImg, roiLb, amountLb, locationLb)
        
        view.addConstraints(format: "H:|-20-[v0]", views:farmNameLb)
        farmNameLb.alignBottomToBottomOf(of: farmImg, by: -50)
        
        
        let removeImg = getImage(#imageLiteral(resourceName: "remove"), height: 45, width: 45)
        removeImg.addGesture(UITapGestureRecognizer(target: self, action: #selector(remove)))
        let addImg = getImage(#imageLiteral(resourceName: "add"), height: 45, width: 45)
        addImg.addGesture(UITapGestureRecognizer(target: self, action: #selector(add)))
        
        view.addConstraints(format: "V:[v0]-70-[v1]", views: locationLb, quantityTf)
        view.addConstraints(format: "V:[v0]-70-[v1]", views: locationLb, addImg)
        view.addConstraints(format: "V:[v0]-70-[v1]", views: locationLb, removeImg)

        view.alignHorizontal(views: quantityTf)
        view.addConstraints(format: "H:[v0]-15-[v1(65)]-15-[v2]", views: removeImg, quantityTf, addImg)
        
        view.addConstraints(format: "V:[v0]-40-[v1]", views: quantityTf, totalAmountTitleLb)
        view.addConstraints(format: "V:[v0]-40-[v1]", views: quantityTf, totalAmountLb)
        view.addConstraints(format: "H:|-25-[v0]-15-[v1]", views:totalAmountTitleLb, totalAmountLb)

        view.addConstraints(format: "H:|-25-[v0]-25-|", views:addToCartBtn)

        view.addConstraints(format: "V:[v0]-40-|", views: addToCartBtn)
        
    }
    
    var quantity = 0
    
    lazy var quantityTf = getTextField()

    lazy var farmImg = getImage(height: 170, width: view.frame.width)

    lazy var farmNameLb = getLabel(text: "", fontSize: 30, boldFont: true, textColor: .black)

    lazy var roiImg = getImage(#imageLiteral(resourceName: "roi"), height: 17, width: 17)
    lazy var amountImg = getImage(#imageLiteral(resourceName: "wallet"), height: 17, width: 17)
    lazy var locationImg = getImage(#imageLiteral(resourceName: "location"), height: 17, width: 17)

//    lazy var farmNameLb = getLabel(text: "Cucumber Farm", fontSize: 15, boldFont: true, textColor: .black)
    lazy var roiLb = getLabel(text: "-", fontSize: 15, textColor: .black)
    lazy var amountLb = getLabel(text: "₦--,---", fontSize: 15, textColor: .black)
    lazy var locationLb = getLabel(text: "-", fontSize: 15, textColor: .black)
    
    
    lazy var selectQuantityLb = getLabel(text: "Select Quantity", fontSize: 13, textColor: .black)
    lazy var totalAmountTitleLb = getLabel(text: "Total Amount:", fontSize: 13, textColor: .black)
    lazy var totalAmountLb = getLabel(text: "₦0", fontSize: 18, boldFont: true, textColor: .black)

    lazy var addToCartBtn = getButton(title: "Add to cart", action: #selector(addToCart))
    
    @objc func addToCart(){
        if quantity == 0{
            showDialog(message: "Invalid quantity")
        }else{
            
            let addToCartData:[String : Any] = ["quantity":quantity, "farmId":farm!.id!, "userId":DefaultsManager.getUserId()]
            
            makeHttpCall(.cart, .post, addToCartData){data in
                self.showDialog(message: "\(self.quantity) units of \(self.farm!.farmName!) was successfully added to your cart", disableBackgroundTap:true){_ in
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        }
    }
    
    @objc func add(){
        quantity+=1
        quantityTf.text = "\(quantity)"
        
        let totalAmount = (farm?.amountToInvest ?? 0) * quantity
        totalAmountLb.text = "₦\(totalAmount.withCommas())"
    }
    
    @objc func remove(){
        if quantity > 0{
            quantity-=1
            quantityTf.text = "\(quantity)"
            
            let totalAmount = (farm?.amountToInvest ?? 0) * quantity
            totalAmountLb.text = "₦\(totalAmount.withCommas())"
        }
    }
    
    
    func getTextField()->CustomTextField{
        let textField = CustomTextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.setPadding()
        textField.layer.borderWidth = 1.5
        textField.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 255/255).cgColor
        textField.layer.cornerRadius = 4
        textField.backgroundColor = UIColor.white
        textField.textAlignment = .center
        textField.textColor = .black
        textField.font = .boldSystemFont(ofSize: 17)
        textField.text = "0"
        textField.setShadow()
        textField.isUserInteractionEnabled = false
        return textField
    }
}
