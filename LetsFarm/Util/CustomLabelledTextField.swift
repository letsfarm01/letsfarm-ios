//
//  CustomLabelledTextField.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 21/11/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

enum CustomTextFieldTheme {
    case DARK, LIGHT
}

class CustomLabelledTextField : UIView {
    
    var text : String? {
        get{
            return self.textField.text
        }
        set {
            self.textField.text = newValue
        }
    }
    
    var labelText : String? {
        get{
            return self.label.text
        }
        set {
            self.label.text = newValue
        }
    }
    
    override var tag: Int {
        didSet{
            self.textField.tag = tag
        }
    }
    
    var placeholder : String? {
        get{
            return textField.placeholder
        }
        set{
            self.textField.placeholder = newValue
//            self.textField.placeholderColor = UIColor.red
            self.setNeedsLayout()
        }
    }
    
    override func becomeFirstResponder() -> Bool {
        return self.textField.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        return self.textField.resignFirstResponder()
    }
    
    func isValid (_ lenght: Int = 1) -> Bool {
        return self.textField.isValid(lenght: lenght)
    }
    
    var trimmedText:String?{
        get{
            return self.textField.trimmedText
        }
    }
    
    var delegate : UITextFieldDelegate? {
        get{
            return self.textField.delegate
        }
        set {
            self.textField.delegate = newValue
        }
    }
    var returnKeyType : UIReturnKeyType? {
        get{
            return self.textField.returnKeyType
        }
        set {
            self.textField.returnKeyType = newValue!
        }
    }
    
    var keyboardType : UIKeyboardType? {
        get{
            return self.textField.keyboardType
        }
        set {
            self.textField.keyboardType = newValue!
        }
    }
    
    open func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event){
        self.textField.addTarget(target, action: action, for: controlEvents)
    }
//    open func addGesture(_ gesture:UITapGestureRecognizer){
//        self.textField.addGesture(gesture)
//    }

    var inputAccessoryView2 : UIView? {
        get{
            return self.textField.inputAccessoryView
        }
        set {
            self.textField.inputAccessoryView = newValue!
        }
    }
    
    var rightIcon : UIImage? {
        didSet{
            self.rightImageView.image = rightIcon
        }
    }
    
    
    let label:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    
    let textField : CustomTextField = {
        let textField = CustomTextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.setPadding()
        textField.layer.borderWidth = 1.5
        textField.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 255/255).cgColor
        textField.layer.cornerRadius = 4
        textField.backgroundColor = UIColor.white
//        textField.delegate = self
        textField.setShadow()
        return textField
    }()
    
    private let rightImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews () {
        
        self.addConstraints(format: "H:|-2-[v0]-2-|", views: self.textField)
        self.addConstraints(format: "H:|[v0]", views: self.label)
        self.addConstraints(format: "V:|[v0]-10-[v1]|", views: self.label, self.textField)

//        self.translatesAutoresizingMaskIntoConstraints = false
//        self.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        self.layer.masksToBounds = true
//        self.layer.cornerRadius = 4
//        self.backgroundColor = UIColor(white: 0.9, alpha: 0.3)
//
//        self.addSubview(self.textField)
//        self.addSubview(self.rightImageView)
//
//        self.rightImageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
//        self.rightImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
//
//        self.textField.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
//        self.textField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
//        self.textField.rightAnchor.constraint(equalTo: self.rightImageView.leftAnchor, constant: 10).isActive = true
//        self.textField.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -50).isActive = true
//        self.textField.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //        self.delegate?.textFieldDidEndEditing!(textField)
    //    }
    //
    //    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //        return self.delegate?.textFieldShouldReturn!(textField) ?? true
    //    }
    //
    //    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    //        return self.delegate?.textFieldShouldBeginEditing!(textField) ?? true
    //    }
    //
    //    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    //        return self.delegate?.textFieldShouldBeginEditing!(textField) ?? true
    //    }
    
}

