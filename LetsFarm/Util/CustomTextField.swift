//
//  Customself.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 16/08/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

class CustomTextField: UITextField, UITextFieldDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setPadding()
        self.layer.borderWidth = 1.5
        self.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 255/255).cgColor
        self.layer.cornerRadius = 4
        self.backgroundColor = UIColor.white
        self.delegate = self
        self.setShadow()
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        return self.delegate?.textFieldShouldReturn?(textField) ?? true
//    }
//    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        return self.delegate?.textFieldShouldBeginEditing?(textField) ?? true
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
