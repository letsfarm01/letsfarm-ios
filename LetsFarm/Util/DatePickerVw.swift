//
//  DatePickerVw.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 20/08/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

class DatePickerVw: UIView {
    
    var textField: CustomTextField?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
        layer.cornerRadius = 12
        backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        
        addConstraints(format: "H:|[v0]|", views: datePicker)
        addConstraints(format: "V:|[v0]-70-|", views: datePicker)
        addConstraints(format: "V:[v0]-20-[v1]", views: datePicker, cancelBtn)
        addConstraints(format: "V:[v0]-20-[v1]", views: datePicker, okBtn)
        addConstraints(format: "H:[v0]-50-|", views: okBtn)
        addConstraints(format: "H:|-50-[v0]", views: cancelBtn)
        
        isHidden = true

    }
    
    func attachTextField(textField:CustomTextField, mainView:UIView) {
        self.textField = textField
        mainView.alignVertical(views: self)
        mainView.alignHorizontal(views: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    lazy var datePicker : MonthYearPickerView  = {
        let datePicker = MonthYearPickerView()
        //        datePicker.onDateSelected = { (month: Int, year: Int) in
        //            let string = String(format: "%02d/%d", month, year)
        //            NSLog(string) // should show something like 05/2015
        //        }
        return datePicker
    }()
    
    
    lazy var cancelBtn : UIButton = {
        let button = UIButton()
        button.setTitleColor(UIColor.black, for: .normal)
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self, action: #selector(cancelDatePicker), for: .touchUpInside)
        return button
    }()
    
    @objc func cancelDatePicker(){
        self.isHidden = true
    }
    
    lazy var okBtn : UIButton = {
        let button = UIButton()
        button.setTitleColor(UIColor.black, for: .normal)
        button.setTitle("Ok", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self, action: #selector(ok), for: .touchUpInside)
        return button
    }()
    
    @objc func ok(){
        let month = datePicker.getMonth()
        let year = datePicker.getYear()
        //        NSLog("\(month) : \(year)")
        let selectedMonth = DateFormatter().monthSymbols[month-1].capitalized
        self.textField!.text = "\(selectedMonth), \(year)"
        self.isHidden = true
    }
    
}
