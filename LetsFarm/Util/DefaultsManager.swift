//
//  DefaultsManager.swift
//  CleanAgent
//
//  Created by Dekola Ak on 22/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation

class DefaultsManager {
    
    static let tokenKey = "token"
    static let emailKey = "email"
    static let userNameKey = "userName"
    static let phoneNumberKey = "phoneNumber"
    static let userIdKey = "userId"
    static let refCodeKey = "refCode"
    static let remeberMeKey = "remeberMe"
    
    
    static func getUserName()->String{
        let preferences = UserDefaults.standard
        return preferences.string(forKey: userNameKey) ?? ""
    }
    
    static func setUserName(_ userName:String){
        let preferences = UserDefaults.standard
        preferences.set("\(userName)", forKey: userNameKey)
        preferences.synchronize()
    }
    static func getUserPhone()->String{
        let preferences = UserDefaults.standard
        return preferences.string(forKey: phoneNumberKey) ?? ""
    }
    
    static func setUserPhone(_ userPhone:String){
        let preferences = UserDefaults.standard
        preferences.set(userPhone, forKey: phoneNumberKey)
        preferences.synchronize()
    }
    
    static func getUserId()->String{
        let preferences = UserDefaults.standard
        return preferences.string(forKey: userIdKey) ?? ""
    }
    
    static func setUserId(_ userId:String){
        let preferences = UserDefaults.standard
        preferences.set(userId, forKey: userIdKey)
        preferences.synchronize()
    }
    
    static func getRefCode()->String{
        let preferences = UserDefaults.standard
        return preferences.string(forKey: refCodeKey) ?? ""
    }
    
    static func setRefCode(_ refCode:String){
        let preferences = UserDefaults.standard
        preferences.set(refCode, forKey: refCodeKey)
        preferences.synchronize()
    }
    
    static func getEmail()->String{
        let preferences = UserDefaults.standard
        return preferences.string(forKey: emailKey) ?? ""
    }
    
    static func setEmail(_ email:String){
        let preferences = UserDefaults.standard
        preferences.set(email, forKey: emailKey)
        preferences.synchronize()
    }
    
    static func getToken()->String{
        let preferences = UserDefaults.standard
        return preferences.string(forKey: tokenKey) ?? ""
    }
    
    static func setToken(token:String){
        let preferences = UserDefaults.standard
        preferences.set(token, forKey: tokenKey)
        preferences.synchronize()
    }
    
    static func remeberMe()->Bool{
        let pref = UserDefaults.standard
        return pref.bool(forKey: remeberMeKey)
    }
    
    static func setRememberMe(_ remeberMe:Bool){
        let pref = UserDefaults.standard
        pref.set(remeberMe, forKey: remeberMeKey)
        pref.synchronize()
    }
    
    static func clear(){
//        setToken(token:"")
//        setEmail("")
//        setUserId("")
//        setRememberMe(false)
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }
    
    
    
//    static func get(_ key:Any)->String{
//        let preferences = UserDefaults.standard
//        if ((key as? String) != nil) {
//            return preferences.string(forKey: key) ?? ""
//        }
//        return ""
//    }
//
//    static func set(_ userId:Any){
//        let preferences = UserDefaults.standard
//        preferences.set("\(userId)", forKey: userIdKey)
//        preferences.synchronize()
//    }
}
