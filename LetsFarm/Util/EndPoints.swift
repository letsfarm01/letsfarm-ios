//
//  EndPoints.swift
//  CleanAgent
//
//  Created by Dekola Ak on 22/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation

public enum EndPoints:String{
        
        case login = "https://testapi.letsfarm.com.ng/api/v1/auth/login"
        case signUp = "https://testapi.letsfarm.com.ng/api/v1/auth/register"
        case forgotPassword = "https://testapi.letsfarm.com.ng/api/v1/auth/reset-password"
        case resetPassword = "https://testapi.letsfarm.com.ng/api/v1/auth/forgot-password"
        case verifyUser = "https://testapi.letsfarm.com.ng/api/v1/auth/verify-signup"
        case fetchFarms = "https://testapi.letsfarm.com.ng/api/v1/farm-shop"
        case fetchFarmsStatus = "https://testapi.letsfarm.com.ng/api/v1/farm-status/active-farm-status"
        case resendActivationLink = "https://testapi.letsfarm.com.ng/api/v1/"
        case fetchDashboardInfo = "https://testapi.letsfarm.com.ng/api/v1/dashboard"
        case cart = "https://testapi.letsfarm.com.ng/api/v1/cart"
        case getCartCount = "https://testapi.letsfarm.com.ng/api/v1/cart/user-count"
        case getCart = "https://testapi.letsfarm.com.ng/api/v1/cart/user-cart"
        case getPaymentOptionCart = "https://testapi.letsfarm.com.ng/api/v1/payment-channel?can_fund_wallet=false"
        case getPaymentOptionWallet = "https://testapi.letsfarm.com.ng/api/v1/payment-channel?can_fund_wallet=true"
                                     //https://testapi.letsfarm.com.ng/api/v1/payment-channel?can_fund_wallet=true
        case checkout = "https://testapi.letsfarm.com.ng/api/v1/checkout-cart/farm-shop"
//    
        case checkoutSavedCard = "https://testapi.letsfarm.com.ng/api/v1/checkout-cart/charge-existing-card"
//    {"card_id":"5edc2a26bb242f2b287c6205","payment_gateway":"5edc0b7cc9567500173af059","userId":"5edc08a1c9567500173af050"}
        case verifyTransaction = "https://testapi.letsfarm.com.ng/api/v1/checkout-cart/paystack-verify"
//    transactionId=5f0f2caf732d0900178741f7
        case verifyWalletTransaction = "https://testapi.letsfarm.com.ng/api/v1/fund-wallet/paystack-verify-payment"
    //    transactionId=5f0f2caf732d0900178741f7

        case changePassword = "https://testapi.letsfarm.com.ng/api/v1/auth/change-password"
        case updatePersonalInfo = "https://testapi.letsfarm.com.ng/api/v1/users"
        case updateBusinessInfo = "https://testapi.letsfarm.com.ng/api/v1/profile/business-info"
        case updateBankInfo = "https://testapi.letsfarm.com.ng/api/v1/profile/bank-setting"
        case updateNextKinInfo = "https://testapi.letsfarm.com.ng/api/v1/profile/next-of-kin"
        case fetchBanks = "https://testapi.letsfarm.com.ng/api/v1/bank"
        case fetchPendingTransfers = "https://testapi.letsfarm.com.ng/api/v1/transaction/awaiting-payment-proof"
        case uploadProof = "https://testapi.letsfarm.com.ng/api/v1/bank/payment/bank-transfer"
        case getWalletBalance = "https://testapi.letsfarm.com.ng/api/v1/wallet/user"
        case fundWallet = "https://testapi.letsfarm.com.ng/api/v1/fund-wallet"
        case fundWalletCard = "https://testapi.letsfarm.com.ng/api/v1/fund-wallet/charge-existing-card"
        case getUserWalletTransaction = "https://testapi.letsfarm.com.ng/api/v1/wallet-transaction/user"
        case getTransactions = "https://testapi.letsfarm.com.ng/api/v1/transaction/user"
        case getSavedCard = "https://testapi.letsfarm.com.ng/api/v1/fund-wallet/saved-cards"
        case getActiveSponsorship = "https://testapi.letsfarm.com.ng/api/v1/sponsorship/active"
        case getRunningFarms = "https://testapi.letsfarm.com.ng/api/v1/archive-sponsor/user/email"
        case getFarmHistory = "https://testapi.letsfarm.com.ng/api/v1/sponsorship/history"
        case submitMessage = "https://testapi.letsfarm.com.ng/api/v1/contact-account-manager"
        case getReferral = "https://testapi.letsfarm.com.ng/api/v1/referral/user"
        case getCardPaymentCharges = "https://testapi.letsfarm.com.ng/api/v1/checkout-cart/calculate-charge"
        case appVersion = "https://testapi.letsfarm.com.ng/api/v1/dashboard/app-version"
    
    //    case cscsacsac = "https://testapi.letsfarm.com.ng/api/v1/ascsaca"
    //    case cscsacsac = "https://testapi.letsfarm.com.ng/api/v1/ascsaca"
    
    
//    case login = "https://api.letsfarm.com.ng/api/v1/auth/login"
//    case signUp = "https://api.letsfarm.com.ng/api/v1/auth/register"
//    case forgotPassword = "https://api.letsfarm.com.ng/api/v1/auth/reset-password"
//    case resetPassword = "https://api.letsfarm.com.ng/api/v1/auth/forgot-password"
//    case verifyUser = "https://api.letsfarm.com.ng/api/v1/auth/verify-signup"
//    case fetchFarms = "https://api.letsfarm.com.ng/api/v1/farm-shop"
//    case fetchFarmsStatus = "https://api.letsfarm.com.ng/api/v1/active-farm-status"
//    case resendActivationLink = "https://api.letsfarm.com.ng/api/v1/"
//    case fetchDashboardInfo = "https://api.letsfarm.com.ng/api/v1/dashboard"
//    case cart = "https://api.letsfarm.com.ng/api/v1/cart"
//    case getCartCount = "https://api.letsfarm.com.ng/api/v1/ascsaca"
//    case getCart = "https://api.letsfarm.com.ng/api/v1/user-cart"
//    case getPaymentOption = "https://api.letsfarm.com.ng/api/v1/payment-channel"
//    case checkout = "https://api.letsfarm.com.ng/api/v1/checkout-cart/farm-shop"
//    case checkoutSavedCard = "https://api.letsfarm.com.ng/api/v1/checkout-cart/charge-existing-card"
//    case verifyTransaction = "https://api.letsfarm.com.ng/api/v1/checkout-cart/paystack-verify"
//    case verifyWalletTransaction = "https://api.letsfarm.com.ng/api/v1/fund-wallet/paystack-verify-payment"
//    case changePassword = "https://api.letsfarm.com.ng/api/v1/auth/change-password"
//    case updatePersonalInfo = "https://api.letsfarm.com.ng/api/v1/users"
//    case updateBusinessInfo = "https://api.letsfarm.com.ng/api/v1/profile/business-info"
//    case updateBankInfo = "https://api.letsfarm.com.ng/api/v1/profile/bank-setting"
//    case updateNextKinInfo = "https://api.letsfarm.com.ng/api/v1/profile/next-of-kin"
//    case fetchBanks = "https://api.letsfarm.com.ng/api/v1/bank"
//    case fetchPendingTransfers = "https://api.letsfarm.com.ng/api/v1/transaction/awaiting-payment-proof"
//    case uploadProof = "https://api.letsfarm.com.ng/api/v1/bank/payment/bank-transfer"
//    case getWalletBalance = "https://api.letsfarm.com.ng/api/v1/wallet/user"
//    case fundWallet = "https://api.letsfarm.com.ng/api/v1/fund-wallet"
//    case fundWalletCard = "https://api.letsfarm.com.ng/api/v1/fund-wallet/charge-existing-card"
//    case getUserWalletTransaction = "https://api.letsfarm.com.ng/api/v1/wallet-transaction/user"
//    case getSavedCard = "https://api.letsfarm.com.ng/api/v1/fund-wallet/saved-cards"
//    case getTransactions = "https://api.letsfarm.com.ng/api/v1/transaction/user"
//    case getActiveSponsorship = "https://api.letsfarm.com.ng/api/v1/sponsorship/active"
//    case getRunningFarms = "https://api.letsfarm.com.ng/api/v1/archive-sponsor/user/email"
//    case getFarmHistory = "https://api.letsfarm.com.ng/api/v1/sponsorship/history"
//    case submitMessage = "https://api.letsfarm.com.ng/api/v1/contact-account-manager"
//    case getReferral = "https://api.letsfarm.com.ng/api/v1/referral/user"
//    case getCardPaymentCharges = "https://api.letsfarm.com.ng/api/v1/checkout-cart/calculate-charge"
//    case appVersion = "https://api.letsfarm.com.ng/api/v1/dashboard/app-version"
////    case cscsacsac = "https://api.letsfarm.com.ng/api/v1/ascsaca"
////    case cscsacsac = "https://api.letsfarm.com.ng/api/v1/ascsaca"

//addToCart
}
