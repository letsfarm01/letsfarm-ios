//
//  HttpMethod.swift
//  CleanAgent
//
//  Created by Dekola Ak on 22/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation

public enum HttpMethod:String{
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"

}
