//
//  Data.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import CommonCrypto

extension Data {
    func hmac(key: Data) -> Data {
        var hmac = Data(count: Int(CC_SHA1_DIGEST_LENGTH))
        
        let algo = CCHmacAlgorithm(kCCHmacAlgSHA1)
        key.withUnsafeBytes { (keyPointer) -> Void in
            self.withUnsafeBytes { (dataPointer) -> Void in
                
                hmac.withUnsafeMutableBytes { (hmacPointer) -> Void in
                    CCHmac(algo, keyPointer.baseAddress, key.count, dataPointer.baseAddress, self.count, hmacPointer.baseAddress)
                }
            }
        }
        return hmac
    }
    
    func getHash() -> Data {
        var digest = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        _ = digest.withUnsafeMutableBytes { (digestBytes) in
            self.withUnsafeBytes { (stringBytes) in
                CC_SHA256(stringBytes, CC_LONG(self.count), digestBytes)
            }
        }
        return digest
    }
}
