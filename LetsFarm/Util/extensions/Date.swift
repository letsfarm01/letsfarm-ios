//
//  Date.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
}
