//
//  Float.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation

extension Float {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
