//
//  String.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation

extension String {
    func toBase64() -> String? {
        return Data(self.utf8).base64EncodedString()
    }
    
    func fromBase64() -> String? {
        let b64 = self
            .replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")
        guard let data = Data(base64Encoded: b64, options: .ignoreUnknownCharacters) else{
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    func getFormattedDate() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: self)

        return date?.string(format: "MMM dd, yyyy")
    }
}
