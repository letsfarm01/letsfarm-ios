//
//  UICollectionViewCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionViewCell{
    
    func getButton(title:String, bgColor:UIColor = UIColor.primaryColour, borderColor:UIColor? = nil, titleColor:UIColor = .white, cornerRadius:CGFloat = 4, action: Selector? = nil) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setBackgroundColor(bgColor, for: .normal)
        button.setShadow()
        if let borderColor = borderColor{
            button.layer.borderColor = borderColor.cgColor
            button.layer.borderWidth = 1
        }
        button.clipsToBounds = true;
        button.setTitleColor(titleColor, for: .normal)
        button.setHeight(height: 46)
        if let action = action {
            button.addTarget(self, action: action, for: .touchUpInside)
        }
        button.layer.cornerRadius = cornerRadius
        return button
    }
    
    func getImage(_ image:UIImage? = nil, height:CGFloat? = nil, width:CGFloat? = nil, action: Selector? = nil)->UIImageView{
        let imageView = UIImageView()
        if let height = height, let width = width{
            imageView.setDimensions(height: height, width: width)
        }
        if let image = image{
            imageView.image = image
        }
        if let action = action{
            let tapGesture = UITapGestureRecognizer(target: self, action: action)
            imageView.addGestureRecognizer(tapGesture)
            imageView.isUserInteractionEnabled = true
        }
        imageView.setShadow()
        return imageView
    }
    
    
    func getLabel(text:String, fontSize:CGFloat = 14, boldFont:Bool = false, textColor:UIColor = #colorLiteral(red: 0.151104629, green: 0.1502135396, blue: 0.151794225, alpha: 1), action: Selector? = nil)->UILabel{
        let label = UILabel()
        label.text = text
        label.textColor = textColor
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = boldFont ? UIFont.boldSystemFont(ofSize: fontSize) : UIFont.systemFont(ofSize: fontSize)
        
        if let action = action{
            let tapGesture = UITapGestureRecognizer(target: self, action: action)
            label.addGestureRecognizer(tapGesture)
            label.isUserInteractionEnabled = true
        }
        return label
    }
}
