//
//  UIColor.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    // MARK: - Initialization
    static let primaryColour = #colorLiteral(red: 0.368627451, green: 0.768627451, blue: 0.3607843137, alpha: 1)
    static let secondaryColour = #colorLiteral(red: 0.968627451, green: 0.5803921569, blue: 0.1137254902, alpha: 1)

    static func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
        
        var rgb: UInt32 = 0
        
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        
        let length = hexSanitized.count
        
        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }
        
        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0
            
        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0
            
        } else {
            return nil
        }
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }
    
    // MARK: - Computed Properties
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - From UIColor to String
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
    
//    convenience init(hex: String) {
//        self.init(hex: hex, alpha:1)
//    }
//
//    convenience init(hex: String, alpha: CGFloat) {
//        var hexWithoutSymbol = hex
//        if hexWithoutSymbol.hasPrefix("#") {
//            hexWithoutSymbol = hex.substring(1)
//        }
//        
//        let scanner = Scanner(string: hexWithoutSymbol)
//        var hexInt:UInt32 = 0x0
//        scanner.scanHexInt32(&hexInt)
//        
//        var r:UInt32!, g:UInt32!, b:UInt32!
//        switch (hexWithoutSymbol.length) {
//        case 3: // #RGB
//            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
//            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
//            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
//            break;
//        case 6: // #RRGGBB
//            r = (hexInt >> 16) & 0xff
//            g = (hexInt >> 8) & 0xff
//            b = hexInt & 0xff
//            break;
//        default:
//            // TODO:ERROR
//            break;
//        }
//        
//        self.init(
//            red: (CGFloat(r)/255),
//            green: (CGFloat(g)/255),
//            blue: (CGFloat(b)/255),
//            alpha:alpha)
//    }
}
