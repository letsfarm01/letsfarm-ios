//
//  UIImageView.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
//    func setPadding(left:Bool = false, right:Bool = false, vertical:Bool = false, size:Int = 15){
//        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: size, height: 0))
//
//        if left {
//            self.leftView = paddingView
//            self.leftViewMode = .always
//        }
//        if right{
//            self.rightView = paddingView
//            self.rightViewMode = .always
//        }
//
//        if vertical{
//            self.heightAnchor.constraint(equalToConstant: 45).isActive = true
//        }
//    }
}
extension UIImageView {
    
    func setCornerRadius(radius:CGFloat){
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func applyshadowWithCorner(containerView : UIView, cornerRadious : CGFloat){
        containerView.clipsToBounds = false
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 1
        containerView.layer.shadowOffset = CGSize.zero
        containerView.layer.shadowRadius = 10
        containerView.layer.cornerRadius = cornerRadious
        containerView.layer.shadowPath = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: cornerRadious).cgPath
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadious
    }
    /// SwifterSwift: Set image from a URL.
       ///
       /// - Parameters:
       ///   - url: URL of image.
       ///   - contentMode: imageView content mode (default is .scaleAspectFit).
       ///   - placeHolder: optional placeholder image
       ///   - completionHandler: optional completion handler to run when download finishs (default is nil).
       func download(
           from url: URL,
           contentMode: UIView.ContentMode = .scaleAspectFit,
           placeholder: UIImage? = nil,
           completionHandler: ((UIImage?) -> Void)? = nil) {

           image = placeholder
           self.contentMode = contentMode
           URLSession.shared.dataTask(with: url) { (data, response, _) in
               guard
                   let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                   let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                   let data = data,
                   let image = UIImage(data: data)
                   else {
                       completionHandler?(nil)
                       return
               }
               DispatchQueue.main.async { [unowned self] in
                   self.image = image
                   completionHandler?(image)
               }
               }.resume()
       }

       /// SwifterSwift: Make image view blurry
       ///
       /// - Parameter style: UIBlurEffectStyle (default is .light).
       func blur(withStyle style: UIBlurEffect.Style = .light) {
           let blurEffect = UIBlurEffect(style: style)
           let blurEffectView = UIVisualEffectView(effect: blurEffect)
           blurEffectView.frame = bounds
           blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
           addSubview(blurEffectView)
           clipsToBounds = true
       }

       /// SwifterSwift: Blurred version of an image view
       ///
       /// - Parameter style: UIBlurEffectStyle (default is .light).
       /// - Returns: blurred version of self.
       func blurred(withStyle style: UIBlurEffect.Style = .light) -> UIImageView {
           let imgView = self
           imgView.blur(withStyle: style)
           return imgView
       }
    
    

}
