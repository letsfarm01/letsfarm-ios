//
//  UILabel.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

extension UILabel{
    func setText(text:String, isCompulsory:Bool = false){
        let attributedText = NSMutableAttributedString()
        
        if isCompulsory{
            attributedText.append(NSAttributedString(string: "*  ", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]))
        }
        
        attributedText.append(NSAttributedString(string: text, attributes: nil))

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        
        attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedText.string.count))
        
        self.attributedText = attributedText
    }
}
