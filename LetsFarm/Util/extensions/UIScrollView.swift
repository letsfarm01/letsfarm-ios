//
//  UIScrollView.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

extension UIScrollView{
    
    func addStackView(stackView:UIStackView){
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        // Set the width of the stack view to the width of the scroll view for vertical scrolling
        stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        //        formSv.heightAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
    }
}
