//
//  UIStackView.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

extension UIStackView {
    func addArrangedSubview(views:UIView...){
        for view in views{
            self.addArrangedSubview(view)
        }
    }
    func addArrangedSubview(views:[UIView]){
        for view in views{
            self.addArrangedSubview(view)
        }
    }
    func setPadding(dimension:CGFloat){
        self.layoutMargins = UIEdgeInsets(top: dimension, left: dimension, bottom: dimension, right: dimension)
        self.isLayoutMarginsRelativeArrangement = true
    }
    
    func setPadding(top:CGFloat, bottom:CGFloat, left:CGFloat, right:CGFloat){
        self.layoutMargins = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        self.isLayoutMarginsRelativeArrangement = true
    }
    
    func setCustomSpacing(dimension:CGFloat, after:UIView...) {
        for view in after{
            self.setCustomSpacing(dimension, after: view)
        }
    }
    func addBackground(color: UIColor) {
           let subView = UIView(frame: bounds)
           subView.backgroundColor = color
           subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
           insertSubview(subView, at: 0)
           subView.layer.cornerRadius = 10
       }
}
