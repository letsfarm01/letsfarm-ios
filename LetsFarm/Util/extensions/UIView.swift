//
//  UIView.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    func addGesture(_ gesture:UITapGestureRecognizer){
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(gesture)
    }
    
    func setHeight(height:CGFloat){
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    func setWidth(width:CGFloat){
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
    }
    
    func setDimensions(height:CGFloat, width:CGFloat){
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
    }
    
    func addSubview(views : UIView...){
        for view in views {
            addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    func addConstraints(formats: [String], view: UIView){
        for format in formats{
            addConstraints(format: format, views: view)
        }
    }

    func addConstraints(formats: [String], views: UIView...){
        for format in formats{
            for view in views{
                addConstraints(format: format, views: view)
            }
        }
    }

    func addConstraints(format: String, views: [UIView]){
        for myView in views{
            addConstraints(format: format, views: myView)
        }
    }
    
    func addConstraints(format : String, views : UIView...){
        var myDict = [String:UIView]()
        
        for (index, view) in views.enumerated() {
            myDict["v\(index)"] = view
            self.addSubview(views: view)
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: myDict))
    }
    
    func setShadow(shadowRadius: CGFloat = 3, color:UIColor = UIColor.rgb(200, 200, 200), offSet:Int = 0, opacity:Float = 10){
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity;
        self.layer.shadowRadius = shadowRadius;
        self.layer.shadowOffset = CGSize(width: offSet, height: offSet)
    }
    
    func alignHorizontal(views: UIView...){
        for myView in views {
            self.addSubview(views: myView)
            myView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        }
    }
    
    func alignVertical(views: UIView...){
        for myView in views {
            self.addSubview(views: myView)
            
            myView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            
        }
    }
    
    func alignTopToParentTop(by:CGFloat=0){
        self.topAnchor.constraint(equalTo: self.superview!.topAnchor, constant: by).isActive = true
    }
    
    func alignTopToTopOf(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.topAnchor.constraint(equalTo: of.topAnchor, constant: by).isActive = true
    }
    
    func alignTopToBottomOf(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.topAnchor.constraint(equalTo: of.bottomAnchor, constant: by).isActive = true
    }
    
    func alignBottomToBottomOf(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.bottomAnchor.constraint(equalTo: of.bottomAnchor, constant: by).isActive = true
    }
    
    func alignBottomToParentBottom(by:CGFloat=0){
        self.bottomAnchor.constraint(equalTo: self.superview!.bottomAnchor, constant: by).isActive = true
    }
    
    func alignBottomToTopOf(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.bottomAnchor.constraint(equalTo: of.topAnchor, constant: by).isActive = true
    }
    
    func alignStartToStartOf(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.leadingAnchor.constraint(equalTo: of.leadingAnchor, constant: by).isActive = true
    }
    func alignStartToParentStart(by:CGFloat=0){
        self.leadingAnchor.constraint(equalTo: self.superview!.leadingAnchor, constant: by).isActive = true
    }
    
    func alignStartToEndOf(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.leadingAnchor.constraint(equalTo: of.trailingAnchor, constant: by).isActive = true
    }
    
    func alignEndToEndOf(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.trailingAnchor.constraint(equalTo: of.trailingAnchor, constant: by).isActive = true
    }
    
    func alignEndToParentEnd(by:CGFloat=0){
        self.trailingAnchor.constraint(equalTo: self.superview!.trailingAnchor, constant: by).isActive = true
    }
    
    func alignEndToStartOf(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.trailingAnchor.constraint(equalTo: of.leadingAnchor, constant: by).isActive = true
    }
    
    func alignToCenterHorizontal(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.centerXAnchor.constraint(equalTo: of.centerXAnchor, constant: by).isActive = true
    }
    
    func alignToCenterVertical(of:UIView, by:CGFloat=0){
        of.superview?.addSubview(self)
        self.centerYAnchor.constraint(equalTo: of.centerYAnchor, constant: by).isActive = true
    }
    
    func alignHorizontal(views: UIView..., to:UIView){
        for myView in views {
            self.addSubview(views: myView)
            self.addConstraint(NSLayoutConstraint(item: myView, attribute: .centerX, relatedBy: .equal, toItem: to, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        }
    }
    
    func alignVertical(views: UIView..., to:UIView){
        for myView in views {
            self.addSubview(views: myView)
            self.addConstraint(NSLayoutConstraint(item: myView, attribute: .centerY, relatedBy: .equal, toItem: to, attribute: .centerY, multiplier: 1.0, constant: 0.0))
        }
    }
    
}
