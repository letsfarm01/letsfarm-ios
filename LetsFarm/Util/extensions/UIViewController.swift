//
//  UIViewController.swift
//  Baxi Mobile
//
//  Created by Dekola Ak on 01/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

//open class CustomUITabBarController : UITabBarController, CustomUIViewController {
//
//}
//open class CustomUIViewController : UIViewController {
//
//    open var activityIndicator: UIActivityIndicatorView!
//
//}

import Foundation
import UIKit

extension UIViewController {
    
    func getCollectionView(spacing:CGFloat, cell:AnyClass, identifier:String, delegate:UICollectionViewDelegate, dataSource:UICollectionViewDataSource, backgroundColor:UIColor = .white,  scrollDirection:UICollectionView.ScrollDirection = .vertical)->UICollectionView{
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = spacing
        flowLayout.minimumInteritemSpacing = spacing
        flowLayout.scrollDirection = scrollDirection
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = backgroundColor
        collectionView.register(cell, forCellWithReuseIdentifier: identifier)
        collectionView.delegate = delegate
        collectionView.dataSource = dataSource
        return collectionView
    }
    
    func getMainScrollView(_ mainSv:UIStackView)->UIScrollView{
        let scroll = UIScrollView()
        scroll.addConstraints(formats: ["V:|[v0]|","H:|[v0]|"], view: mainSv)
        scroll.addStackView(stackView: mainSv)
        return scroll
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
        
    func getButton(title:String, bgColor:UIColor = UIColor.primaryColour, borderColor:UIColor? = nil, titleColor:UIColor = .white, cornerRadius:CGFloat = 4, action: Selector) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setBackgroundColor(bgColor, for: .normal)
        button.setShadow()
        if let borderColor = borderColor{
            button.layer.borderColor = borderColor.cgColor
            button.layer.borderWidth = 1
        }
        button.clipsToBounds = true;
        button.setTitleColor(titleColor, for: .normal)
        button.setHeight(height: 46)
        button.addTarget(self, action: action, for: .touchUpInside)
        button.layer.cornerRadius = cornerRadius
        return button
    }
    
    func getStackView(_ views: UIView...)->UIStackView{
        let stackView = UIStackView()
//        stackView.distribution = .fillEqually
        stackView.alignment = .fill
//        stackView.setPadding(dimension: 2)
        stackView.spacing = 20
        stackView.axis = .vertical
        stackView.addArrangedSubview(views:views)
        return stackView
    }
    
    func getImage(_ image:UIImage? = nil, height:CGFloat? = nil, width:CGFloat? = nil, action: Selector? = nil)->UIImageView{
        let imageView = UIImageView()
        if let height = height, let width = width{
            imageView.setDimensions(height: height, width: width)
        }
        if let image = image{
            imageView.image = image
        }
        
        imageView.setShadow()
        
        if let action = action{
            let tapGesture = UITapGestureRecognizer(target: self, action: action)
            imageView.addGestureRecognizer(tapGesture)
            imageView.isUserInteractionEnabled = true
        }
        
        return imageView
    }
    
    
    func getLabel(text:String, fontSize:CGFloat = 14, boldFont:Bool = false, textColor:UIColor = #colorLiteral(red: 0.151104629, green: 0.1502135396, blue: 0.151794225, alpha: 1), backgroundColour: UIColor? = nil, action: Selector? = nil)->UILabel{
        let label = UILabel()
        label.text = text
        label.textColor = textColor
        if let backgroundColour = backgroundColour{
            label.backgroundColor = backgroundColour
        }
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = boldFont ? UIFont.boldSystemFont(ofSize: fontSize) : UIFont.systemFont(ofSize: fontSize)
        
        if let action = action{
            let tapGesture = UITapGestureRecognizer(target: self, action: action)
            label.addGestureRecognizer(tapGesture)
            label.isUserInteractionEnabled = true
        }
        return label
    }
    
    func printJsonResponse(_ receivedData:Data){
        do{
            let jsonData = try JSONSerialization.jsonObject(with: receivedData, options: [])
            print("\(jsonData)")
        }
        catch{
        
        }
    }
    
    func showDialog(message:String, title:String = "Info", poitiveBtnTxt:String? = "OK", negativeBtnTxt:String? = nil, disableBackgroundTap:Bool = false, tfListener: ((String) -> Void)? = nil, listener: ((Bool) -> Void)? = nil){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        if let tfListener = tfListener{
            alert.addTextField { (textField) in
                textField.placeholder = ""
            }
            alert.addAction(UIAlertAction(title: "Submit", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0]
                tfListener(textField!.text!)
            }))
        }
        else{
            alert.addAction(UIAlertAction(title: poitiveBtnTxt, style: .default){alert in
                listener?(true)
            })
        }
        
    
        if let negativeBtnTxt = negativeBtnTxt{
            alert.addAction(UIAlertAction(title: negativeBtnTxt, style: .cancel){alert in
                listener?(false)
            })
        }
        
        self.present(alert, animated: true, completion:{
            if !disableBackgroundTap{
                alert.view.superview?.isUserInteractionEnabled = true
                alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
            }
        })
    }
    
    
    @objc func alertControllerBackgroundTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func processError(_ receivedData:Data, _ statusCode:Int, listener: ((Bool)->Void)? = nil){
        do{
            let genRes = try JSONDecoder().decode(GeneralResponse.self, from: receivedData)
            print("\(genRes.msg!)")
            showDialog(message:"\(genRes.msg!)", listener:listener)
        }
        catch{
            showDialog(message: "An error occurred", listener:listener)
        }
    }
    
        
//    func makeHttpCall(_ endpoint:String, _ httpMethod:HttpMethod, _ dictionary:[String:Any]? = nil, queryItems:[URLQueryItem]? = nil, _ getStatusCode: ((Data, HTTPURLResponse, Error?) -> Void)? = nil, _ getData: @escaping (Data) -> Void){
//                    
//        let request = getRequest(endpoint, httpMethod, dictionary, queryItems)!
//        dump(request)
//        print(dictionary ?? ["":""])
////        request.debugDescription
//        startUrlSession(request as URLRequest, getData, getStatusCode)
//    }
    func makeHttpCall(_ endpoint:EndPoints, _ httpMethod:HttpMethod, _ dictionary:[String:Any]? = nil, queryItems:[URLQueryItem]? = nil, path:String? = "", _ getStatusCode: ((Data, HTTPURLResponse, Error?) -> Void)? = nil, _ getData: @escaping (Data) -> Void){
        
        var url = "\(endpoint.rawValue)"
        print(url)
        
        if let path = path{
            url.append("/\(path)")
        }
        
        let request = getRequest(url, httpMethod, dictionary, queryItems)!
//        dump(request)
        print(dictionary ?? ["":""])
//        request.debugDescription
        startUrlSession(request as URLRequest, getData, getStatusCode)
    }
    
    func getRequest(_ endpoint:String, _ httpMethod: HttpMethod, _ dictionary:[String:Any]? = nil, _ queryItems:[URLQueryItem]? = nil)->URLRequest?{
        
        var urlReq = URLComponents(string: endpoint)!
        
        if let queryItems = queryItems{
            urlReq.queryItems = queryItems
        }

        var request = URLRequest(url: urlReq.url!)
        request.httpMethod = httpMethod.rawValue
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
//        var urlReq = URLComponents(string: "\(baseUrl)/rest/v2/exchange/view")!
        
        if DefaultsManager.getToken() != ""{
            let token = "\(DefaultsManager.getToken())"
            request.setValue(token, forHTTPHeaderField: "Authorization")
        }
    
        if let dictionary = dictionary{
            guard let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options:[]) else{
                return nil
            }
            request.httpBody = jsonData
        }
        
        return request
    }
    
    
  var activityIndicator : UIActivityIndicatorView {
        get{
            let indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
            indicator.frame = CGRect(x: 100, y: 100, width: 500, height: 500)
            indicator.transform = CGAffineTransform(scaleX: 3, y: 3)
            view.alignVertical(views: indicator)
            view.alignHorizontal(views: indicator)
            return indicator
        }
        
    }
        
    func startUrlSession(_ urlRequest: URLRequest, _ getData: @escaping (Data) -> Void, _ getStatusCode: ((Data, HTTPURLResponse, Error?) -> Void)?){
        let indicator = activityIndicator
        indicator.startAnimating()
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            DispatchQueue.main.async {indicator.removeFromSuperview()}
            guard let httpResponse = response as? HTTPURLResponse, let receivedData = data, error == nil
                else {
                    let errorMessage = "DataTask error: " + error!.localizedDescription + "\n"
                    print(errorMessage)
                    DispatchQueue.main.async {
                        self.showDialog(message: error!.localizedDescription, title:"Error")
                    }
                    return
            }
            
            self.printJsonResponse(receivedData)
            
            print("\(httpResponse.statusCode)")
            DispatchQueue.main.async {
                
                if let getStatusCode = getStatusCode{
                    getStatusCode(receivedData, httpResponse, error)
                }
                else{
                    switch httpResponse.statusCode {
                    case 200...299:
                        getData(receivedData)
                    case 401:
                        self.logOut()
                    default:
                        self.processError(receivedData, httpResponse.statusCode)
                    }
                }
            }
        }.resume()
    }

    func areValid(_ textFields:CustomLabelledTextField...)->Bool{
        
        var valid = true
        
        for tf in textFields {
            if !tf.isValid() {
                valid = false
            }
        }
        return valid
    }
        
    func logOut(){
        DefaultsManager.clear()
        
        let scene = UIApplication.shared.connectedScenes.first
           if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {                                  sd.changeRootViewContoller(LoginController())
           }
    }
}
