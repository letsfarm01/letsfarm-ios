//
//  AppVersionRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 12/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct AppVersionRes : Codable {

    let code : Int?
    let data : AppVersionResData?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }
    
}

struct AppVersionResData : Codable {

    let v : Int?
    let id : String?
    let createdAt : String?
    let deleted : Bool?
    let minAllowedAndroidVersion : String?
    let minAllowedIosVersion : String?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case minAllowedAndroidVersion = "min_allowed_android_version"
        case minAllowedIosVersion = "min_allowed_ios_version"
        case updatedAt = "updatedAt"
    }
    
}
