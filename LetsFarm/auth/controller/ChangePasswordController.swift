//
//  ChangePasswordController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 12/05/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class ChangePasswordController: BaseController {
    
    var email:String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = "Create an account"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addConstraints(format: "V:|-130-[v0]-20-[v1]-40-[v2]", views: forgotPasswordLb, welcomeLb, mainSv)        
        view.addConstraints(format: "H:|-30-[v0]", views: [forgotPasswordLb, welcomeLb])
        view.addConstraints(formats: ["H:|-30-[v0]-30-|"], views: mainSv)
    }
    
    lazy var forgotPasswordLb = getLabel(text: "Reset Password", fontSize: 25, boldFont: true, textColor: .black)
    
    lazy var welcomeLb = getLabel(text: "Enter the token sent to your mail", fontSize: 15, textColor: .black)
    
    lazy var passwordTf = getLabelledTextField(placeholder: "New password", labelText: "Enter your new password", keyboardType: .default, isPassword:true)
    
    lazy var confirmPasswordTf = getLabelledTextField(placeholder: "Confirm password", labelText: "Enter your new password", keyboardType: .default, isPassword:true)

    lazy var tokenTf = getLabelledTextField(placeholder: "Token", labelText: "Enter your reset token", keyboardType: .phonePad)

    lazy var changePasswordBtn = getButton(title: "Change Password", action: #selector(changePassword))
    
    lazy var alreadyHaveAccntLb = getLabel(text: "Already have an account? Log in", fontSize: 20, boldFont: true, textColor: .primaryColour, action: #selector(login))
    
    @objc func login(){
        if let sd = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate) {
            sd.changeRootViewContoller(LoginController())
        }
    }
    
    lazy var loginLb = getLabel(text: "", fontSize: 12, textColor: UIColor.primaryColour)
    
    lazy var createAccountVw:UIView = {
        let view = UIView()
        view.alignHorizontal(views: alreadyHaveAccntLb, loginLb)
        view.addConstraints(format: "V:|[v0]-10-[v1]|", views: alreadyHaveAccntLb, loginLb)
        return view
    }()
    
    lazy var mainSv = getStackView(tokenTf, passwordTf, confirmPasswordTf, changePasswordBtn, createAccountVw)
        
    @objc func changePassword(){
        if tokenTf.isValid() && passwordTf.isValid(6) && passwordTf.textField.isPasswordMatch(confirmPasswordTf.textField){
            let changePasswordReq = ["password":passwordTf.text!, "confirmPassword":confirmPasswordTf.text!, "token":tokenTf.text!]

            
            makeHttpCall(.resetPassword, .post, changePasswordReq){data in
                
                do{
                    let genRes = try JSONDecoder().decode(GeneralResponse.self, from: data)
                        
                    self.showDialog(message: genRes.msg ?? "Your password has been successfully changed"){positive in
                        if let sd = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate) {
                            sd.changeRootViewContoller(LoginController())
                        }
                    }
                }catch{}
            }
        }
    }
}
