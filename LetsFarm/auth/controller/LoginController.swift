//
//  LoginController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 10/05/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class LoginController: BaseController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = "Login"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addConstraints(format: "V:|-130-[v0]-20-[v1]-20-[v2]", views: welcomeLb, loginLb, mainSv)
        view.addConstraints(formats: ["H:|-20-[v0]"], views: welcomeLb, loginLb)
        view.addConstraints(formats: ["H:|-20-[v0]-20-|"], views: mainSv)

        if useDummyData{
            emailTf.text = "akano.adekola@gmail.com"
            passwordTf.text = "kkkkkk"
            
//            emailTf.text = "arokoyuolalekan@gmail.com"
//            passwordTf.text = "Lekens0001"
        }
    }
    
    lazy var welcomeLb = getLabel(text: "Welcome to Letsfarm.co", fontSize: 15, textColor: .black)
    
    lazy var loginLb = getLabel(text: "Login", fontSize: 35, boldFont: true, textColor: .black)

    lazy var emailTf = getLabelledTextField(placeholder: "Email", labelText: "Your email address", keyboardType: .emailAddress)
    
    lazy var passwordTf = getLabelledTextField(placeholder: "Password", labelText: "Your password", keyboardType: .default, isPassword:true)
    
    lazy var loginBtn = getButton(title: "Login", action: #selector(login))
    
    lazy var remeberMeSt : UISwitch = {
        let mSwitch = UISwitch(frame:CGRect(x: 10, y: 10, width: 10, height: 10))
        mSwitch.setOn(false, animated: false)
//        mSwitch.addTarget(self, action: #selector(remeberMeSwitch(sender:)), for: .valueChanged)
        return mSwitch
    }()

    lazy var remeberMeLb = getLabel(text:  "Remember me", fontSize: 14, textColor: .black)
    lazy var forgotPasswordLb = getLabel(text:  "Forgot Password?", fontSize: 14, boldFont: true, action:#selector(forgotPassword))
    
    @objc func forgotPassword(){
        navigationController?.pushViewController(ResetPasswordController(), animated: true)
    }
    
    lazy var remeberMeVw:UIView = {
        let view = UIView()
        view.addConstraints(format: "H:|[v0]-10-[v1]-20-[v2]|", views: remeberMeLb, remeberMeSt, forgotPasswordLb)
        view.addConstraints(format: "V:|-20-[v0]-20-|", views: remeberMeLb, remeberMeSt, forgotPasswordLb)
        view.alignVertical(views: remeberMeLb, remeberMeSt, forgotPasswordLb)
        return view
    }()
    
    lazy var dontHaveAccntLb = getLabel(text: "Don't have an account? Register", fontSize: 15, boldFont: true, textColor: UIColor.primaryColour, action: #selector(createAccount))
    
    lazy var createAccntLb = getLabel(text: "", fontSize: 12, textColor: UIColor.primaryColour, action: #selector(createAccount))
    
    @objc func createAccount(){
        navigationController?.pushViewController(SignUpController(), animated: true)
    }
    
    lazy var createAccountVw:UIView = {
        let view = UIView()
        view.alignHorizontal(views: dontHaveAccntLb, createAccntLb)
        view.addConstraints(format: "V:|[v0]-10-[v1]|", views: dontHaveAccntLb, createAccntLb)
        return view
    }()
    
    lazy var mainSv = getStackView(welcomeLb, loginLb, emailTf, passwordTf, remeberMeVw, loginBtn, createAccountVw)
    
    @objc func login(){
        if emailTf.textField.isEmailValid() && passwordTf.isValid(6) {
                
            let loginReq = ["email":emailTf.text!,"password":passwordTf.text!]
            
            self.makeHttpCall(.login, .post, loginReq, {data, response, error in
                
                do{
                    let genRes = try JSONDecoder().decode(GeneralResponse.self, from: data)
                    
                    let statusCode = response.statusCode
                    switch(response.statusCode){
                        case 200...299:
                            self.processSuccess(data)
                        case 401:
                            if genRes.status == "USER_NOT_VERIFIED" {
                                self.verifyUser()
                            }else{
                                self.showDialog(message: genRes.msg ?? "Invalid login details", title: "Error")
                        }
                        default:
                            self.processError(data, statusCode)
                    }
                    
                }catch{}
            }){data in }
        }
    }
    func processSuccess(_ data:Data){
        do{
            let loginRes = try JSONDecoder().decode(LoginRes.self, from: data)

//            self.showDialog(message: loginRes.msg ?? "Login succesful", title: "Success")
            
            DefaultsManager.setRememberMe(self.remeberMeSt.isOn)

            DefaultsManager.setToken(token: loginRes.data?.token ?? "")
            DefaultsManager.setEmail(loginRes.data?.user?.email ?? "")
            DefaultsManager.setUserId(loginRes.data?.user?._id ?? "")
            DefaultsManager.setRefCode(loginRes.data?.user?.referralCode ?? "")

                self.navigationController?.pushViewController(TabBarController(), animated: true)
        }
        catch{
            
        }
    }
    
    func verifyUser(){
        let resendActivationLinkReq = ["email":emailTf.text!]
        
        self.makeHttpCall(.resendActivationLink, .post, resendActivationLinkReq){data in
            do{
                let genRes = try JSONDecoder().decode(GeneralResponse.self, from: data)
                self.showDialog(message: genRes.msg ?? "Login succesful", title: "Success")
            }catch{}

        }
        
    }
}
