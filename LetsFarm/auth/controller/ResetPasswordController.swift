//
//  ResetPasswordController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 12/05/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//


import Foundation
import UIKit

class ResetPasswordController: BaseController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = "Password Reset"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        welcomeLb.numberOfLines = 0
        view.addConstraints(format: "V:|-130-[v0]-20-[v1]-40-[v2]", views: forgotPasswordLb, welcomeLb, mainSv)
        view.alignHorizontal(views: forgotPasswordLb)
        view.addConstraints(formats: ["H:|-75-[v0]-75-|"], views: welcomeLb)
        view.addConstraints(formats: ["H:|-30-[v0]-30-|"], views: mainSv)

        if useDummyData{
            emailTf.text = "akano.adekola@gmail.com"
        }
    }
        
    lazy var forgotPasswordLb = getLabel(text: "Forgot Password", fontSize: 25, boldFont: true, textColor: .black)
    
    lazy var welcomeLb = getLabel(text: "We will send a link to your registered email address to reset your password", fontSize: 15, textColor: .black)

    lazy var emailTf = getLabelledTextField(placeholder: "Email", labelText: "Enter your email", keyboardType: .emailAddress)
    
    lazy var resetPasswordBtn = getButton(title: "Send Code", action: #selector(resetPassword))
    
    lazy var alreadyHaveAccntLb = getLabel(text: "Already have an account? Log in", fontSize: 20, boldFont: true, textColor: .primaryColour, action: #selector(login))
    
    @objc func login(){
        if let sd = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate) {
            sd.changeRootViewContoller(LoginController())
        }
    }
    
    lazy var loginLb = getLabel(text: "", fontSize: 12, textColor: UIColor.primaryColour)
    
    lazy var createAccountVw:UIView = {
        let view = UIView()
        view.alignHorizontal(views: alreadyHaveAccntLb, loginLb)
        view.addConstraints(format: "V:|[v0]-10-[v1]|", views: alreadyHaveAccntLb, loginLb)
        return view
    }()
    
    lazy var mainSv = getStackView(emailTf, resetPasswordBtn, createAccountVw)
        
    @objc func resetPassword(){
        if emailTf.textField.isEmailValid() {
            let resetPasswordReq = ["email":emailTf.text!]
            
            self.makeHttpCall(.forgotPassword, .post, resetPasswordReq){ data in
                do{
                    let genRes = try JSONDecoder().decode(GeneralResponse.self, from: data)
                
                    self.showDialog(message: genRes.msg ?? "Kindly check your mail for a reset token"){ _ in
                        let changePasswordController = ChangePasswordController()
                        changePasswordController.email = self.emailTf.text!
                        self.navigationController?.pushViewController(changePasswordController, animated: true)
                    }
                }catch{}
            
            }
        }
    }
}
