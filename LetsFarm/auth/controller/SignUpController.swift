//
//  SignUpController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 12/05/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class SignUpController: BaseController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = "Create an account"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        self.scrollView = getMainScrollView(mainSv)
        
//        view.addConstraints(format: "V:|-120-[v0]-20-|", views: self.scrollView!)
//        view.addConstraints(formats: ["H:|-30-[v0]-30-|"], views: self.scrollView!)
        
        self.scrollView?.backgroundColor = .white
        
        self.scrollView = getMainScrollView(mainSv)

        view.addConstraints(format: "V:|-120-[v0]-20-[v1]-20-[v2]-20-|", views: welcomeLb, signUpLb, self.scrollView!)
        view.addConstraints(formats: ["H:|-20-[v0]-20-|"], views: self.scrollView!)
        view.addConstraints(formats: ["H:|-20-[v0]"], views: welcomeLb, signUpLb)

        
        if useDummyData{
            useDummyData()
        }
    }
    
    func useDummyData(){
        firstNameTf.text = "Adekola"
        lastNameTf.text = "Akano"
        emailTf.text = "akano.adekola@gmail.com"
        phoneNumberTf.text = "08099411130"
        passwordTf.text = "kkkkkk"
        confirmPasswordTf.text = "kkkkkk"
    }
    
    lazy var welcomeLb = getLabel(text: "Welcome to Letsfarm.co", fontSize: 15, textColor: .black)
    
    lazy var signUpLb = getLabel(text: "Sign Up", fontSize: 35, boldFont: true, textColor: .black)
    
    lazy var firstNameTf = getLabelledTextField(placeholder: "First name", labelText: "Enter your first name", keyboardType: .default)
    
    lazy var lastNameTf = getLabelledTextField(placeholder: "Last name", labelText: "Enter your last name", keyboardType: .default)

    lazy var emailTf = getLabelledTextField(placeholder: "Email", labelText: "Enter your email", keyboardType: .emailAddress)

    lazy var phoneNumberTf = getLabelledTextField(placeholder: "PhoneNumber", labelText: "Enter your phone number", keyboardType: .phonePad)

    lazy var passwordTf = getLabelledTextField(placeholder: "Password", labelText: "Enter your password", keyboardType: .default, isPassword:true)
    
    lazy var confirmPasswordTf = getLabelledTextField(placeholder: "Confirm password", labelText: "Confirm your password", keyboardType: .default, isPassword:true)
    
    lazy var referralCodeTf = getLabelledTextField(placeholder: "Referral code (optional)", labelText: "Enter your referral code", keyboardType: .default)

    lazy var signUpBtn = getButton(title: "Create account", action: #selector(signUp))
    
    lazy var alreadyHaveAccntLb = getLabel(text: "Already have an account? Log in", fontSize: 15, boldFont: true, textColor: .primaryColour, action: #selector(login))
    
    lazy var loginLb = getLabel(text: "", fontSize: 12, textColor: UIColor.primaryColour, action: #selector(login))
    
    @objc func login(){        
        if let sd = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate) {
            sd.changeRootViewContoller(LoginController())
        }
    }
    
    lazy var createAccountVw:UIView = {
        let view = UIView()
        view.alignHorizontal(views: alreadyHaveAccntLb, loginLb)
        view.addConstraints(format: "V:|[v0]-10-[v1]|", views: alreadyHaveAccntLb, loginLb)
        return view
    }()
    
    lazy var termsVw:UIView = {
        let view = UIView()
//        view.alignVertical(views: termsSt, termsLb)
        view.addConstraints(format: "H:|[v0]-10-[v1]-10-[v2]|", views: termsLb, termsLb2, termsSt)
        view.addConstraints(format: "V:|-10-[v0]-10-|", views: [termsLb, termsLb2, termsSt])
        return view
    }()
    
    lazy var mainSv = getStackView(firstNameTf, lastNameTf, emailTf, phoneNumberTf, passwordTf, confirmPasswordTf, referralCodeTf, termsVw, signUpBtn, createAccountVw)

    @objc func signUp(){
        if firstNameTf.isValid(4) && emailTf.textField.isEmailValid() && phoneNumberTf.isValid(11) && passwordTf.textField.isPasswordMatch(confirmPasswordTf.textField) && isTermsAccepted() {
            let signUpReq = SignUpReq()
            signUpReq.firstName = firstNameTf.text!
            signUpReq.lastName = lastNameTf.text!
            signUpReq.referralCode = referralCodeTf.text ?? ""
            signUpReq.email = emailTf.text!
            signUpReq.telephone = phoneNumberTf.text!
            signUpReq.password = passwordTf.text!
            
            makeHttpCall(.signUp, .post, signUpReq.toDictionary()){data in
                
                do{
                    let genRes = try JSONDecoder().decode(GeneralResponse.self, from: data)
                
                    self.showDialog(message: genRes.msg ?? "Sign up Succeeful"){ _ in
                        if let sd = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate) {
                            sd.changeRootViewContoller(LoginController())
                        }
                    }
                }catch{}
                
//                let verificationController = VerificationController()
//                verificationController.email = self.emailTf.text!
//                self.navigationController?.pushViewController(verificationController, animated: true)
            }
        }
    }
    
    func isTermsAccepted()->Bool{
        let valid = termsSt.isOn
        if !valid{
            showDialog(message: "Kindly accept our terms and conditions", title: "Info")
        }
        return valid
    }
    
    lazy var termsSt : UISwitch = {
        let mSwitch = UISwitch(frame:CGRect(x: 10, y: 10, width: 10, height: 10))
//        mSwitch.setOn(false, animated: false)
//        mSwitch.addTarget(self, action: #selector(remeberMeSwitch(sender:)), for: .valueChanged)
        return mSwitch
    }()
    
    lazy var termsLb:UILabel={
        let label = UILabel()
        label.text = "Kindly accept our "
        label.font = UIFont.systemFont(ofSize: 13)
        label.addGesture(UITapGestureRecognizer(target: self, action: #selector(showTerms)))
        return label
    }()
    
    lazy var termsLb2:UILabel={
        let label = UILabel()
        label.text = "terms and conditions"
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textColor = UIColor.primaryColour
        label.addGesture(UITapGestureRecognizer(target: self, action: #selector(showTerms)))
        return label
    }()
    
    @objc func showTerms(){
        let webURL = NSURL(string: "https://cleanagent.ng/#/?section=policy")!

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(webURL as URL, options:
            [:], completionHandler: nil)
        }
        else {UIApplication.shared.openURL(webURL as URL)
        }
    }
}
