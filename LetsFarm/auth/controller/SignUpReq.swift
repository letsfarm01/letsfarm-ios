//
//  SignUpReq.swift
//  LetsFarm
//
//  Created by Akano Adekola on 12/05/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation


class SignUpReq : NSObject{

    var email : String!
    var firstName : String!
    var lastName : String!
    var referralCode : String!

    var password : String!
    var telephone : String!

    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if email != nil{
            dictionary["email"] = email
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if password != nil{
            dictionary["password"] = password
        }
        if referralCode != nil{
            dictionary["referral_code"] = referralCode
        }
        if telephone != nil{
            dictionary["phone_number"] = telephone
        }
        return dictionary
    }

}
