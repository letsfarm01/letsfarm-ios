//
//  SplashController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 10/05/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class SplashController: BaseController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        tabBarController?.tabBar.isHidden = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        nameVw.axis = .horizontal
        nameVw.spacing = 0
        
        view.alignVertical(views: logoImg)
        view.alignHorizontal(views: logoImg, nameVw)
        
        view.addConstraints(format: "V:[v0]-20-[v1]", views: logoImg, nameVw)
        
        checkAppVersion()

        
//        let scene = UIApplication.shared.connectedScenes.first
//        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
//            sd.changeRootViewContoller(PersonalInfoController())
//        }
    }
    
    lazy var logoImg = getImage(#imageLiteral(resourceName: "logo"), height: 100, width: 100)
    lazy var appName = getLabel(text: "LETS", fontSize: 21, boldFont: true, textColor: .primaryColour)
    lazy var appName2 = getLabel(text: "FARM", fontSize: 21, boldFont: true, textColor: .secondaryColour)
    
    lazy var nameVw = getStackView(appName, appName2)

    func checkAppVersion(){
        makeHttpCall(.appVersion, .get){data in
            let minAppVersion = (try? JSONDecoder().decode(AppVersionRes.self, from: data).data?.minAllowedIosVersion) ?? "0"
//            let minAppVersion = "3"
            let appVersion = (Bundle.main.infoDictionary?["CFBundleVersion"] as? String) ?? "0"
            
            if minAppVersion > appVersion{
                self.showDialog(message: "Kindly update your app to enjoy new and amazing features", title: "Update Required!"){_ in
//                    self.openAppStore(NSURL(string: "")!, NSURL(string: "")!)
                }
            }else{
                self.enterApp()
            }
            
        }
    }
 
    func enterApp(){
        let scene = UIApplication.shared.connectedScenes.first
        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
            if DefaultsManager.remeberMe() {
                sd.changeRootViewContoller(TabBarController())
            }else{
                sd.changeRootViewContoller(LoginController())
            }
        }
    }
    
    func openAppStore(_ appURL:NSURL, _ webURL:NSURL){

        if UIApplication.shared.canOpenURL(appURL as URL) {
             if #available(iOS 10.0, *) {
                 UIApplication.shared.open(appURL as URL, options:[:], completionHandler: nil)
             }
             else {
                UIApplication.shared.openURL(appURL as URL)
             }
         }
        else {
             if #available(iOS 10.0, *) {
                 UIApplication.shared.open(webURL as URL, options:
                 [:], completionHandler: nil)
             }
             else {UIApplication.shared.openURL(webURL as URL)
             }
        }
    }
}
