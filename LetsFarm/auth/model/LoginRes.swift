//
//  LoginRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 12/05/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct LoginRes : Codable {

    let code : Int?
    let data : LoginResData?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }

}


struct LoginResData : Codable {

    let token : String?
    let user : User?

    enum CodingKeys: String, CodingKey {
        case token = "token"
        case user = "user"
    }

}

struct User : Codable {

    let v : Int?
    let _id : String?
    let authType : String?
    let deleted : Bool?
    let email : String?
    let firstName : String?
    let fullName : String?
    let id : String?
    let isAuthSignUp : Bool?
    let isVerified : Bool?
    let lastName : String?
    let phoneNumber : String?
    let referralCode : String?
    let role : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case _id = "_id"
        case authType = "auth_type"
        case deleted = "deleted"
        case email = "email"
        case firstName = "first_name"
        case fullName = "full_name"
        case id = "id"
        case isAuthSignUp = "is_auth_sign_up"
        case isVerified = "is_verified"
        case lastName = "last_name"
        case phoneNumber = "phone_number"
        case referralCode = "referral_code"
        case role = "role"
    }

}
