//
//  BankTransfersController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
// MARK: - your text goes here

class BankTransfersController: BaseController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var bankTransfersResData:[BankTransfersResData]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true

        view.backgroundColor = .white
        navigationItem.title = "Bank Transfers"

        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        view.addConstraints(format: "H:|-20-[v0]-20-|", views: [bankTransfersCv])
        view.addConstraints(format: "V:|-110-[v0]-25-|", views: bankTransfersCv)
        fetchbankTransfers()

    }
    
    lazy var bankTransfersCv = getCollectionView(spacing: 20, cell: BankTransfersCell.self, identifier: "bankTransfersCell", delegate: self, dataSource: self)

    
    func fetchbankTransfers(){
        self.makeHttpCall(.fetchPendingTransfers, .get, path:DefaultsManager.getUserId()){data in
            self.bankTransfersResData = try? JSONDecoder().decode(BankTransfersRes.self, from: data).data
            self.bankTransfersCv.reloadData()
        }
    }

    @objc func back(sender: UIBarButtonItem) {
//        navigationController?.pushViewController(TabBarController(), animated: true)
        
        let scene = UIApplication.shared.connectedScenes.first
        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
            sd.changeRootViewContoller(TabBarController())
        }
    }
    
// MARK: your text goes here
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-50, height: 75)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bankTransfersResData?.count ?? 0
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let position = indexPath.item
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bankTransfersCell", for: indexPath) as! BankTransfersCell
            cell.tag = position
            cell.addGesture(UITapGestureRecognizer(target: self, action: #selector(onCellTap(sender:))))
            cell.data = self.bankTransfersResData![indexPath.item]
            return cell
    }
    
    @objc func onCellTap(sender:UITapGestureRecognizer){
        if let position = sender.view?.tag{
            let transfer = bankTransfersResData![position]
            
            let uploadPaymentProofController = UploadPaymentProofController()
            uploadPaymentProofController.transactionRef = transfer.orderReference
            uploadPaymentProofController.transactionId = transfer.id
            uploadPaymentProofController.amount = transfer.totalPrice
            
            navigationController?.pushViewController(uploadPaymentProofController, animated: true)
        }
    }
}
