//
//  PaymentInstructionsController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class PaymentInstructionsController: BaseController {

    var accountNumber:String = "0138013221"
    var amount:String = ""
    var reference:String = ""
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
                    
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.addConstraints(format: "H:|-15-[v0]", views: copyAccountBtn)
        view.addConstraints(format: "H:[v0]-15-|", views: copyReferenceBtn)

//        view.addConstraints(format: "H:[v0]-1-[v1]", views: copyAccountBtn, copyReferenceBtn)

        
        view.addConstraints(format: "V:|-20-[v0]-15-[v1]-20-[v2]-20-|", views: backImg, copyAccountBtn, detailsLb)
        view.addConstraints(format: "H:|-15-[v0]", views: backImg)
        view.addConstraints(format: "H:|-15-[v0]-15-|", views: detailsLb)

        copyReferenceBtn.alignBottomToBottomOf(of: copyAccountBtn)
        
//        let width = (view.frame.width - 31)/2
        
        copyReferenceBtn.setHeight(height: 18)
        copyAccountBtn.setHeight(height: 18)
        
//        copyReferenceBtn.setWidth(width: width)
//        copyAccountBtn.setWidth(width: width)

        copyAccountBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        copyReferenceBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        
        detailsLb.textAlignment = .left
    }
    
    lazy var detailsLb = getLabel(text: "1) Make payment using Online banking e.g Bank App on Phone or Web\n" +
    "\nLogin to your online banking app and make a transfer into any of our list banks:\n" +
    "\nBank Name: Union Bank of Nigeria\nAccount Number: \(accountNumber)\nAccount Name: Letsfarm Integrated Services Ltd\n" +
    "Amount: \(amount)\nNarration/Comment: \(reference)\n\n" +
    "2) Bank transfer procedure using bank teller\n" +
    "\nWalk into any of the under listed bank and fill teller to make payment:\n" +
    "\n" +
    "Bank Name: Union Bank of Nigeria\n" +
    "Account Number: \(accountNumber)\n" +
    "Account Name: Letsfarm Integrated Services Ltd\n" +
    "Amount: \(amount)\n" +
    "Depositor's Name: Your registered name on Letsfarm\nNarration/Comment: \(reference)\n\n" +
        "N.B: On top of the deposit slip, kindly write this number \(reference) \n", fontSize: 15, boldFont: false, textColor: .black)
    
    lazy var backImg = getImage(#imageLiteral(resourceName: "back"), height: 30, width: 30, action: #selector(goBack))
    
    lazy var copyAccountBtn = getButton(title: "Copy account number", bgColor: .white, borderColor: .primaryColour, titleColor: .primaryColour, cornerRadius: 5, action: #selector(copyAccountNumber))
//    Copy reference code
    lazy var copyReferenceBtn = getButton(title: "Copy reference code", bgColor: .white, borderColor: .primaryColour, titleColor: .primaryColour, cornerRadius: 5, action: #selector(copyReferenceCode))
    
    @objc func copyAccountNumber(){
        let pasteboard = UIPasteboard.general
        pasteboard.string = accountNumber
        showDialog(message: "Account number has been copied to clipboard")
    }
    
    @objc func copyReferenceCode(){
        let pasteboard = UIPasteboard.general
        pasteboard.string = reference
        showDialog(message: "Reference code has been copied to clipboard")
    }
    
    @objc func goBack(){
         dismiss(animated: true)
    }
    
}
