//
//  UploadPaymentProofController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog
import FirebaseStorage

class UploadPaymentProofController: BaseController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    
    var userImage:UIImage?{
        didSet{
            self.fileUploadLb.text = "File uploaded"
        }
    }
    
    var amount:Int?
    var transactionId:String?
    var transactionRef:String?
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       
       navigationController?.isNavigationBarHidden = false
       tabBarController?.tabBar.isHidden = true
   }
       
    override func viewDidLoad() {
       super.viewDidLoad()

        view.backgroundColor = .white
        navigationItem.title = "Proof of payment"
        
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        view.addConstraints(format: "V:|-120-[v0]-20-[v1]-20-[v2]-20-[v3]", views: instructionLb, uploadBtn, fileUploadLb, commentTf)
        
        view.addConstraints(formats: ["H:|-20-[v0]"], views: instructionLb, fileUploadLb)
        
        view.addConstraints(formats: ["H:|-20-[v0(150)]"], views: uploadBtn)

        view.addConstraints(formats: ["H:|-20-[v0]-20-|"], views: commentTf)

        view.addConstraints(formats: ["H:|-20-[v0]-20-|", "V:[v0]-20-|"], views: submitBtn)

        viewPaymentInstructions()

    }
    
    
    lazy var instructionLb = getLabel(text: "View payment Instructions", fontSize: 15, boldFont: true, textColor: .black,  action: #selector(viewPaymentInstructions))
    
    lazy var fileUploadLb = getLabel(text: "", fontSize: 13)

    lazy var uploadBtn = getButton(title: "Upload proof", bgColor: .white, borderColor: .primaryColour, titleColor: .primaryColour, cornerRadius: 6, action: #selector(picProof))
    
    lazy var commentTf = getLabelledTextField(placeholder: "Comment", labelText: "Comment (optional)")
    
    lazy var submitBtn = getButton(title: "Submit", action: #selector(uploadImage))
    
    @objc func back() {
        navigationController?.pushViewController(BankTransfersController(), animated: true)
    }
    
    func submit(url:String){
        
        let documentUpload = DocumentUpload()
        documentUpload.transactionId = transactionId
        documentUpload.transactionRef = transactionRef
        documentUpload.userId = DefaultsManager.getUserId()
        documentUpload.bankName = ""
        documentUpload.message = commentTf.trimmedText
        documentUpload.amount = amount

        let document = Document()
        document.url = url
        documentUpload.documents.append(document)
        
        makeHttpCall(.uploadProof, .post, documentUpload.toDictionary()){data in
            let dataDict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
            let message:String? = dataDict?["msg"] as? String
            
            self.showDialog(message: message ?? "Proof uploaded successfully", title: "Success", disableBackgroundTap: true){_ in
                self.back()
            }
        }
        
    }
    
    @objc func viewPaymentInstructions(){
        let controller = PaymentInstructionsController()

        controller.amount = "₦\(amount?.withCommas() ?? "0")"
        controller.reference = transactionRef ?? ""
        let popup = PopupDialog(viewController: controller, transitionStyle: .bounceDown, tapGestureDismissal: true, panGestureDismissal: false)

        present(popup, animated: true, completion: nil)
    }
    

        
    @objc func picProof(){
        let vc = UIImagePickerController()
    //        vc.sourceType = .camera
            vc.sourceType = .photoLibrary
            vc.allowsEditing = true
            vc.delegate = self
            present(vc, animated: true)
    }
            
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        userImage = image
        
    }
    
    
    @objc func uploadImage(){
        
        let indicator = activityIndicator
        indicator.startAnimating()
        
        let storageRef = Storage.storage().reference()
        
        let fileName = userImage?.accessibilityIdentifier ?? "\(Date().string(format: "MMddyyyyHHmmssSSSZ"))"
        
        let fileRef = storageRef.child("letsfarm_images/proof_payment/\(fileName).jpg")

        fileRef.putData((userImage?.pngData())!, metadata: nil) { (metadata, error) in
            
            indicator.removeFromSuperview()
            
            fileRef.downloadURL { (url, error) in
                if let downloadURL = url {
                    self.submit(url: "\(downloadURL)")
                    print("downloadURL \(downloadURL)")
                }
            }
        }
    }
}
