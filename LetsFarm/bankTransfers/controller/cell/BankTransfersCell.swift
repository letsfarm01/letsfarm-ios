//
//  BankTransfersCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 16/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class BankTransfersCell: UICollectionViewCell {
    
    var data : BankTransfersResData?{
        didSet{
            descrLb.text = data?.narration
            amountLb.text = "₦\(data?.totalPrice?.withCommas() ?? "0")"
            referenceLb.text = data?.orderReference
            instructionLb.text = data?.paymentProof == nil ? "Upload proof of payment" : "Awaiting confirmation"
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.init(hex: "#f6f6f6")
        
        setShadow()

        layer.cornerRadius = 4
        
        addConstraints(format: "H:[v0]-10-|", views: amountLb)
        addConstraints(format: "H:|-10-[v0]-10-[v1]", views: descrLb, amountLb)
        
        addConstraints(format: "H:[v0]-10-|", views: instructionLb)
        addConstraints(format: "H:|-10-[v0]-10-[v1]-10-|", views: referenceLb, instructionLb)

        addConstraints(format: "V:|-10-[v0]-10-[v1]-10-|", views: descrLb, referenceLb)
        
        amountLb.alignTopToTopOf(of: descrLb)
        instructionLb.alignTopToTopOf(of: referenceLb)

    }
    

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var descrLb = getLabel(text: "Fish farm at 30% in 8 Months", fontSize: 15, boldFont: true, textColor: .black)
    
    lazy var amountLb = getLabel(text: "Start of cycle: Jun 30, 2020", fontSize: 15, boldFont: true, textColor: .black)
    lazy var referenceLb = getLabel(text: "End of cycle: Jun 30, 2020", fontSize: 13)
    
    lazy var instructionLb = getLabel(text: "Amount invested: N65,000", fontSize: 13, textColor: #colorLiteral(red: 0, green: 0.8894402385, blue: 0.1355991662, alpha: 1))
}
