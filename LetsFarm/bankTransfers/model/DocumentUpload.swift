//
//  DocumentUpload.swift
//  LetsFarm
//
//  Created by Akano Adekola on 17/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

class DocumentUpload : NSObject{

    var amount : Int!
    var bankName : String!
    var documents = [Document]()
    var message : String!
    var transactionId : String!
    var transactionRef : String!
    var userId : String!


    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amount != nil{
            dictionary["amount"] = amount
        }
        if bankName != nil{
            dictionary["bank_name"] = bankName
        }
        if message != nil{
            dictionary["message"] = message
        }
        if transactionId != nil{
            dictionary["transactionId"] = transactionId
        }
        if transactionRef != nil{
            dictionary["transactionRef"] = transactionRef
        }
        if userId != nil{
            dictionary["userId"] = userId
        }
        if documents != nil{
            var dictionaryElements = [[String:Any]]()
            for documentsElement in documents {
                dictionaryElements.append(documentsElement.toDictionary())
            }
            dictionary["documents"] = dictionaryElements
        }
        return dictionary
    }

}

class Document : NSObject{

    var url : String!


    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if url != nil{
            dictionary["url"] = url
        }
        return dictionary
    }

}
