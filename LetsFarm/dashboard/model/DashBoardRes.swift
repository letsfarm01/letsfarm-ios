//
//  DashBoardRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
struct DashBoardRes : Codable {

    let code : Int?
    let data : DashBoardResData?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
            case code = "code"
            case data = "data"
            case msg = "msg"
            case status = "status"
    }


}
struct DashBoardResData : Codable {

    let activeSponsorship : ActiveSponsorship?
    let expectedReturns : ExpectedReturn?

    enum CodingKeys: String, CodingKey {
        case activeSponsorship = "active_sponsorship"
        case expectedReturns = "expected_returns"
    }


}

struct ExpectedReturn : Codable {

    let currency : String?
    let date : Int?
    let descriptionField : [String]?
    let interests : [Int]?
    let invested : [Int]?
    let names : [String]?
    let numberOfFarms : Int?
    let returns : [Int]?
    let spike : [Int]?
    let total : Int?

    enum CodingKeys: String, CodingKey {
            case currency = "currency"
            case date = "date"
            case descriptionField = "description"
            case interests = "interests"
            case invested = "invested"
            case names = "names"
            case numberOfFarms = "number_of_farms"
            case returns = "returns"
            case spike = "spike"
            case total = "total"
    }


}

struct ActiveSponsorship : Codable {

    let currency : String?
    let date : Int?
    let duration : Int?
    let durationType : String?
    let endDate : String?
    let interest : Int?
    let name : String?
    let roi : Int?
    let spike : [Int]?
    let startDate : String?
    let total : Int?

    enum CodingKeys: String, CodingKey {
            case currency = "currency"
            case date = "date"
            case duration = "duration"
            case durationType = "duration_type"
            case endDate = "end_date"
            case interest = "interest"
            case name = "name"
            case roi = "roi"
            case spike = "spike"
            case startDate = "start_date"
            case total = "total"
    }


}
