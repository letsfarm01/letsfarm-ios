//
//  WalletBalanceRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 15/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct WalletBalanceRes : Codable {

    let code : Int?
    let data : WalletBalanceResData?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }

}
struct WalletBalanceResData : Codable {

    let v : Int?
    let id : String?
    let createdAt : String?
    let deleted : Bool?
    let lastFundDate : String?
    let status : Bool?
    let updatedAt : String?
    let userId : String?
    let walletBalance : Int?

    enum CodingKeys: String, CodingKey {
            case v = "__v"
            case id = "_id"
            case createdAt = "createdAt"
            case deleted = "deleted"
            case lastFundDate = "last_fund_date"
            case status = "status"
            case updatedAt = "updatedAt"
            case userId = "userId"
            case walletBalance = "wallet_balance"
    }


}
