//
//  AllOrdersRes.swift
//  CleanAgent
//
//  Created by Dekola Ak on 26/01/2020.
//  Copyright © 2020 Dekola Ak. All rights reserved.
//

import Foundation

struct AllOrdersRes : Codable {

    let orderItems : [AllOrdersItems]?
    let isFavoriteCleaner : Bool?
    let address : String?
    let businessName : String?
    let cleanerId : Int?
    let canReview : Int?
    let canCancel : Bool?
    let createdAt : String?
    let deliveryDate : String?
    let id : Int?
    let notes : String?
    let orderStatus : String?
    let pickupDate : String?
    let serviceType : String?
    let serviceTypeAmount : String?
    let totalAmount : String?
    let transactionId : String?
    let ratings : Int?

    enum CodingKeys: String, CodingKey {
            case ratings = "ratings"
            case canCancel = "can_cancel"
            case canReview = "can_review"
            case orderItems = "order_items"
            case isFavoriteCleaner = "is_favorite_cleaner"
            case address = "address"
            case businessName = "business_name"
            case cleanerId = "cleaner_id"
            case createdAt = "created_at"
            case deliveryDate = "delivery_date"
            case id = "id"
            case notes = "notes"
            case orderStatus = "order_status"
            case pickupDate = "pickup_date"
            case serviceType = "service_type"
            case serviceTypeAmount = "service_type_amount"
            case totalAmount = "total_amount"
            case transactionId = "transaction_id"
    }
}

struct AllOrdersItems : Codable {

    let cleaners2clothingItemsId : Int?
    let clothingItemId : Int?
    let ironOnly : Int?
    let isBulk : Int?
    let itemName : String?
    let orderItemId : Int?
    let price : Int?
    let quantity : Int?
    let washIron : Int?
    let washOnly : Int?

    enum CodingKeys: String, CodingKey {
        case cleaners2clothingItemsId = "cleaners2clothing_items_id"
        case clothingItemId = "clothing_item_id"
        case ironOnly = "iron_only"
        case isBulk = "is_bulk"
        case itemName = "itemName"
        case orderItemId = "order_item_id"
        case price = "price"
        case quantity = "quantity"
        case washIron = "wash_iron"
        case washOnly = "wash_only"
    }

}
