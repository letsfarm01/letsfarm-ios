//
//  Profile.swift
//  CleanAgent
//
//  Created by Dekola Ak on 22/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation


struct GetProfileRes : Codable {

    let user : User?

    enum CodingKeys: String, CodingKey {
            case user = "user"
    }
}


struct User : Codable {

    let address : String?
    let confirmed : Int?
    let email : String?
    let firstName : String?
    let id : Int?
    let lastName : String?
    let referralCode : String?
    let telephone : String?

    enum CodingKeys: String, CodingKey {
            case address = "address"
            case confirmed = "confirmed"
            case email = "email"
            case firstName = "first_name"
            case id = "id"
            case lastName = "last_name"
            case referralCode = "referral_code"
            case telephone = "telephone"
    }
}

class UpdateProfileReq : NSObject{

    var address : String!
    var firstName : String!
    var lastName : String!
    var telephone : Int!

    var password : String!
    var oldPassword : String!

    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if telephone != nil{
            dictionary["telephone"] = telephone
        }
        
        
        if password != nil{
            dictionary["password"] = password
        }
        if oldPassword != nil{
            dictionary["old_password"] = oldPassword
        }
        
        return dictionary
    }
}

struct UpdateProfileRes : Codable {

    let status : Int?

    enum CodingKeys: String, CodingKey {
        case status = "status"
    }
    
}
