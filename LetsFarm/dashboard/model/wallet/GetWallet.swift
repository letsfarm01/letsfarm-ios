//
//  GetWallet.swift
//  CleanAgent
//
//  Created by Dekola Ak on 23/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation

struct GetWalletRes : Codable {

    let data : WalletData?

    enum CodingKeys: String, CodingKey {
            case data = "data"
    }
}
struct WalletData : Codable {

    let balance : String?

    enum CodingKeys: String, CodingKey {
            case balance = "balance"
    }
}
