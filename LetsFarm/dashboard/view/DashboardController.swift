//
//  DashboardController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog

class DashboardController: BaseController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate{
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        tabBarController?.tabBar.isHidden = false
        
        fetchDashBoardInfo()
        fetchWalletBalance()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        navigationItem.title = ""
        view.backgroundColor = .white
        
        view.addConstraints(formats: ["H:|-15-[v0]", "V:|-55-[v0]"], view: walletView)
        view.addConstraints(formats: ["H:[v0]-15-|", "V:|-55-[v0(50)]"], view: profileImg)
        view.addSubview(welcomeLb)
        view.addConstraints(format: "V:[v0]-5-[v1]", views: profileImg, welcomeLb)
        
        view.addConstraints(format: "V:[v0]-25-[v1]-25-[v2]-25-[v3]-100-|", views: walletView, accountSummaryLb, accountSummaryVw, menuCv)
        
        view.addConstraints(format: "H:|-15-[v0]", views: accountSummaryLb)
        
        view.addConstraints(format: "H:|-15-[v0]-15-|", views: [accountSummaryVw, menuCv])
        
        welcomeLb.alignEndToEndOf(of: profileImg)
        
        profileImg.layer.cornerRadius = 25
        profileImg.clipsToBounds = true
        
        fecthUserInfo()
        
    }
    
    func fetchDashBoardInfo(){
        makeHttpCall(.fetchDashboardInfo, .get){data in
            if let dashInfo = try? JSONDecoder().decode(DashBoardRes.self, from: data).data{
                self.expectedReturnsLb.text = "₦\(dashInfo.expectedReturns?.total?.withCommas() ?? "0")"
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                self.nextPayoutDateLb.text = (dashInfo.activeSponsorship?.endDate ?? "").getFormattedDate()
            }

        }
    }
    
    
    func fetchWalletBalance(){
        
        makeHttpCall(.getWalletBalance, .get, path:DefaultsManager.getUserId()){data in
            let walletInfo = try? JSONDecoder().decode(WalletBalanceRes.self, from: data)
            self.walletBalanceLb.text = "₦\(walletInfo?.data?.walletBalance?.withCommas() ?? "0")"

        }
    }
    
    lazy var walletBalanceTitleLb = getLabel(text: "Wallet Balance")
    lazy var walletBalanceLb = getLabel(text: "₦---", fontSize: 17, boldFont: true, textColor: .black)
    lazy var fundWalletLb = getLabel(text: "Fund Wallet", fontSize: 17, boldFont: true, textColor: .primaryColour)

    lazy var walletView:UIView = {
        let view = UIView()
        view.addGesture(UITapGestureRecognizer(target: self, action: #selector(fundWallet)))
        view.addConstraints(format: "V:|[v0]-5-[v1]-5-[v2]|", views: walletBalanceTitleLb, walletBalanceLb, fundWalletLb)
        view.addConstraints(format: "H:|[v0]|", views: [walletBalanceTitleLb, walletBalanceLb, fundWalletLb])

        self.walletBalanceLb.textAlignment = .left

        return view
    }()
    
    var paymentOptions:[PaymentChannelResData]?
//    var cardCharges:TransactionChargeResData?

    @objc func fundWallet(){
        if paymentOptions != nil {
            showAmountDialog()
        }else{
            fetchPaymentOptions()
        }
    }
    
    func fetchPaymentOptions(){
        
        makeHttpCall(.getPaymentOptionWallet, .get){data in
            self.paymentOptions = try? JSONDecoder().decode(PaymentChannelRes.self, from: data).data
            self.showAmountDialog()
        }
    }
    
    func showAmountDialog(){
        showDialog(message: "Enter the amount", title: "Amount", disableBackgroundTap: false, tfListener: {amount in
            let amount = Int(amount)!
            self.getCardPaymentCharges(amount)
        })
    }
    
    func getCardPaymentCharges(_ amount:Int){
        
        let data = ["amount":amount]
        
        makeHttpCall(.getCardPaymentCharges, .post, data){data in
            let cardCharges = try? JSONDecoder().decode(TransactionChargeRes.self, from: data).data
            //todo remove forced unwrapping
            self.showPaymentOptions(amount, cardCharges!)
        }
    }
    
    func showPaymentOptions(_ amount:Int, _ cardCharges:TransactionChargeResData){
        
        let controller = PaymentOptionController()

        controller.paymentOptionSelection = {paymentChannel in
            self.paymentOptionSelection(paymentChannel, amount)
        }

        controller.paymentOptions = paymentOptions
        controller.cardCharges = cardCharges
        controller.totalPrice = amount

        let popup = PopupDialog(viewController: controller, transitionStyle: .bounceDown, tapGestureDismissal: true, panGestureDismissal: false)

        present(popup, animated: true, completion: nil)
    }
    
    func paymentOptionSelection(_ paymentChannelResData:PaymentChannelResData, _ amount:Int){
        if (paymentChannelResData.name == "Paystack"){

            let controller = CardSelectionController()
            controller.savedCardListener = {cardId in
                self.processPayment(paymentChannel:paymentChannelResData, amount:amount, cardId:cardId)
            }
            controller.newCardListener = {
                self.processPayment(paymentChannel:paymentChannelResData, amount:amount)
            }

            let popup = PopupDialog(viewController: controller, transitionStyle: .bounceDown, tapGestureDismissal: true, panGestureDismissal: false)
            
            present(popup, animated: true, completion: nil)
            
        }else{
            self.processPayment(paymentChannel:paymentChannelResData, amount:amount)
        }
    }
    
    func processPayment(paymentChannel:PaymentChannelResData, amount: Int, cardId:String? = nil){
        
        let dict:[String:String] = ["amount":"\(amount)", "payment_gateway":paymentChannel.id ?? "", "card_id":cardId ?? "", "userId":DefaultsManager.getUserId()]
        
        if cardId != nil {
            makeHttpCall(.fundWalletCard, .post, dict){data in
                let dataDict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                let message:String? = dataDict?["msg"] as? String
                
                self.showSuccess(message)

            }
        }else{
            makeHttpCall(.fundWallet, .post, dict){data in
                let checkOutRes = try? JSONDecoder().decode(CheckOutRes.self, from: data)

                if paymentChannel.name == "Paystack"{
                                
                    let checkOutRes = try? JSONDecoder().decode(CheckOutRes.self, from: data)

                    if let details = checkOutRes?.data?.details{
                        let controller = CheckOutNewCardController()
                        
                        controller.isWalletFunding = true
                        controller.ref = details.ref
                        controller.accessCode = details.accessCode
                        controller.transactionId = details.transactionId
                        controller.totalPrice = details.total
                        
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                    
                }else if paymentChannel.name == "Bank Transfer" {
                    
                    let dataDict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                    let message:String? = dataDict?["msg"] as? String
                    
                    self.showDialog(message: message ?? "Wallet Successfully funded", title: "Success", disableBackgroundTap: true){_ in
                        self.showPaymentProof(checkOutRes)
                    }

                }else{
                    self.showSuccess()
                }
            }
        }
        
        
        
    }
    
    func showPaymentProof(_ checkOutRes:CheckOutRes?){
        let controller = UploadPaymentProofController()
        
        controller.transactionId = checkOutRes?.data?.transactionId
        controller.transactionRef = checkOutRes?.data?.transactionRef
        controller.amount = checkOutRes?.data?.amount
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showSuccess(_ message:String? = nil){
        self.showDialog(message: message ?? "Wallet Successfully funded", title: "Success", disableBackgroundTap: true)
        fetchDashBoardInfo()
        fetchWalletBalance()
    }
    
    lazy var profileImg = getImage(#imageLiteral(resourceName: "profile"), height: 50, width: 50)
    lazy var welcomeLb = getLabel(text: "Welcome")
    
    lazy var accountSummaryLb = getLabel(text: "Account Summary", fontSize: 15)

    lazy var expectedReturnsTitleLb = getLabel(text: "Expected returns", textColor: .white)
    lazy var expectedReturnsLb = getLabel(text: "₦---", fontSize: 15, boldFont: true, textColor: .white)
    
    lazy var nextPayoutDateTitleLb = getLabel(text: "Next Payout date", textColor: .white)
    lazy var nextPayoutDateLb = getLabel(text: "-----", fontSize: 15, boldFont: true, textColor: .white)

    lazy var accountSummaryVw:UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.backgroundColor = .primaryColour
        
        view.addConstraints(formats: ["H:|-15-[v0]"], views: expectedReturnsTitleLb, nextPayoutDateTitleLb)
        view.addConstraints(formats: ["H:[v0]-15-|"], views: expectedReturnsLb, nextPayoutDateLb)

        view.addConstraints(format: "V:|-15-[v0]-10-[v1]-15-|", views: expectedReturnsTitleLb, nextPayoutDateTitleLb)
        view.addConstraints(format: "V:|-15-[v0]-10-[v1]-15-|", views: expectedReturnsLb, nextPayoutDateLb)

        return view
    }()
    
    lazy var menuCv:UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 10
        flowLayout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: "menuCell")
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as! MenuCell
        cell.menuItem = menuItems[indexPath.item]
        cell.tag = indexPath.item
        cell.addGesture(UITapGestureRecognizer(target: self, action: #selector(onMenu(sender:))))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 80)
    }
    
    
    @objc func onMenu(sender:UITapGestureRecognizer){
        let position = sender.view?.tag ?? 0
        var controller:UIViewController?
        switch position{
            case 0: controller = FarmHistoryController()
            case 1: controller = AllFarmController()
            case 3: controller = ReportController()
            case 4: controller = BankTransfersController()
            case 5: controller = AllFarmController()
            case 6: controller = AccountManagerController()
            case 7: controller = ReferralController()
            case 8: controller = AllFarmController()
        default:
            print("Default")
        }
        if let controller = controller {
            navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    var menuItems:[MenuItems] = [MenuItems(image: #imageLiteral(resourceName: "farm_history"), title: "Farm History"), MenuItems(image: #imageLiteral(resourceName: "sponsor_farms"), title: "Sponsor Farms"), MenuItems(image: #imageLiteral(resourceName: "updates"), title: "Updates"), MenuItems(image: #imageLiteral(resourceName: "reports2"), title: "Reports"), MenuItems(image: #imageLiteral(resourceName: "bank_transfer2"), title: "Bank Transfers"), MenuItems(image: #imageLiteral(resourceName: "farm_products2"), title: "Farm Products"), MenuItems(image: #imageLiteral(resourceName: "account_manager"), title: "Account Manager"), MenuItems(image: #imageLiteral(resourceName: "referral"), title: "Referral"), MenuItems(image: #imageLiteral(resourceName: "faq"), title: "FAQ"), ]
    
    
    
    func fecthUserInfo(){

        makeHttpCall(.updatePersonalInfo, .get, path:DefaultsManager.getUserId()){data in
            let userData = try? JSONDecoder().decode(ProfileUpdate.self, from: data).data
            
            if let displayImg =  userData?.userImageUrl, let url = URL(string: displayImg){
                self.profileImg.kf.setImage(with: url)
            }
            self.welcomeLb.text = "Welcome \(userData?.firstName ?? "")"
        }
    }

}

struct MenuItems {
    let image : UIImage?
    let title : String?
}
