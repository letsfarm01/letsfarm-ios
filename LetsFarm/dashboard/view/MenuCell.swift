//
//  MenuCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 14/07/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class MenuCell: UICollectionViewCell {
    
    var menuItem:MenuItems?{
        didSet{
            itemLb.text = menuItem?.title
            itemImg.image = menuItem?.image
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.98, green: 0.98, blue: 0.98, alpha: 1)
        setShadow()
        
        layer.cornerRadius = 5
        addConstraints(format: "V:|-10-[v0(40)]-10-[v1]", views: itemImg, itemLb)
        addConstraints(format: "H:[v0(40)]", views: [itemImg])
//        addConstraints(format: "H:|-5-[v0]-5-|", views: [itemLb])
        alignHorizontal(views: itemLb, itemImg)
    }
    
    
    
    lazy var itemImg:UIImageView={
        let imageView = UIImageView()
        imageView.setCornerRadius(radius: 6)
        return imageView
    }()
    
    lazy var itemLb:UILabel={
        let label = UILabel()
        label.text = "Items"
//        label.backgroundColor = UIColor.red
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 10)
        return label
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
