//
//  OrderCell.swift
//  CleanAgent
//
//  Created by Dekola Ak on 16/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class OrderCell: UICollectionViewCell {
    
    var detailsClick:((AllOrdersRes)->Void)?
    var cancelClick:((AllOrdersRes)->Void)?
    var reviewClick:((AllOrdersRes)->Void)?

    var order:AllOrdersRes?{
        didSet{
            orderNumberValue.text = order?.transactionId
            orderAmountLb.text = "₦\(order?.totalAmount ?? "0")"
            orderStatusLb.text = order?.orderStatus
            
//            ratingBar.text = "\(dryCleaner?.reviews?.ratings ?? 0)"
//            ratingBar.rating = Double(dryCleaner?.reviews?.ratings ?? 0)
            
            if (order?.canCancel == false){
                cancelOrderBtn.isHidden = true
                if(order?.canReview != 1){
                    reviewBtn.isHidden = true
                }
                else{
                    reviewBtn.isHidden = false
                }

                ratingBar.rating = Double(order?.ratings ?? 0)
            }else{
                cancelOrderBtn.isHidden = false
                reviewBtn.isHidden = true
                ratingBar.isHidden = true
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = 8
        layer.masksToBounds = true
        
        orderDetailsBtn.contentEdgeInsets = UIEdgeInsets(top: 10,left: 30,bottom: 10,right: 30)
        cancelOrderBtn.contentEdgeInsets = UIEdgeInsets(top: 10,left: 20,bottom: 10,right: 20)
        reviewBtn.contentEdgeInsets = UIEdgeInsets(top: 10,left: 20,bottom: 10,right: 20)

        addConstraints(format: "V:|[v0(70)]-5-[v1]-10-[v2]-10-|", views: headerVw, orderStatusLb, orderDetailsBtn)
        addConstraints(format: "V:[v0(70)]-15-[v1]|", views: headerVw, ratingBar)
        addConstraints(format: "H:|-15-[v0]", views: [orderStatusLb, orderDetailsBtn])

        addConstraints(format: "H:|[v0]|", views: headerVw)
        addConstraints(format: "H:[v0]-15-|", views: [cancelOrderBtn, reviewBtn, ratingBar])
        
        addConstraints(format: "V:[v0]-10-[v1]", views: orderStatusLb, cancelOrderBtn)
        addConstraints(format: "V:[v0]-10-[v1]", views: orderStatusLb, reviewBtn)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var headerVw:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(hex: "#eeeeee")
        view.addConstraints(format: "V:|-10-[v0]-10-[v1]-10-|", views: orderNumber, orderNumberValue)
        view.addConstraints(format: "H:|-20-[v0]", views: [orderNumber, orderNumberValue])
        view.addConstraints(format: "H:[v0]-20-|", views: orderAmountLb)
        view.addConstraints(format: "V:|-20-[v0]", views: orderAmountLb)
//        alignVertical(views: orderAmountLb)
        return view
    }()
    
    lazy var orderNumber:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Transaction ID"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    lazy var orderNumberValue:UILabel = {
        let label = UILabel()
        label.text = "323231"
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var orderAmountLb:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "₦6,000"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()
    
    lazy var orderStatusLb:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.init(hex: "#D97500")
        label.text = "Order Cancelled"
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var ratingBar:CosmosView = {
        let rating = CosmosView()
        rating.rating = 0
        rating.text = ""
        rating.settings.updateOnTouch = false
        return rating
    }()
    
    lazy var orderDetailsBtn:UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Detaiils", for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderColor = UIColor.customBlue.cgColor
        button.layer.borderWidth = 1
        button.addTarget(self, action: #selector(detailsBtnClick), for: .touchUpInside)
        button.setHeight(height: 30)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.backgroundColor = UIColor.customBlue
        return button
    }()
    
    lazy var cancelOrderBtn:UIButton = {
        let button = UIButton()
        button.setTitleColor(.red, for: .normal)
        button.setTitle("Cancel order", for: .normal)
        button.layer.cornerRadius = 10
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.red.cgColor
        button.layer.borderWidth = 1
        button.setHeight(height: 30)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.addTarget(self, action: #selector(cancelBtnClick), for: .touchUpInside)
        return button
    }()
    lazy var reviewBtn:UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Review order", for: .normal)
        button.layer.cornerRadius = 10
        button.backgroundColor = UIColor.customBlue
        button.layer.borderColor = UIColor.customBlue.cgColor
        button.layer.borderWidth = 1
        button.setHeight(height: 30)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.addTarget(self, action: #selector(reviewBtnClick), for: .touchUpInside)
        return button
    }()
    
    @objc func detailsBtnClick(){
        detailsClick!(self.order!)
    }
    
    @objc func reviewBtnClick(){
        reviewClick!(self.order!)
    }
    
    
    @objc func cancelBtnClick(){
        cancelClick!(self.order!)
    }
    
}
