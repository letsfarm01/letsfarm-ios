//
//  OrderDetailsController.swift
//  CleanAgent
//
//  Created by Dekola Ak on 26/01/2020.
//  Copyright © 2020 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

class OrderDetailsController: BaseController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    var isFavorite:Bool?{
        didSet{
            self.favoriteImg.image = isFavorite == true ? #imageLiteral(resourceName: "favorite_on") : #imageLiteral(resourceName: "favorite_off")
        }
    }
    
    var order:AllOrdersRes?{
        didSet{
            self.transactionNoLb.text = "Transaction Id - \(order?.transactionId ?? "")"
            self.amountLb.text = "₦\(order?.totalAmount ?? "0")"
            self.statusLb.text = "\(order?.orderStatus ?? "")"
            self.dateLb.text = "\(order?.createdAt ?? "")"
            self.isFavorite = order?.isFavoriteCleaner
            
            let nameVw = getDetailsVw("Dry Cleaner", (order?.businessName) ?? "-")
            let pickupAddressVw = getDetailsVw("Pick up Address", (order?.address) ?? "-")
            let pickupDateVw = getDetailsVw("Pick up Date and Time", (order?.pickupDate) ?? "-")
            let deliveryDateVw = getDetailsVw("Delivery Date and Time", (order?.deliveryDate) ?? "-")
            let noteVw = getDetailsVw("Note", (order?.notes) ?? "-")
            
            self.orderDrycleanerItem = order!.orderItems!
            
            if(order?.canReview != 1){
                reviewBtn.isHidden = true
            }
            
            self.mainSv.addArrangedSubview(views: nameVw, pickupAddressVw, pickupDateVw, deliveryDateVw, noteVw)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        navigationItem.title = "Order Details"
        view.backgroundColor = .white
        
        mainSv.spacing = 1
//        self.scrollView = getMainScrollView(mainSv)
        
        view.addConstraints(format: "H:|[v0]|", views: [topVw])
        view.addConstraints(format: "H:|-15-[v0]-15-|", views: [orderCv])
        view.addConstraints(format: "H:|[v0]|", views: self.mainSv)
        view.addConstraints(format: "V:|-80-[v0]-10-[v1]-10-[v2]-10-|", views: topVw, self.mainSv, orderCv)
        
    }
    
    lazy var transactionNoLb:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.customBlue
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    lazy var amountLb:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    lazy var statusLb:UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.7450980544, green: 0.3350246972, blue: 0.2241888852, alpha: 1)
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    lazy var favoriteImg = getImage(#imageLiteral(resourceName: "favorite_on"), height: 25, width: 25, action: #selector(changeFavorite))
    
    lazy var dateLb:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    
    lazy var reviewBtn:UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Review order", for: .normal)
        button.layer.cornerRadius = 10
        button.backgroundColor = UIColor.customBlue
        button.layer.borderColor = UIColor.customBlue.cgColor
        button.layer.borderWidth = 1
        button.setHeight(height: 30)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.addTarget(self, action: #selector(reviewBtnClick), for: .touchUpInside)
        return button
    }()

    @objc func reviewBtnClick(){
        let reviewController = ReviewOrderController()
        reviewController.order = order
        navigationController?.pushViewController(reviewController, animated: true)
    }
    
    lazy var topVw:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(238, 238, 238)
        reviewBtn.contentEdgeInsets = UIEdgeInsets(top: 10,left: 20,bottom: 10,right: 20)
        view.addConstraints(format: "H:|-10-[v0]", views: [transactionNoLb, statusLb, dateLb])
        view.addConstraints(format: "H:[v0]-10-|", views: [amountLb, favoriteImg, reviewBtn])
        view.addConstraints(format: "V:|-20-[v0]-20-[v1]-20-[v2]", views: transactionNoLb, statusLb, dateLb)
        view.addConstraints(format: "V:|-20-[v0]-20-[v1]-20-[v2]-20-|", views: amountLb, favoriteImg, reviewBtn)
        return view
    }()
    
    lazy var mainSv = getStackView()
    
    func getDetailsVw(_ title:String, _ subTitle:String)->UIView{
        let view = UIView()
        
        let titleLb = UILabel()
        titleLb.textColor = UIColor.customBlue
        titleLb.text = title
        titleLb.font = UIFont.boldSystemFont(ofSize: 14)
        
        let subtitleLb = UILabel()
        subtitleLb.textColor = UIColor.black
        subtitleLb.text = subTitle
        subtitleLb.font = UIFont.systemFont(ofSize: 13)

        let lineVw = UIView()
        lineVw.backgroundColor = .gray
        
        view.addConstraints(format: "H:|-20-[v0]", views: [titleLb, subtitleLb])
        view.addConstraints(format: "H:|[v0]|", views: lineVw)
        view.addConstraints(format: "V:|-5-[v0]-10-[v1]-10-[v2(1)]-5-|", views: titleLb, subtitleLb, lineVw)

        return view
    }
    
    @objc func changeFavorite(){
        
        if isFavorite!{
            removeFavorite()
        }else{
            addFavorite()
        }
    }
    
    func addFavorite(){
        let dict = ["cleaner_id":order!.cleanerId!]
        
        makeHttpCall(.favoriteCleaners, .post, dict){ data in
            self.isFavorite = true
            self.showDialog(message: "Successfully added as favorite")
        }
    }
    
    func removeFavorite(){
        let queryItem:[URLQueryItem] = [URLQueryItem(name: "api_access_token", value: "9MC9s-lobNm-F9xCTXA-3R6C-8ifzswERTu"),URLQueryItem(name: "cleaner_id", value: "\(order!.cleanerId!)")]
        
        makeHttpCall(.favoriteCleaners, .post, queryItems:queryItem){ data in
            self.showDialog(message: "Successfully removed as favorite")
            self.isFavorite = false
        }
    }
    
    var orderDrycleanerItem:[AllOrdersItems]?{
        didSet{
            orderCv.reloadData()
        }
    }
    
    lazy var orderCv:UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 5
        flowLayout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.layer.cornerRadius = 2
        collectionView.layer.borderWidth = 1
        collectionView.backgroundColor = .white
        collectionView.layer.borderColor = UIColor.init(hex: "#aaaaaa")?.cgColor
        collectionView.register(ViewOrderItemsCell.self, forCellWithReuseIdentifier: "orderCell")
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 40, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return orderDrycleanerItem?.count ?? 0
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "orderCell", for: indexPath) as! ViewOrderItemsCell
        cell.order = orderDrycleanerItem?[indexPath.item]
        return cell
    }
}
