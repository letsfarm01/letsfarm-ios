//
//  OrdersController.swift
//  CleanAgent
//
//  Created by Dekola Ak on 15/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

class OrdersController: BaseController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var activeFarmsResData:[ActiveFarmsResData]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true

        tabBarController?.tabBar.isHidden = true
        navigationItem.title = ""
        view.backgroundColor = .white
        
        view.addConstraints(format: "H:|[v0]|", views: [farmsCv])
        view.addConstraints(format: "V:|-60-[v0]-20-[v1]-100-|", views: headerLb, farmsCv)
        view.alignHorizontal(views: headerLb)
        fetchFarmHistory()

    }
    
    lazy var headerLb = getLabel(text: "Active Sponsorships", fontSize: 20, boldFont: true, textColor: .black)
    
    lazy var farmsCv = getCollectionView(spacing: 5, cell: ActiveFarmCell.self, identifier: "farmHistoryCell", delegate: self, dataSource: self)

    
    func fetchFarmHistory(){
        self.makeHttpCall(.getActiveSponsorship, .get, path:DefaultsManager.getUserId()){data in
            self.activeFarmsResData = try? JSONDecoder().decode(ActiveFarmsRes.self, from: data).data
            self.farmsCv.reloadData()
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activeFarmsResData?.count ?? 0
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let position = indexPath.item
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "farmHistoryCell", for: indexPath) as! ActiveFarmCell
//            cell.tag = position
//            cell.addGesture(UITapGestureRecognizer(target: self, action: #selector(onFarmCellTap(sender:))))
            cell.activeFarm = self.activeFarmsResData![indexPath.item]
            return cell
    }
}
