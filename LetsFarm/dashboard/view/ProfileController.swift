//
//  ProfileController.swift
//  CleanAgent
//
//  Created by Dekola Ak on 15/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

class DashboardController: BaseController {
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           navigationController?.isNavigationBarHidden = false
           tabBarController?.tabBar.isHidden = true
       }
       
       override func viewDidLoad() {
           super.viewDidLoad()
           tabBarController?.tabBar.isHidden = true
           navigationItem.title = ""
           view.backgroundColor = .white
           
       }
}
