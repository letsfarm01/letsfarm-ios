//
//  TabBarController.swift
//  CleanAgent
//
//  Created by Dekola Ak on 15/12/2019.
//  Copyright © 2019 Dekola Ak. All rights reserved.
//

import Foundation
import UIKit

class TabBarController: UITabBarController {
    
    let dashboardController = DashboardController()
    let ordersController = OrdersController()
    let settingsController = SettingsController()
//    let activityIndicator = UIActivityIndicatorView(style: .gray)

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        tabBarController?.tabBar.isHidden = false
//        self.tabBarController?.selectedIndex = 0
        self.selectedIndex = 0

//        addActivityIndicator(view)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tabBarController?.selectedIndex = 1
//        self.selectedIndex = 1

        let dashboard = UINavigationController(rootViewController: dashboardController)
        dashboard.title = "Dashboard"
        dashboard.tabBarItem.image = #imageLiteral(resourceName: "dashboard")
        
        let orders = UINavigationController(rootViewController: ordersController)
        orders.title = "Sponsorship"
        orders.tabBarItem.image = #imageLiteral(resourceName: "sponsorship")
        
        let settings = UINavigationController(rootViewController: settingsController)
        settings.title = "Settings"
        settings.tabBarItem.image = #imageLiteral(resourceName: "settings")
        
        viewControllers = [dashboard, orders, settings]
        
        activityIndicator.frame = CGRect(x: 100, y: 100, width: 500, height: 500)
        activityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
        view.alignVertical(views: activityIndicator)
        view.alignHorizontal(views: activityIndicator)
        
        
        tabBar.tintColor = .primaryColour
    }
    
}
