//
//  ReferralController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class ReferralController: BaseController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var refCode = DefaultsManager.getRefCode()
    var referrals:[ReferralResData]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true

        view.backgroundColor = .white
        navigationItem.title = "Referrals"
        
        view.addConstraints(format: "H:|[v0]|", views: [referralsCv])
        view.addConstraints(format: "H:|-15-[v0]", views: [referralCodeTitleLb, referralCodeVw])
        view.addConstraints(format: "V:|-120-[v0]-20-[v1]-20-[v2]-25-|", views: referralCodeTitleLb, referralCodeVw, referralsCv)
        fetchReferrals()

    }
    
    lazy var referralsCv = getCollectionView(spacing: 5, cell: ReferralCell.self, identifier: "referralsCell", delegate: self, dataSource: self)

    lazy var referralCodeTitleLb = getLabel(text: "Referral Code", fontSize: 12)
    
    lazy var referralCodeVw:UIView={
        let view = UIView()
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
        
        let refCodeLb = getLabel(text: refCode, fontSize: 17, boldFont: true, textColor: .black)
        let copyBtn = getButton(title: "Copy", bgColor: .primaryColour, titleColor: .white, cornerRadius: 1, action: #selector(copyRefCode))
        copyBtn.clipsToBounds = true
        view.addConstraints(format: "H:|-15-[v0]-15-[v1(80)]|", views: refCodeLb, copyBtn)
        view.addConstraints(format: "V:|[v0]|", views: copyBtn)
        view.alignVertical(views: refCodeLb)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(copyRefCode)))
        return view
    }()
    
    @objc func copyRefCode(){
        let pasteboard = UIPasteboard.general
        pasteboard.string = refCode
        showDialog(message: "Referal code has been copied to clipboard")
    }
    
    func fetchReferrals(){
        self.makeHttpCall(.getReferral, .get, path:DefaultsManager.getUserId()){data in
            self.referrals = try? JSONDecoder().decode(ReferralRes.self, from: data).data
            self.referralsCv.reloadData()
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return referrals?.count ?? 0
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "referralsCell", for: indexPath) as! ReferralCell
            cell.referral = self.referrals![indexPath.item]
            return cell
    }
}
