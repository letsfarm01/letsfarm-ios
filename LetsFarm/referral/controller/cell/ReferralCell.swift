//
//  ReferralCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class ReferralCell: UICollectionViewCell {
    
    var referral : ReferralResData?{
        didSet{
            emailLb.text = "Email: \(referral?.referralEmail ?? "")"
            bonusLb.text = "Earned bonus: ₦\(referral?.expectedBonus?.withCommas() ?? "0")"
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addConstraints(format: "V:|-20-[v0]-20-[v1]-20-[v2(1)]", views: emailLb, bonusLb, lineVw)
        
        addConstraints(formats: ["H:|-20-[v0]"], views: emailLb, bonusLb)
        
        addConstraints(format: "H:|[v0]|", views: lineVw)
        lineVw.backgroundColor = UIColor.init(hex: "#eeeeee")

    }
    

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var emailLb = getLabel(text: "Fish farm at 30% in 8 Months", fontSize: 14, boldFont: true, textColor: .black)
    
    lazy var bonusLb = getLabel(text: "Start of cycle: Jun 30, 2020", fontSize: 12)
    
    lazy var lineVw = UIView()
}
