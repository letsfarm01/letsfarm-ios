//
//  ReferralRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct ReferralRes : Codable {

    let code : Int?
    let data : [ReferralResData]?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }
    
}

struct ReferralResData : Codable {

    let v : Int?
    let id : String?
    let amountSponsor : Int?
    let createdAt : String?
    let currency : String?
    let deleted : Bool?
    let expectedBonus : Int?
    let hasJoined : Bool?
    let hasSponsor : Bool?
    let isBonusEarned : Bool?
    let isCycleCompleted : Bool?
    let isSponsorValid : Bool?
    let percentage : Int?
    let referral : Referral?
    let referralEmail : String?
//    let referree : AnyObject?
    let referreeEmail : String?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case amountSponsor = "amount_sponsor"
        case createdAt = "createdAt"
        case currency = "currency"
        case deleted = "deleted"
        case expectedBonus = "expected_bonus"
        case hasJoined = "has_joined"
        case hasSponsor = "has_sponsor"
        case isBonusEarned = "is_bonus_earned"
        case isCycleCompleted = "is_cycle_completed"
        case isSponsorValid = "is_sponsor_valid"
        case percentage = "percentage"
        case referral = "referral"
        case referralEmail = "referral_email"
//        case referree = "referree"
        case referreeEmail = "referree_email"
        case updatedAt = "updatedAt"
    }
    
}

struct Referral : Codable {

    let v : Int?
    let id : String?
    let address : String?
    let authType : String?
//    let channelNotice : AnyObject?
    let city : String?
    let country : String?
    let deleted : Bool?
    let dob : String?
    let firstName : String?
    let fullName : String?
    let gender : String?
    let isAuthSignUp : Bool?
    let isVerified : Bool?
    let lastName : String?
    let migrated : Bool?
    let phoneNumber : String?
    let privileges : [String]?
    let referralCode : String?
    let referralDone : Bool?
    let role : String?
    let stateProvince : String?
    let userImageUrl : String?
    let username : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case address = "address"
        case authType = "auth_type"
//        case channelNotice = "channel_notice"
        case city = "city"
        case country = "country"
        case deleted = "deleted"
        case dob = "dob"
        case firstName = "first_name"
        case fullName = "full_name"
        case gender = "gender"
        case isAuthSignUp = "is_auth_sign_up"
        case isVerified = "is_verified"
        case lastName = "last_name"
        case migrated = "migrated"
        case phoneNumber = "phone_number"
        case privileges = "privileges"
        case referralCode = "referral_code"
        case referralDone = "referral_done"
        case role = "role"
        case stateProvince = "state_province"
        case userImageUrl = "user_image_url"
        case username = "username"
    }
    
}
