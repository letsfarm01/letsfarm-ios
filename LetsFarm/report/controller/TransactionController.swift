//
//  TransactionController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//


import Foundation
import UIKit

class TransactionController: BaseController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var transactions:[TransactionsResData]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true

        view.backgroundColor = .white
        navigationItem.title = "Transactions"
        
        view.addConstraints(format: "H:|[v0]|", views: [farmsCv])
        view.addConstraints(format: "V:|-10-[v0]-25-|", views: farmsCv)
        fetchFarmHistory()

    }
    
    lazy var farmsCv = getCollectionView(spacing: 5, cell: TransactionCell.self, identifier: "transactionCell", delegate: self, dataSource: self)

    
    func fetchFarmHistory(){
        self.makeHttpCall(.getTransactions, .get, path:DefaultsManager.getUserId()){data in
            self.transactions = try? JSONDecoder().decode(TransactionsRes.self, from: data).data
            self.farmsCv.reloadData()
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return transactions?.count ?? 0
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "transactionCell", for: indexPath) as! TransactionCell
            cell.transaction = self.transactions![indexPath.item]
            return cell
    }
}
