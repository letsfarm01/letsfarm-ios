//
//  WalletHistoryController.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//


import Foundation
import UIKit

class WalletHistoryController: BaseController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var walletTransactions:[WalletTransactionResData]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true

        view.backgroundColor = .white
        navigationItem.title = "Wallet History"
        
        view.addConstraints(format: "H:|[v0]|", views: [farmsCv])
        view.addConstraints(format: "V:|-10-[v0]-25-|", views: farmsCv)
        fetchFarmHistory()

    }
    
    lazy var farmsCv = getCollectionView(spacing: 5, cell: WalletTransactionCell.self, identifier: "walletTransactionCell", delegate: self, dataSource: self)

    
    func fetchFarmHistory(){
        self.makeHttpCall(.getUserWalletTransaction, .get, path:DefaultsManager.getUserId()){data in
            self.walletTransactions = try? JSONDecoder().decode(WalletTransactionRes.self, from: data).data
            self.farmsCv.reloadData()
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return walletTransactions?.count ?? 0
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "walletTransactionCell", for: indexPath) as! WalletTransactionCell
            cell.walletTransaction = self.walletTransactions![indexPath.item]
            return cell
    }
}
