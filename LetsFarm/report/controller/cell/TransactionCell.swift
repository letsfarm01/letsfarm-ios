//
//  TransactionCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class TransactionCell: UICollectionViewCell {
    
    var transaction : TransactionsResData?{
        didSet{
            self.descrLb.text = transaction?.narration
            self.dateLb.text = transaction?.createdAt?.getFormattedDate()
            self.statusLb.text = transaction?.status
            self.paymentChannelLb.text = transaction?.paymentChannel?.name
            self.referenceLb.text = transaction?.orderReference
            self.amountLb.text = "₦\(transaction?.totalPrice?.withCommas() ?? "0")"
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addConstraints(format: "V:|-10-[v0]-10-[v1]-10-[v2]-10-[v3(1)]", views: descrLb, dateLb, paymentChannelLb, lineVw)
        addConstraints(format: "H:|-10-[v0]", views: descrLb)
        addConstraints(formats: ["H:|-15-[v0]"], views: dateLb, paymentChannelLb)

        addConstraints(format: "V:|-10-[v0]-10-[v1]-10-[v2]-10-|", views: descrLb, dateLb, paymentChannelLb)

        addConstraints(formats: ["H:[v0]-15-|"], views: amountLb, statusLb, referenceLb)

        addConstraints(format: "H:|[v0]|", views: lineVw)
        lineVw.backgroundColor = UIColor.init(hex: "#eeeeee")
        
        statusLb.alignBottomToBottomOf(of: dateLb)
        referenceLb.alignBottomToBottomOf(of: paymentChannelLb)
        amountLb.alignBottomToBottomOf(of: descrLb)

    }
    

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var descrLb = getLabel(text: "", fontSize: 12, boldFont: true, textColor: .black)
    
    lazy var dateLb = getLabel(text: "", fontSize: 12)
    lazy var statusLb = getLabel(text: "", fontSize: 12)
    
    lazy var paymentChannelLb = getLabel(text: "", fontSize: 12)
    lazy var referenceLb = getLabel(text: "", fontSize: 12)

    lazy var amountLb = getLabel(text: "", fontSize: 12)

    lazy var lineVw = UIView()
}

