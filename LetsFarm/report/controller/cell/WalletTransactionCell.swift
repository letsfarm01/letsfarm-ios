//
//  WalletTransactionCell.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation
import UIKit

class WalletTransactionCell: UICollectionViewCell {
    
    var walletTransaction : WalletTransactionResData?{
        didSet{
            self.descrLb.text = walletTransaction?.transactionId?.narration
            self.dateLb.text = walletTransaction?.createdAt?.getFormattedDate()
            self.statusLb.text = walletTransaction?.type
            self.amountLb.text = "₦\(walletTransaction?.amount?.withCommas() ?? "0")"
        }
    }

        override init(frame: CGRect) {
            super.init(frame: frame)
            
            addConstraints(format: "V:|-10-[v0]-10-[v1]-10-[v2(1)]", views: descrLb, dateLb, lineVw)
            addConstraints(format: "H:|-10-[v0]", views: descrLb)
            addConstraints(formats: ["H:|-15-[v0]"], views: dateLb)

            addConstraints(format: "V:|-10-[v0]-10-[v1]-10-|", views: descrLb, dateLb)

            addConstraints(formats: ["H:[v0]-15-|"], views: amountLb, statusLb)

            addConstraints(format: "H:|[v0]|", views: lineVw)
            lineVw.backgroundColor = UIColor.init(hex: "#eeeeee")
            
            statusLb.alignBottomToBottomOf(of: dateLb)
            amountLb.alignBottomToBottomOf(of: descrLb)

        }
        

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        lazy var descrLb = getLabel(text: "", fontSize: 12, boldFont: true, textColor: .black)
        
        lazy var dateLb = getLabel(text: "", fontSize: 12)
        lazy var statusLb = getLabel(text: "", fontSize: 12)
        
        lazy var amountLb = getLabel(text: "", fontSize: 12)

        lazy var lineVw = UIView()
    }

