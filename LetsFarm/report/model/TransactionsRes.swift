//
//  TransactionsRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct TransactionsRes : Codable {

    let code : Int?
    let data : [TransactionsResData]?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }
    
}


struct TransactionsResData : Codable {

    let v : Int?
    let id : String?
    let createdAt : String?
    let deleted : Bool?
    let narration : String?
    let orderReference : String?
    let paymentChannel : PaymentChannel?
    let paymentReference : String?
    let status : String?
    let totalPrice : Int?
    let transactionType : String?
    let updatedAt : String?
    let userId : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case narration = "narration"
        case orderReference = "orderReference"
        case paymentChannel = "payment_channel"
        case paymentReference = "paymentReference"
        case status = "status"
        case totalPrice = "total_price"
        case transactionType = "transactionType"
        case updatedAt = "updatedAt"
        case userId = "userId"
    }
    
}

struct PaymentChannel : Codable {

    let v : Int?
    let id : String?
    let canFund : Bool?
    let createdAt : String?
    let deleted : Bool?
    let descriptionField : String?
    let isActive : Bool?
    let name : String?
    let updatedAt : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case canFund = "can_fund"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case descriptionField = "description"
        case isActive = "is_active"
        case name = "name"
        case updatedAt = "updatedAt"
    }
    
}
