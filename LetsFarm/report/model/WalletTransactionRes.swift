//
//  WalletTransactionRes.swift
//  LetsFarm
//
//  Created by Akano Adekola on 11/08/2020.
//  Copyright © 2020 Akano Adekola. All rights reserved.
//

import Foundation

struct WalletTransactionRes : Codable {

    let code : Int?
    let data : [WalletTransactionResData]?
    let msg : String?
    let status : String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case status = "status"
    }
    
}

struct WalletTransactionResData : Codable {

    let v : Int?
    let id : String?
    let amount : Int?
    let createdAt : String?
    let deleted : Bool?
    let narration : String?
    let newWalletBalance : Int?
    let oldWalletBalance : Int?
    let transactionId : TransactionId?
    let type : String?
    let updatedAt : String?
    let userId : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case amount = "amount"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case narration = "narration"
        case newWalletBalance = "new_wallet_balance"
        case oldWalletBalance = "old_wallet_balance"
        case transactionId = "transactionId"
        case type = "type"
        case updatedAt = "updatedAt"
        case userId = "userId"
    }
    
}

struct TransactionId : Codable {

    let v : Int?
    let id : String?
    let createdAt : String?
    let deleted : Bool?
    let narration : String?
    let orderReference : String?
    let paymentChannel : String?
    let paymentReference : String?
    let status : String?
    let totalPrice : Int?
    let transactionType : String?
    let updatedAt : String?
    let userId : String?

    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case createdAt = "createdAt"
        case deleted = "deleted"
        case narration = "narration"
        case orderReference = "orderReference"
        case paymentChannel = "payment_channel"
        case paymentReference = "paymentReference"
        case status = "status"
        case totalPrice = "total_price"
        case transactionType = "transactionType"
        case updatedAt = "updatedAt"
        case userId = "userId"
    }
    
}
